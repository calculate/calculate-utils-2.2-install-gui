#include "pagelicense.h"

#include <QLayout>
#include <QTextEdit>
#include <QCheckBox>

#include <QDebug>

#include "tools.h"

PageLicense::PageLicense() :
	InstallerPage( )
{
	setupUi();

	connect( m_checkAccept, SIGNAL(toggled(bool)), this, SIGNAL(changeNext(bool)) );
}

void PageLicense::setupUi()
{
	m_textLicense = new QTextEdit;
	m_checkAccept = new QCheckBox;

	m_textLicense->setReadOnly(true);

	QHBoxLayout*	hbox_1 = new QHBoxLayout;

	hbox_1->addWidget(m_checkAccept);
	hbox_1->addStretch();

	QVBoxLayout*	vbox_1 = new QVBoxLayout;

	vbox_1->addWidget(m_textLicense);
	vbox_1->addLayout(hbox_1);

	setLayout(vbox_1);

	retranslateUi();
}

void PageLicense::retranslateUi()
{
	setTitle( tr("License") );

	m_textLicense->setText(
		"<html>" +
		tr(
			"<h3>License</h3>"
			"<h4>License Agreement</h4>"
			"<p>This operating system (the OS) is composed of many individual software components,"
			" the copyrights on each of which belong to their respective owners."
			" Each component is distributed under their own license agreement.</p>"
			"<p>Installing, modifying or distributing this operating system, given to you as free archive,"
			" you agree with all of the following.</p>"
			"<h4>Warranties</h4>"
			"<p>This software is distributed without warranty of any kind."
			" You assume all responsibility for the use of the operating system.</p>"
			"<h4>Installing</h4>"
			"<p>OS can be installed on any number of computers.</p>"
			"<h4>Distribution</h4>"
			"<p>Most of the software included in this operating system, allows you to freely modify, copy and "
			"distribute it. Also included in the OS software is distributed in the different conditions."
			" For more information please refer to the documentation accompanying a particular software component.</p>"
		) +
		"</html>"
	);
	m_checkAccept->setText( tr("Accept") );
}

void PageLicense::show()
{
	emit changeNext( m_checkAccept->isChecked() );
}

