#pragma once

#include <QtGui/QDialog>
#include <QMap>

#include "commons.h"

class QLabel;
class QLineEdit;
class QComboBox;
class QCheckBox;
class QWidget;

class MountPointDialog: public QDialog
{
	Q_OBJECT

public:
	MountPointDialog(
		QWidget* parent,
		const MountPointsList& AllMountPoints,
		const MountPointsMap& NotUsedParts,
		const MountPoint& mountPoint = MountPoint()
	);
	~MountPointDialog();

	MountPoint	getMountPoint() { return m_MountPoint; }

private slots:
	void preAccept();
	void changedControls();

private:
	void setupUi();

private:
	QLabel*			m_labMountPoint;
	QComboBox*		m_cbMountPoint;
	QLabel*			m_labPart;
	QComboBox*		m_cbPart;
	QCheckBox*		m_chkboxFormat;
	QWidget*		m_widgetFS;
	QLabel*			m_labFS;
	QComboBox*		m_cmbboxFS;

	QPushButton*	m_butOk;
	QPushButton*	m_butCancel;

	MountPoint		m_MountPoint;

	MountPointsMap	m_NotUsedParts;
	MountPointsList	m_AllMountPoints;
};