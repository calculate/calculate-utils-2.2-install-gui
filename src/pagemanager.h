#ifndef PAGEMANAGER_H
#define PAGEMANAGER_H

#include <QObject>

class QLabel;
class QStackedWidget;
class InstallerPage;

typedef QList<InstallerPage*>::iterator	PageIterator;

class PageManager : public QObject
{
	Q_OBJECT
public:
	explicit PageManager(QStackedWidget* stackedWidget, QLabel* listLabel, QObject* parent = 0);
	~PageManager();

public:
	void addPage(InstallerPage* page);
	unsigned int getPageCount();

	void showOnce(InstallerPage* page);

	void retranslatePages();

public slots:
	void showFirst();

	void showNext();
	void showPrevious();

signals:
	void changeNext(bool);
	void changePrev(bool);

private:
	void pageUpdate();
	void removeStackedPage();

private:
	QStackedWidget*			m_StackWidget;
	QLabel*					m_ListLabel;

	QList<InstallerPage*>	m_Pages;
	PageIterator			m_CurPage;

	bool					m_isSinglePage;
};

#endif // PAGEMANAGER_H
