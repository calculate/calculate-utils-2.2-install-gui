#pragma once

#include <QObject>

#include <QMap>
#include <QString>
#include <QStringList>

#include <QVariant>

class QDomNode;

typedef QMap<QString, QVariant> MapConfig;

class CalculateConfig: public QObject
{
	Q_OBJECT
public:
	static CalculateConfig* instance();

	bool		getDefaultConfig();
	bool 		getNewPartitioning();
	bool 		getNewMountpoints();

	QVariant	getValue(const QString& key) const { return m_Config[key]; }
	void 		setValue(const QString& key, const QVariant& value);

	void		showInstallParameters();
	QStringList	getInstallParameters();

	QStringList getPasswordUsers();

signals:
	void		sendParameters(QStringList);

private:
	CalculateConfig();
	~CalculateConfig();
	CalculateConfig(const CalculateConfig&);
	CalculateConfig& operator=(const CalculateConfig&);

	void		parseVariables(const QDomNode& node, MapConfig& cfg);
	void		parseVar(const QDomNode& node, MapConfig& cfg);
	void		parseVarList(const QDomNode& node, MapConfig& cfg);

	QStringList	getDisksParameters();

private:
	static CalculateConfig*	_instance;

	MapConfig	m_Config;
};
