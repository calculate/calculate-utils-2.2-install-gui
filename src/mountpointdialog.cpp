#include "mountpointdialog.h"

#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QBoxLayout>
#include <QPushButton>
#include <QRegExp>
#include <QRegExpValidator>
#include <QPair>

#include <QMessageBox>

#include <QDebug>


MountPointDialog::MountPointDialog (
	QWidget* parent,
	const MountPointsList& AllMountPoints,
	const MountPointsMap& NotUsedParts,
	const MountPoint& mountPoint
)
:	QDialog(parent), m_MountPoint(mountPoint), m_NotUsedParts(NotUsedParts), m_AllMountPoints(AllMountPoints)
{
	setupUi();

	if ( !m_MountPoint.mountpoint.isEmpty() )
	{
		m_cbMountPoint->clear();
		m_cbMountPoint->addItem( m_MountPoint.mountpoint );
		m_cbMountPoint->setEditable(false);

		int partIndx = m_cbPart->findData(m_MountPoint.dev);
		if (partIndx != -1)
			m_cbPart->setCurrentIndex(partIndx);

		m_chkboxFormat->setChecked(m_MountPoint.format);
		QString	fs = m_MountPoint.fs;
		if ( !m_MountPoint.fs_new.isEmpty() )
			fs = m_MountPoint.fs_new;

		//
		int fsIndx = m_cmbboxFS->findText( fs );
		if ( fsIndx != -1)
			m_cmbboxFS->setCurrentIndex( fsIndx );
	}

	connect( m_butOk, SIGNAL(clicked(bool)), this, SLOT(preAccept()) );
	connect( m_butCancel, SIGNAL(clicked(bool)), this, SLOT(reject()) );

	connect( m_cbMountPoint, SIGNAL(currentIndexChanged(int)), this, SLOT(changedControls()) );
	connect( m_cbPart, SIGNAL(currentIndexChanged(int)), this, SLOT(changedControls()) );

	changedControls();
}

MountPointDialog::~MountPointDialog()
{
}

void MountPointDialog::setupUi()
{
	QRegExp	rxDir("/[a-zA-Z][0-9a-zA-Z_-/]{0,63}");

	m_labMountPoint = new QLabel( tr("Mount point: ") );
	m_cbMountPoint = new QComboBox;
	m_cbMountPoint->addItems(
		QStringList() << "/boot" << "/usr" << "/var" << "/var/calculate" <<
			"/var/calculate/remote" << "/home" << "/tmp" << "swap"
	);
	m_cbMountPoint->setEditable(true);
	m_cbMountPoint->setValidator( new QRegExpValidator(rxDir, this) );

	m_labPart = new QLabel( tr("Partition: ") );
	m_cbPart = new QComboBox;

	if (m_MountPoint.migrated)
	{
		m_cbPart->addItem( tr("none"), QVariant("none") );
	}
	QMapIterator<QString,MountPoint> i(m_NotUsedParts);
	while ( i.hasNext() )
	{
		i.next();

		QString partDesc = i.value().dev + " - " + i.value().fs + " - " + i.value().size + " " + i.value().label;
		m_cbPart->addItem( partDesc, QVariant(i.key()) );
	}

	QGridLayout*	gbox_0 = new QGridLayout;
	gbox_0->setContentsMargins(0, 0, 0, 0);
	gbox_0->addWidget(m_labMountPoint, 0, 0);
	gbox_0->addWidget(m_cbMountPoint, 0, 1);
	gbox_0->addWidget(m_labPart, 1, 0);
	gbox_0->addWidget(m_cbPart, 1, 1);

	m_chkboxFormat = new QCheckBox( tr("Format partition") );

	m_widgetFS = new QWidget;

	m_labFS = new QLabel( tr("File system: ") );
	m_cmbboxFS = new QComboBox;
	m_cmbboxFS->addItem( "btrfs" );
	m_cmbboxFS->addItem( "ext2" );
	m_cmbboxFS->addItem( "ext3" );
	m_cmbboxFS->addItem( "ext4" );
	m_cmbboxFS->addItem( "jfs" );
	m_cmbboxFS->addItem( "nilfs2" );
	m_cmbboxFS->addItem( "reiserfs" );
	m_cmbboxFS->addItem( "xfs" );
	m_cmbboxFS->addItem( "swap" );

	QHBoxLayout*	hbox_format = new QHBoxLayout;
	hbox_format->addWidget(m_labFS);
	hbox_format->addWidget(m_cmbboxFS);

	m_widgetFS->setLayout(hbox_format);
	m_widgetFS->setVisible(false);
	connect(m_chkboxFormat, SIGNAL(toggled(bool)), m_widgetFS, SLOT(setVisible(bool)) );

	m_butOk = new QPushButton( tr("OK") );
	m_butCancel = new QPushButton( tr("Cancel") );

	QHBoxLayout*	hbox_2 = new QHBoxLayout;
	hbox_2->addStretch();
	hbox_2->addWidget(m_butOk);
	hbox_2->addWidget(m_butCancel);

	QVBoxLayout*	vbox_0 = new QVBoxLayout;
	vbox_0->addLayout(gbox_0);
	vbox_0->addWidget(m_chkboxFormat);
	vbox_0->addWidget(m_widgetFS);
	vbox_0->addStretch();
	vbox_0->addLayout(hbox_2);

	setLayout( vbox_0 );
}

void MountPointDialog::preAccept()
{
	if ( m_cbMountPoint->currentText().isEmpty() )
	{
		QMessageBox::warning(this, tr("Warning"), tr("Mount point must be not empty") );
		return;
	}

	if (m_MountPoint.mountpoint.isEmpty())
	{
		foreach(const MountPoint& mp, m_AllMountPoints )
		{
			if (mp.mountpoint == m_cbMountPoint->currentText())
			{
				QMessageBox::warning(this, tr("Warning"), tr("Duplicate mount point %1").arg(mp.mountpoint) );
				return;
			}
		}
	}

	m_MountPoint.mountpoint = m_cbMountPoint->currentText();

	QString	tmp_dev = m_MountPoint.dev;
	m_MountPoint.dev = m_cbPart->itemData( m_cbPart->currentIndex() ).toString();
	if (m_MountPoint.dev != "none")
	{

		m_MountPoint.format = m_chkboxFormat->isChecked();
		if (m_MountPoint.format)
			m_MountPoint.fs_new = m_cmbboxFS->currentText();

		MountPoint mp = m_NotUsedParts[m_MountPoint.dev];

		m_MountPoint.fs = mp.fs;
		m_MountPoint.label = mp.label;
		m_MountPoint.size = mp.size;

	} else {
		m_MountPoint.label = "";
		m_MountPoint.size = "";
		m_MountPoint.fs = "";
		m_MountPoint.fs_new = "";
		m_MountPoint.format = false;
	}

	accept();
}

void MountPointDialog::changedControls()
{
	bool isSwap = m_cbMountPoint->currentText() == "swap";
	bool isNone = m_cbPart->currentText() == "none";

	m_chkboxFormat->setEnabled( !(isSwap || isNone) );
	m_cmbboxFS->setEnabled( !(isSwap || isNone) );
}

