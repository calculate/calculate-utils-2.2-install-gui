#pragma once

#include <QMainWindow>
#include <QString>
#include <QScopedPointer>

#include "tools.h"

class QPushButton;
class QToolButton;
class QLabel;
class QStackedWidget;
class QAction;
class QMenu;

class QTranslator;

class PageManager;
class PageCfdisk;

class SystemInstaller : public QMainWindow
{
	Q_OBJECT
public:
	explicit SystemInstaller(QWidget *parent = 0);
	~SystemInstaller();

protected:
	void changeEvent(QEvent* event);
	void closeEvent(QCloseEvent* event);

private:
	void setupUi();
	void retranslateUi();

	void selectWindowSize();

	void setupInstallerPages();

	void setTranslator(QTranslator* translator);
	void removeTranslator();

private slots:
	void changeNext(bool);
	void changePrev(bool);
	void toggleButtons(bool);

	void changeLanguage(QString);

	void doPartitioning(QString);
	void completePartitioning();

	void showCmd(QStringList params);
	void showAbout();

	void showCopyMenu(const QPoint& point);
	void copyCmd();

	void finishInstall();


private:
	// ui
	QPushButton*	m_butPrev;
	QPushButton*	m_butNext;
	QPushButton*	m_butFinish;
	QLabel*			m_labelImage;
	QLabel*			m_labelPages;
	QStackedWidget* m_stackPages;
	QLabel*			m_labelInstCmd;
	QPushButton*	m_butAbout;

	QToolButton*	m_butCopy;

	QAction*		m_actCopy;
	QMenu*			m_menuCopy;

	//
	QTranslator*	m_Translator;

	PageCfdisk*					m_PageCfdisk;
	QScopedPointer<PageManager>	m_PageManager;

	QString			m_CurrentLanguage;

	volatile bool	m_doFinish;
};

