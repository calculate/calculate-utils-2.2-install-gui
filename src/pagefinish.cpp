#include "pagefinish.h"

#include <QApplication>
#include <QDebug>

#include <QLabel>
#include <QBoxLayout>

#include "tools.h"

PageFinish::PageFinish() :
	InstallerPage()
{
	setupUi();
}

void PageFinish::setupUi()
{
	m_labelComplete = new QLabel;

	m_labelComplete->setWordWrap(true);
	m_labelComplete->setAlignment( Qt::AlignLeft | Qt::AlignTop );

	QVBoxLayout* vbox = new QVBoxLayout;

	vbox->addWidget(m_labelComplete);

	setLayout(vbox);

	retranslateUi();
}

void PageFinish::retranslateUi()
{
	setTitle( tr("Complete") );
	m_labelComplete->setText(
		"<html>" +
		tr(
			"<h4>Congratulation!</h4>"
			"<p>Installation complete."
			"Press Finish for exit.</p>"
		) +
		"</html>"
	);
}

void PageFinish::show()
{
	emit toggleButtons(false);
}
