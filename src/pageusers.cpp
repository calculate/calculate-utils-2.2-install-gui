#include "pageusers.h"

#include <QBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QLineEdit>
#include <QToolButton>
#include <QListWidget>
#include <QMessageBox>

#include <QScopedPointer>
#include <QTimer>

#include "userinfodialog.h"

#include <QDebug>

#include "calculateconfig.h"

PageUsers::PageUsers()
  :	InstallerPage()
{
	setupUi();

	// connect
	connect( m_butAddUser, SIGNAL(clicked()), this, SLOT(addUser()) );
	connect( m_butDelUser, SIGNAL(clicked()), this, SLOT(delUser()) );
	connect( m_lstUsers, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(modifyUser()) );

	connect( m_edRootPsw, SIGNAL(textChanged(QString)), this, SLOT(checkPasswords()) );
	connect( m_edRootPswRep, SIGNAL(textChanged(QString)), this, SLOT(checkPasswords()) );

	// add default guest user
	m_migratedUsers = CalculateConfig::instance()->getValue("def_cl_migrate_user").toStringList();
// 	if ( !m_migratedUsers.contains("guest") )
// 		m_lstUsers->addItem("guest");

	checkPasswords();
}

void PageUsers::setupUi()
{
	m_labRoot = new QLabel;

	QHBoxLayout* hbox_0 = new QHBoxLayout;
	hbox_0->addWidget(m_labRoot);
	hbox_0->addStretch();

	m_labRootPsw = new QLabel;
	m_edRootPsw = new QLineEdit;
	m_edRootPsw->setEchoMode( QLineEdit::Password );

	m_labRootPswRep = new QLabel;
	m_edRootPswRep = new QLineEdit;
	m_edRootPswRep->setEchoMode( QLineEdit::Password );

	QGridLayout*	gbox_0 = new QGridLayout;
	gbox_0->setContentsMargins(0, 0, 0, 0);
	gbox_0->addWidget(m_labRootPsw, 0, 0);
	gbox_0->addWidget(m_edRootPsw, 0, 1);
	gbox_0->addWidget(m_labRootPswRep, 1, 0);
	gbox_0->addWidget(m_edRootPswRep, 1, 1);

	QFrame*	hline = new QFrame;
	hline->setFrameShape(QFrame::HLine);
	hline->setFrameShadow(QFrame::Sunken);

	m_labMatch = new QLabel;
	QHBoxLayout* hbox_1 = new QHBoxLayout;
	hbox_1->addStretch();
	hbox_1->addWidget(m_labMatch);

	m_labUsers = new QLabel;

	QHBoxLayout* hbox_users = new QHBoxLayout;
	hbox_users->addWidget(m_labUsers);
	hbox_users->addStretch();

	m_butAddUser = new QToolButton;
	m_butAddUser->setIcon( QIcon(":/img/list-add-user.png") );

	m_butDelUser = new QToolButton;
	m_butDelUser->setIcon( QIcon(":/img/list-remove-user.png") );

	QHBoxLayout* hbox_but = new QHBoxLayout;
	hbox_but->addWidget( m_butAddUser );
	hbox_but->addWidget( m_butDelUser );
	hbox_but->addStretch();

	m_lstUsers = new QListWidget;

	QFrame*	hline2 = new QFrame;
	hline2->setFrameShape(QFrame::HLine);
	hline2->setFrameShadow(QFrame::Sunken);

	m_labMigratedUsers = new QLabel("");
	m_labMigratedUsers->setWordWrap(true);

	QVBoxLayout* vbox_0 = new QVBoxLayout;
	vbox_0->addLayout(hbox_0);
	vbox_0->addLayout(gbox_0);
	vbox_0->addLayout(hbox_1);
	vbox_0->addWidget(hline);
	vbox_0->addLayout(hbox_users);
	vbox_0->addLayout(hbox_but);
	vbox_0->addWidget(m_lstUsers);
	vbox_0->addWidget(hline2);
	vbox_0->addWidget(m_labMigratedUsers);
//	vbox_0->addStretch();

	setLayout(vbox_0);

	retranslateUi();

}

void PageUsers::retranslateUi()
{
	setTitle( tr("Users") );

	m_labRoot->setText( tr("Set root password:") );
	m_labRootPsw->setText( tr("Password") );
	m_labRootPswRep->setText( tr("Confirm Password") );

	m_butAddUser->setToolTip( tr("Add user") );
	m_butDelUser->setToolTip( tr("Remove selected user") );

	m_lstUsers->setToolTip( tr("Added users.\nFor modifying user - double click it.") );

	m_labUsers->setText( tr("Create users:") );

	if ( m_migratedUsers.size() > 0 )
		m_labMigratedUsers->setText( tr("These user accounts will be migrated: ") + m_migratedUsers.join(", ") );
	else
		m_labMigratedUsers->setText( tr("If the users will not be added, the 'guest' user be created automatically."));
}

void PageUsers::show()
{
	if ( !CalculateConfig::instance()->getPasswordUsers().contains("root") )
	{
		m_edRootPsw->setEnabled(false);
		m_edRootPswRep->setEnabled(false);
		m_pswState = true;
		m_labMatch->setText( "" ); //tr("Root password will be moved from current system.") );
	}

	emit changeNext( m_pswState );
}

void PageUsers::addUser()
{
//	qDebug() << "add user";
	QScopedPointer<UserInfoDialog> userDlg( new UserInfoDialog(this) );

	if ( userDlg->exec() == QDialog::Accepted )
	{
		UserInfo userInfo = userDlg->getUserInfo();

		if ( findUserName(userInfo) == -1 && !m_migratedUsers.contains(userInfo.name) )
		{
			m_lstUserInfo << userInfo;
			m_lstUsers->addItem( userInfo.name );
			updateConfig();
		}
		else
		{
			QMessageBox::critical( this, tr("Error"), tr("User %1 already exists.").arg(userInfo.name) );
			QTimer::singleShot(0, this, SLOT(addUser()));
		}
	}
}

void PageUsers::delUser()
{
//	qDebug() << "del user";
	if (m_lstUserInfo.isEmpty())
	{
		return;
	}
	else
	{
		int index = m_lstUsers->currentRow();
		if (index != -1)
		{
			delete m_lstUsers->takeItem(index);
			m_lstUserInfo.removeAt(index);
		}
		updateConfig();
	}
}

void PageUsers::modifyUser()
{
//	qDebug() << "modify user";
	if (m_lstUserInfo.isEmpty())
	{
		return;
	}
	else
	{
		int index = m_lstUsers->currentRow();
		if (index != -1)
		{
			QScopedPointer<UserInfoDialog> userDlg( new UserInfoDialog(this, m_lstUserInfo.at(index)) );

			if ( userDlg->exec() == QDialog::Accepted )
			{
				UserInfo userInfo = userDlg->getUserInfo();

				int	index_0 = findUserName(userInfo);
				if ( (index_0 == -1) || (index_0 == index) )
				{
					delete m_lstUsers->takeItem(index);
					m_lstUsers->insertItem(index, userInfo.name);
					m_lstUserInfo.replace(index, userInfo);

					updateConfig();
				}
				else
				{
					QMessageBox::critical( this, tr("Error"), tr("User %1 already exists.").arg(userInfo.name) );
					QTimer::singleShot(0, this, SLOT(modifyUser()));
				}
			}
		}
	}
}

void PageUsers::checkPasswords()
{
//	qDebug() << "check passwords";

	m_pswState = false;

	if ( (!m_edRootPsw->text().isEmpty()) && (m_edRootPsw->text() == m_edRootPswRep->text()) )
		m_pswState = true;

	QString	pswState;
	if (m_pswState )
	{
		pswState = tr("Passwords match");
	}
	else
	{
		pswState = tr("Passwords do not match");
	}
	m_labMatch->setText( pswState );

	emit changeNext( m_pswState );
}

int PageUsers::findUserName( const UserInfo& userInfo )
{
	int i = 0;
	foreach(const UserInfo& info, m_lstUserInfo)
	{
		if (info.name.compare(userInfo.name, Qt::CaseInsensitive) == 0)
			return i;
		++i;
	}
	return -1;
}

bool PageUsers::validate()
{
	CalculateConfig::instance()->setValue("gui_root_psw", m_edRootPsw->text());

	return true;
}

void PageUsers::updateConfig()
{
	CalculateConfig*	clConf = CalculateConfig::instance();

	QStringList	users;
	QStringList	passwords;
	foreach(const UserInfo& user, m_lstUserInfo)
	{
		users << user.name;
		passwords << user.psw;
	}

	clConf->setValue("gui_users", users);
	clConf->setValue("gui_passwds", passwords);

	clConf->showInstallParameters();
}

