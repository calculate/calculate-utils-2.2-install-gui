#pragma once

#include <QtGui/QDialog>

#include "commons.h"

class QLabel;
class QLineEdit;

class UserInfoDialog: public QDialog
{
	Q_OBJECT

public:
	UserInfoDialog(QWidget* parent = 0, const UserInfo& userInfo = UserInfo() );
	~UserInfoDialog();

	UserInfo getUserInfo() { return m_UserInfo; }

private slots:
	void checkPasswords();
	void preAccept();

private:
	void setupUi();

private:
	QLabel*			m_labUserName;
	QLineEdit*		m_edUserName;
	QLabel*			m_labPsw;
	QLineEdit*		m_edPsw;
	QLabel*			m_labPswRep;
	QLineEdit*		m_edPswRep;
	QLabel*			m_labMatch;

	QPushButton*	m_butOk;
	QPushButton*	m_butCancel;

	UserInfo		m_UserInfo;
};