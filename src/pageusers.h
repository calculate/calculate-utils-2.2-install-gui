#pragma once

#include "installerpage.h"

#include "commons.h"

class QLabel;
class QLineEdit;
class QToolButton;
class QListWidget;

class PageUsers : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageUsers();

	void retranslateUi();
	bool validate();

protected:
	void setupUi();

public slots:
	void show();

private slots:
	void addUser();
	void delUser();
	void modifyUser();
	void checkPasswords();

private:
	int 	findUserName(const UserInfo& userInfo);
	void 	updateConfig();

private:
	QLabel*			m_labRoot;
	QLabel*			m_labRootPsw;
	QLineEdit*		m_edRootPsw;
	QLabel*			m_labRootPswRep;
	QLineEdit*		m_edRootPswRep;
	QLabel*			m_labMatch;

	QLabel*			m_labUsers;

	QToolButton*	m_butAddUser;
	QToolButton*	m_butDelUser;

	QListWidget*	m_lstUsers;

	QLabel*			m_labMigratedUsers;

	bool			m_pswState;
	QList<UserInfo>	m_lstUserInfo;
	QStringList		m_migratedUsers;
};
