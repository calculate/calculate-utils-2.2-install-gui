#include "systeminstaller.h"

#include <QtGui>
#include <QtGlobal>

#include <QIcon>
#include <QStackedWidget>
#include <QPushButton>
#include <QToolButton>
#include <QGroupBox>
#include <QLocale>
#include <QAction>
#include <QMenu>

#include "calculateconfig.h"

#include "pagemanager.h"

#include "pagewelcome.h"
#include "pagelicense.h"
#include "pagepartitioning.h"
#include "pagemountpoints.h"
#include "pageconfiguration.h"
#include "pageusers.h"
#include "pageinstall.h"
#include "pagefinish.h"

#include "pagecfdisk.h"

const QString	VER_STR = "2.2.28";

SystemInstaller::SystemInstaller(QWidget *parent) :
	QMainWindow(parent),
	m_Translator(new QTranslator),
	m_doFinish(false)
{
	bool	installReady(true);

	if ( !CalculateConfig::instance()->getDefaultConfig() )
	{
		QMessageBox::critical(this, tr("Critical error"), tr("Failed to launch 'cl-install'.") );
		changeLanguage( "en_US" );
		installReady = false;
	} else {
		// set os locale language
		changeLanguage( CalculateConfig::instance()->getValue("os_locale_lang").toString() );
	}

	setupUi();

	setupInstallerPages();

	if ( !installReady )
		m_butNext->setEnabled(false);
}

SystemInstaller::~SystemInstaller()
{
}

void SystemInstaller::setupUi()
{
	QWidget*		centralWidget( new QWidget(this) );

	// buttons
	QHBoxLayout*	hbox_buttons( new QHBoxLayout );

	m_butPrev = new QPushButton;
	m_butNext = new QPushButton;
	m_butFinish = new QPushButton;
	m_butAbout = new QPushButton;
	m_butFinish->setVisible(false);

	hbox_buttons->addWidget( m_butAbout );
	hbox_buttons->addStretch();
	hbox_buttons->addWidget( m_butPrev );
	hbox_buttons->addWidget( m_butNext );
	hbox_buttons->addWidget( m_butFinish );

	// right pannel = widget for pages
	m_stackPages = new QStackedWidget;

	QVBoxLayout*	vbox_1( new QVBoxLayout );
	QGroupBox*		group_box_page( new QGroupBox );
	QVBoxLayout*	group_box_page_l( new QVBoxLayout );
	group_box_page_l->addWidget( m_stackPages );

	group_box_page->setLayout( group_box_page_l );

	vbox_1->addWidget( group_box_page );

	// left pannel
	// logo
	m_labelImage = new QLabel;
	m_labelImage->setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );
	m_labelImage->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );
	m_labelImage->setMargin(10);

	m_labelPages  = new QLabel;

	QGroupBox*		group_box_list( new QGroupBox );
	QVBoxLayout*	group_box_list_l( new QVBoxLayout );
	group_box_list_l->addWidget( m_labelImage );
	group_box_list_l->addWidget( m_labelPages );
	group_box_list_l->addStretch();

	group_box_list->setLayout( group_box_list_l );

	// cmd pannel
	QGroupBox*		cmd_grp = new QGroupBox;
	m_labelInstCmd = new QLabel;
	m_labelImage->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
	QFont	fontCmd = QApplication::font();
	fontCmd.setFamily("Droid Sans Mono");
	m_labelInstCmd->setFont(fontCmd);
	m_labelInstCmd->setWordWrap(true);

	m_butCopy = new QToolButton;
	m_butCopy->setIcon( QIcon(":/img/edit-copy-4.png") );

	QHBoxLayout*	cmd_layout = new QHBoxLayout;
	cmd_layout->addWidget(m_labelInstCmd);
	cmd_layout->addWidget(m_butCopy);
	cmd_grp->setLayout(cmd_layout);

	// left + right pannels
	QHBoxLayout*	hbox_2( new QHBoxLayout );
	hbox_2->addWidget( group_box_list, 2 );
	hbox_2->addLayout( vbox_1, 9);

	QVBoxLayout*	vbox_main( new QVBoxLayout );
	vbox_main->addLayout( hbox_2 );
	vbox_main->addWidget( cmd_grp );
	vbox_main->addLayout( hbox_buttons );

	centralWidget->setLayout(vbox_main);
	setCentralWidget( centralWidget );

	// set windows icon
	QIcon	icon( ":/img/calculate-icon.png" );
	setWindowIcon(icon);

	// set window size
	selectWindowSize();

	QImage	logo = QImage(":/img/calculate-logo.png").scaledToWidth( 120, Qt::SmoothTransformation);
	m_labelImage->setPixmap( QPixmap::fromImage(logo) );

	m_actCopy = new QAction( this );
	//m_actCopy->setShortcut( QKeySequence("Ctrl+C") );
	connect( m_actCopy, SIGNAL(triggered()), this, SLOT(copyCmd()) );

	m_menuCopy = new QMenu(this);
	m_menuCopy->addAction(m_actCopy);

	m_labelInstCmd->setContextMenuPolicy(Qt::CustomContextMenu);
	connect( m_labelInstCmd, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showCopyMenu(QPoint)) );

	connect( m_butCopy, SIGNAL(clicked(bool)), this, SLOT(copyCmd()) );

	connect( m_butAbout, SIGNAL(clicked(bool)), this, SLOT(showAbout()) );

	connect( CalculateConfig::instance(), SIGNAL(sendParameters(QStringList)), this, SLOT(showCmd(QStringList)) );

	retranslateUi();
}

void SystemInstaller::setupInstallerPages()
{
	// create PageManager and pages
	m_PageManager.reset( new PageManager(m_stackPages, m_labelPages, this) );
	connect( m_PageManager.data(), SIGNAL(changeNext(bool)), this, SLOT(changeNext(bool)) );
	connect( m_PageManager.data(), SIGNAL(changePrev(bool)), this, SLOT(changePrev(bool)) );

	PageWelcome*	pageLanguage( new PageWelcome );
	connect( pageLanguage, SIGNAL(changeLanguage(QString)), this, SLOT(changeLanguage(QString)) );
	m_PageManager->addPage(pageLanguage);
/*
	PageLicense*	pageLicense( new PageLicense );
	m_PageManager->addPage(pageLicense);
*/
	PagePartitioning*	pagePartitoning( new PagePartitioning );
	connect( pagePartitoning, SIGNAL(manualyPartitioning(QString)), this, SLOT(doPartitioning(QString)) );
	m_PageManager->addPage(pagePartitoning);

	PageMountPoints* pageMountPoints( new PageMountPoints );
	m_PageManager->addPage(pageMountPoints);

	PageConfiguration* pageConfiguration( new PageConfiguration );
	m_PageManager->addPage(pageConfiguration);

	PageUsers* pageUsers( new PageUsers );
	m_PageManager->addPage( pageUsers );

	PageInstall* pageInstall( new PageInstall );
	m_PageManager->addPage(pageInstall);

	PageFinish* pageFinish( new PageFinish );
	connect( pageFinish, SIGNAL(toggleButtons(bool)), this, SLOT(toggleButtons(bool)) );
	m_PageManager->addPage(pageFinish);

	m_PageManager->showFirst();

	connect( m_butNext, SIGNAL(clicked()), m_PageManager.data(), SLOT(showNext()) );
	connect( m_butPrev, SIGNAL(clicked()), m_PageManager.data(), SLOT(showPrevious()) );
	connect( m_butFinish, SIGNAL(clicked()), this, SLOT(finishInstall()) );
}

void SystemInstaller::retranslateUi()
{
	m_butPrev->setText( tr("Previous") );
	m_butNext->setText( tr("Next") );
	m_butFinish->setText( tr("Finish") );
	m_butAbout->setText( tr("About") );

	m_actCopy->setText( tr("Copy") );

	setWindowTitle( tr("Calculate Linux installer") );

	if (m_PageManager)
		m_PageManager->retranslatePages();
}

void SystemInstaller::setTranslator(QTranslator* translator)
{
	removeTranslator();
	m_Translator = translator;
	qApp->installTranslator(translator);
}

void SystemInstaller::removeTranslator()
{
	if (m_Translator)
	{
		delete m_Translator;
		m_Translator = 0;
	}
}

void SystemInstaller::changeLanguage(QString lang)
{
	qDebug() << "New language: " + lang;

	m_CurrentLanguage = lang;

	// save global config for locale
	CalculateConfig::instance()->setValue( "gui_locale_language", QVariant(lang) );

	// My translator
	QTranslator*	translator( new QTranslator );
	translator->load("cl-install-gui_" + lang, "/usr/share/cl-install-gui/");

	setTranslator(translator);
}

void SystemInstaller::changeEvent(QEvent* event)
{
	switch (event->type())
	{
		case QEvent::LanguageChange:
			retranslateUi();
			break;
		default:
			break;
	}
	QWidget::changeEvent(event);
}

void SystemInstaller::closeEvent ( QCloseEvent* event )
{
	if (m_doFinish)
	{
		event->accept();
		return;
	}

	QMessageBox	quest(this);
	quest.setWindowTitle( tr("Attention") );
	quest.setText( tr("Do you want to abort the installation now?") );
	quest.setStandardButtons( QMessageBox::Yes | QMessageBox::No );
	quest.setIcon( QMessageBox::Question );
	quest.button(QMessageBox::Yes)->setText( tr("Yes") );
	quest.button(QMessageBox::No)->setText( tr("No") );

	if ( quest.exec() == QMessageBox::Yes )
	{
		qDebug() << "Canceled by user.";
		event->accept();
	}
	else
	{
		event->ignore();
	}
}


void SystemInstaller::changeNext(bool state)
{
	m_butNext->setEnabled(state);
}

void SystemInstaller::changePrev(bool state)
{
	m_butPrev->setEnabled(state);
}

void SystemInstaller::toggleButtons(bool state)
{
	m_butNext->setVisible(state);
	m_butPrev->setVisible(state);
	m_butFinish->setVisible(!state);
}

void SystemInstaller::doPartitioning(QString disk)
{
	m_PageCfdisk = new PageCfdisk( "cfdisk", disk );
	m_PageManager->showOnce(m_PageCfdisk);
	connect( m_PageCfdisk, SIGNAL(completed()), this, SLOT(completePartitioning()) );
}

void SystemInstaller::completePartitioning()
{
	delete m_PageCfdisk;
	CalculateConfig::instance()->getNewPartitioning();
	m_PageManager->showPrevious();
}

void SystemInstaller::showCopyMenu(const QPoint& point)
{
	m_menuCopy->popup( m_labelInstCmd->mapToGlobal(point) );
}

void SystemInstaller::copyCmd()
{
	QClipboard* clipboard = QApplication::clipboard();
	clipboard->setText( m_labelInstCmd->text() );
}

void SystemInstaller::showCmd( QStringList params )
{
	if (params.count() > 0)
	{
		QString	cmd("cl-install");

		foreach(const QString& param, params)
			cmd += " " + param;

		m_labelInstCmd->setText(cmd);
	}
	else
	{
		m_labelInstCmd->clear();
	}
}

void SystemInstaller::showAbout()
{
	QMessageBox::about(this, tr("About cl-install-gui"),
		tr(
			"<b>calculate-install-gui %1</b><br>"
			"GUI-frontend for cl-install<br>"
			"<br><br>"
			"Developer:<br>"
			"&nbsp;&nbsp;&nbsp;&nbsp;Ivan Loskutov aka vanner<br>"
			"<br>"
			"Translators:<br>"
			"&nbsp;&nbsp;&nbsp;&nbsp;Rosen Alexandrov aka ROKO__<br>"
			"&nbsp;&nbsp;&nbsp;&nbsp;Vadim Bosyuk aka Vados<br>"
		).arg(VER_STR)
	);
}

void SystemInstaller::finishInstall()
{
	qDebug() << "Installation complete.";
	m_doFinish = true;
	close();
}

void SystemInstaller::selectWindowSize()
{
	QRect	scr = QApplication::desktop()->screenGeometry();

	int w = 900;
	int h = 550;

	// minimum size for window
	setMinimumSize(w, h);
	setMaximumSize(w, h);
	move( scr.width() - scr.width()/2 - w/2, scr.height() - scr.height()/2 - h/2 );
}


