#include "pagepartitioning.h"

#include <QRadioButton>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>
#include <QBoxLayout>
#include <QScopedPointer>
#include <QMessageBox>

#include <QDebug>

#include "calculateconfig.h"

PagePartitioning::PagePartitioning() :
	InstallerPage( )
{
	setupUi();

	m_butExistPartitions->setChecked(true);

	connect( m_butManualPartitioning, SIGNAL(clicked()), this, SLOT(partitioningCurrentDisk()) );
	mapSignals();
}

void PagePartitioning::setupUi()
{
	// widgets
	m_labDisk = new QLabel;

	m_butExistPartitions = new QRadioButton;
	m_butAllDisk = new QRadioButton;
	m_butAllDisk->setEnabled(false);
	m_butManualPartitioning = new QPushButton;

	m_cmbxDisks = new QComboBox;

	// layouts
	QHBoxLayout*	hbox_1 = new QHBoxLayout;
	hbox_1->addWidget(m_labDisk);
	hbox_1->addWidget( m_cmbxDisks);

	QHBoxLayout*	hbox_2 = new QHBoxLayout;
	hbox_2->addWidget( m_butAllDisk );
	hbox_2->addStretch();

	QHBoxLayout*	hbox_3 = new QHBoxLayout;
	hbox_3->addWidget( m_butExistPartitions );
	hbox_3->addStretch();

	QHBoxLayout*	hbox_4 = new QHBoxLayout;
	hbox_4->addWidget( m_butManualPartitioning );
	hbox_4->addStretch();

	QVBoxLayout*	vbox_1 = new QVBoxLayout;
	vbox_1->addLayout(hbox_1);
	vbox_1->addLayout(hbox_2);
	vbox_1->addLayout(hbox_3);
	vbox_1->addLayout(hbox_4);
	vbox_1->addStretch();

	setLayout(vbox_1);

	retranslateUi();
}

void PagePartitioning::retranslateUi()
{
	setTitle( tr("Partitioning") );

	m_labDisk->setText( tr("Disk for install: ") );
	m_butExistPartitions->setText( tr("Use existing partitions") );
	m_butAllDisk->setText( tr("Use automatically partitioning") );
	m_butManualPartitioning->setText( tr("Manually partitioning") );

	m_cmbxDisks->setToolTip( tr("Selected disk will be used for manual or automatic partitioning.") );
}

bool PagePartitioning::validate()
{
	unmapSignals();
	return true;
}

void PagePartitioning::mapSignals()
{
	connect( m_cmbxDisks, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateData()) );
	connect( m_butAllDisk, SIGNAL(toggled(bool)), this, SLOT(updateData()) );
}

void PagePartitioning::unmapSignals()
{
	disconnect(m_cmbxDisks);
	disconnect(m_butAllDisk);
}

void PagePartitioning::show()
{
	unmapSignals();

	m_cmbxDisks->clear();

	QStringList disks = CalculateConfig::instance()->getValue("os_device_dev").toStringList();
	foreach(const QString& disk, disks)
		m_cmbxDisks->addItem(disk);

	if ( m_cmbxDisks->count() == 0 )
	{
		QMessageBox::critical(this, tr("Error"), tr("Disks not found"));
		emit changeNext(false);
	}

	updateData();

	mapSignals();
}

void PagePartitioning::partitioningCurrentDisk()
{
//	qDebug() << "Select: " << m_cmbxDisks->currentText();
	if ( m_cmbxDisks->count() != 0)
		emit manualyPartitioning( m_cmbxDisks->currentText() );
}

void PagePartitioning::updateData()
{
	// store selected options to CalculateConfig
	QString	partitioningVal = m_butAllDisk->isChecked() ? QString("auto") :	QString("manual");
	CalculateConfig::instance()->setValue("gui_partitioning", partitioningVal);

	if (m_butAllDisk->isChecked())
		CalculateConfig::instance()->setValue("--disk", QStringList() << m_cmbxDisks->currentText() );
	else
		CalculateConfig::instance()->setValue("--disk", QStringList() );


	CalculateConfig::instance()->showInstallParameters();
}


