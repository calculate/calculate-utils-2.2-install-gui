#include "pageconfiguration.h"

#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QApplication>
#include <QMessageBox>
#include <QScrollArea>
#include <QRegExp>
#include <QRegExpValidator>
#include <QPair>

#include <QDebug>

#include "calculateconfig.h"

PageConfiguration::PageConfiguration() :
	InstallerPage()
{
	setupUi();
}

void PageConfiguration::setupUi()
{
	QRegExp	rxName("[a-zA-Z][0-9a-zA-Z_\\-.]{0,63}");
	QRegExp	rxNameIP("[0-9a-zA-Z_\\-/:\\\\.]{0,128}");

	//
	m_labelSelectParams = new QLabel;

	// hostname
	QHBoxLayout*	hbox_hostname = new QHBoxLayout;
	m_labelHostName = new QLabel;
	m_editHostName = new QLineEdit;
	m_editHostName->setValidator( new QRegExpValidator(rxName, this) );
	m_editHostName->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	hbox_hostname->addWidget(m_labelHostName);
	hbox_hostname->addWidget(m_editHostName);

	// domain
	QHBoxLayout*	hbox_domain = new QHBoxLayout;
	m_labelDomainName = new QLabel;
	m_editDomainName = new QLineEdit;
	m_editDomainName->setValidator( new QRegExpValidator(rxName, this) );
	m_editDomainName->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	hbox_domain->addWidget(m_labelDomainName);
	hbox_domain->addWidget(m_editDomainName);

	// language
	QHBoxLayout*	hbox_language = new QHBoxLayout;
	m_labelLanguage = new QLabel;
	m_cmbboxLanguage = new QComboBox;
	hbox_language->addWidget(m_labelLanguage);
	hbox_language->addWidget(m_cmbboxLanguage);

	// timezone
	QHBoxLayout*	hbox_timezone = new QHBoxLayout;
	m_labelTimezone = new QLabel;
	m_cmbboxTimezone = new QComboBox;
	hbox_timezone->addWidget(m_labelTimezone);
	hbox_timezone->addWidget(m_cmbboxTimezone);

	// mbr device
	QHBoxLayout*	hbox_mbrdev = new QHBoxLayout;
	m_labelMbrDevice = new QLabel;
	m_cmbboxMbrDevice = new QComboBox;
	hbox_mbrdev->addWidget(m_labelMbrDevice);
	hbox_mbrdev->addWidget(m_cmbboxMbrDevice);

	//  #-video_drv-#         [w] "radeon" [fglrx, intel, nv, nvidia, vesa, vmware]
	QHBoxLayout*	hbox_videodrv = new QHBoxLayout;
	m_labelVideoDrv = new QLabel;
	m_cmbboxVideoDrv = new QComboBox;
	hbox_videodrv->addWidget(m_labelVideoDrv);
	hbox_videodrv->addWidget(m_cmbboxVideoDrv);

	// composite
	QHBoxLayout*	hbox_composite = new QHBoxLayout;
	m_labelComposite = new QLabel;
	m_chkboxComposite = new QCheckBox;
	m_chkboxComposite->setLayoutDirection( Qt::RightToLeft );
	hbox_composite->addWidget(m_labelComposite);
	hbox_composite->addWidget(m_chkboxComposite);

	// extended parameters
	m_chkboxExtParameters = new QCheckBox;
	m_widgetExtParameters = new QWidget;

	QVBoxLayout*	vbox_extparam = new QVBoxLayout;

	// builder
	QHBoxLayout*	hbox_builder = new QHBoxLayout;
	m_labelBuilder = new QLabel;
	m_chkboxBuilder = new QCheckBox;
	m_chkboxComposite->setLayoutDirection( Qt::RightToLeft );
	hbox_builder->addWidget(m_labelBuilder);
	hbox_builder->addWidget(m_chkboxBuilder);

	// makeopts
	QHBoxLayout*	hbox_makeopts = new QHBoxLayout;
	m_labelMakeOpts = new QLabel;
	m_editMakeOpts = new QLineEdit;
	m_editMakeOpts->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	hbox_makeopts->addWidget(m_labelMakeOpts);
	hbox_makeopts->addWidget(m_editMakeOpts);

	// proxy
	QHBoxLayout*	hbox_proxy = new QHBoxLayout;
	m_labelProxy = new QLabel;
	m_editProxy = new QLineEdit;
	m_editProxy->setValidator( new QRegExpValidator(rxNameIP, this) );
	m_editProxy->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	hbox_proxy->addWidget(m_labelProxy);
	hbox_proxy->addWidget(m_editProxy);

	// ntp
	QHBoxLayout*	hbox_ntp = new QHBoxLayout;
	m_labelNtp = new QLabel;
	m_editNtp = new QLineEdit;
	m_editNtp->setValidator( new QRegExpValidator(rxNameIP, this) );
	m_editNtp->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	hbox_ntp->addWidget(m_labelNtp);
	hbox_ntp->addWidget(m_editNtp);

	// clock type
	QHBoxLayout*	hbox_clockt = new QHBoxLayout;
	m_labelClockType = new QLabel;
	m_cmbboxClockType = new QComboBox;
	hbox_clockt->addWidget(m_labelClockType);
	hbox_clockt->addWidget(m_cmbboxClockType);

	vbox_extparam->addLayout(hbox_builder);
	vbox_extparam->addLayout(hbox_makeopts);
	vbox_extparam->addLayout(hbox_proxy);
	vbox_extparam->addLayout(hbox_ntp);
	vbox_extparam->addLayout(hbox_clockt);
	m_widgetExtParameters->setLayout(vbox_extparam);
	m_widgetExtParameters->setVisible(false);
	connect(m_chkboxExtParameters, SIGNAL(toggled(bool)), m_widgetExtParameters, SLOT(setVisible(bool))  );

	//
	QVBoxLayout* vbox_pageconf = new QVBoxLayout;
	vbox_pageconf->addWidget( m_labelSelectParams );
	vbox_pageconf->addLayout(hbox_hostname);
	vbox_pageconf->addLayout(hbox_domain);
	vbox_pageconf->addLayout(hbox_language);
	vbox_pageconf->addLayout(hbox_timezone);
	vbox_pageconf->addLayout(hbox_mbrdev);
	vbox_pageconf->addLayout(hbox_videodrv);
	vbox_pageconf->addLayout(hbox_composite);

	QFrame*	hline = new QFrame;
	hline->setFrameShape(QFrame::HLine);
	hline->setFrameShadow(QFrame::Sunken);

	vbox_pageconf->addWidget(hline);
	vbox_pageconf->addWidget(m_chkboxExtParameters);
	vbox_pageconf->addWidget(m_widgetExtParameters);

	vbox_pageconf->addStretch();

	QScrollArea*	scrArea = new QScrollArea;
	scrArea->setWidgetResizable(true);
	scrArea->setAutoFillBackground(false);

	QWidget*		scrWidg = new QWidget;
	QVBoxLayout*	scrWidgLayout = new QVBoxLayout(scrWidg);

	scrWidgLayout->addLayout(vbox_pageconf);

	scrWidg->setLayout(scrWidgLayout);

	scrArea->setWidget(scrWidg);
	scrArea->setFrameShadow(QFrame::Plain);
	scrArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

	QVBoxLayout*	vbox_scroll = new QVBoxLayout;
	vbox_scroll->addWidget(scrArea);

	setLayout( vbox_scroll );

	retranslateUi();
}

void PageConfiguration::mapSignals()
{
	connect(m_editHostName, SIGNAL(textChanged(QString)), this, SLOT( updateData()));
	connect(m_editDomainName, SIGNAL(textChanged(QString)), this, SLOT( updateData()));
	connect(m_cmbboxLanguage, SIGNAL(currentIndexChanged(int)), this, SLOT( updateData()));
	connect(m_cmbboxTimezone, SIGNAL(currentIndexChanged(int)), this, SLOT( updateData()));
	connect(m_cmbboxMbrDevice, SIGNAL(currentIndexChanged(int)), this, SLOT( updateData()));
	connect(m_cmbboxVideoDrv, SIGNAL(currentIndexChanged(int)), this, SLOT( updateData()));
	connect(m_chkboxComposite, SIGNAL(toggled(bool)), this, SLOT( updateData()));
	connect(m_chkboxBuilder, SIGNAL(toggled(bool)), this, SLOT( updateData()));
	connect(m_editMakeOpts, SIGNAL(textChanged(QString)), this, SLOT( updateData()));
	connect(m_editProxy, SIGNAL(textChanged(QString)), this, SLOT( updateData()));
	connect(m_editNtp, SIGNAL(textChanged(QString)), this, SLOT( updateData()));
	connect(m_cmbboxClockType, SIGNAL(currentIndexChanged(int)), this, SLOT( updateData()));
	connect(m_chkboxExtParameters, SIGNAL(toggled(bool)), this, SLOT( updateData()));
}

void PageConfiguration::unmapSignals()
{
	disconnect(m_editHostName);
	disconnect(m_editDomainName);
	disconnect(m_cmbboxLanguage);
	disconnect(m_cmbboxTimezone);
	disconnect(m_cmbboxMbrDevice);
	disconnect(m_cmbboxVideoDrv);
	disconnect(m_chkboxComposite);
	disconnect(m_chkboxBuilder);
	disconnect(m_editMakeOpts);
	disconnect(m_editProxy);
	disconnect(m_editNtp);
	disconnect(m_cmbboxClockType);
	disconnect(m_chkboxExtParameters);
}

void PageConfiguration::retranslateUi()
{
	setTitle( tr("Configuring") );

	m_labelSelectParams->setText( tr("Select parameters: ") );

	m_labelHostName->setText( tr("Hostname: ") );

	m_labelDomainName->setText( tr("Domain: ") );

	// lang
	m_langMap.clear();
	m_langMap["be_BY"] = tr("Belarusian");
	m_langMap["bg_BG"] = tr("Bulgarian");
	m_langMap["da_DK"] = tr("Danish");
	m_langMap["en_GB"] = tr("English [en_GB]");
	m_langMap["en_US"] = tr("English [en_US]");
	m_langMap["fr_BE"] = tr("French [fr_BE]");
	m_langMap["fr_CA"] = tr("French [fr_CA]");
	m_langMap["fr_FR"] = tr("French [fr_FR]");
	m_langMap["de_DE"] = tr("German");
	m_langMap["is_IS"] = tr("Icelandic");
	m_langMap["it_IT"] = tr("Italian");
	m_langMap["pl_PL"] = tr("Polish");
	m_langMap["pt_BR"] = tr("Portuguese");
	m_langMap["ru_RU"] = tr("Russian");
	m_langMap["sv_SE"] = tr("Swedish");
	m_langMap["es_ES"] = tr("Spanish");
	m_langMap["nn_NO"] = tr("Norwegian Nynorsk");
	m_langMap["uk_UA"] = tr("Ukrainian");

	m_labelLanguage->setText( tr("Language:") );
	m_cmbboxLanguage->clear();
	foreach(QString lang, CalculateConfig::instance()->getValue("os_lang").toStringList() )
	{
		if ( !m_langMap[lang].isEmpty() )
			m_cmbboxLanguage->addItem( m_langMap[lang], QVariant(lang) );
		else
			qDebug() << "Unsupported language " << lang;
	}

	m_labelTimezone->setText( tr("Timezone:") );
	setupTimezones();

	m_labelMbrDevice->setText( tr("Device for install Grub:") );
	m_cmbboxMbrDevice->clear();
	m_cmbboxMbrDevice->addItems( CalculateConfig::instance()->getValue("os_device_dev").toStringList() );
	m_cmbboxMbrDevice->addItem( "-" );

	m_labelVideoDrv->setText( tr("Video driver:") );
	m_cmbboxVideoDrv->clear();
	m_cmbboxVideoDrv->addItem("fglrx");
	m_cmbboxVideoDrv->addItem("intel");
	m_cmbboxVideoDrv->addItem("nv");
	m_cmbboxVideoDrv->addItem("nouveau");
	m_cmbboxVideoDrv->addItem("nvidia");
	m_cmbboxVideoDrv->addItem("radeon");
	m_cmbboxVideoDrv->addItem("vesa");
	m_cmbboxVideoDrv->addItem("vmware");

	m_labelComposite->setText( tr("Use desktop effects") );

	m_chkboxExtParameters->setText( tr("Expert settings") );

	m_labelBuilder->setText( tr("Installation for assembling") );

	m_labelMakeOpts->setText( tr("Make options (MAKEOPTS):") );

	m_labelProxy->setText( tr("Proxy server:") );

	m_labelNtp->setText( tr("NTP server:") );

	m_labelClockType->setText( tr("Clock type:") );
	m_cmbboxClockType->clear();
	m_cmbboxClockType->addItem( tr("Local"), QVariant("local") );
	m_cmbboxClockType->addItem( tr("UTC"), QVariant("UTC") );

}

bool PageConfiguration::validate()
{
	// check parameters
	if ( m_editHostName->text().isEmpty() )
	{
		QMessageBox::warning(this, tr("Warning"), tr("Hostname is empty.") );
		return false;
	}

	unmapSignals();

	return true;
}

void PageConfiguration::show()
{
	CalculateConfig*	clConf = CalculateConfig::instance();

	m_editHostName->setText( clConf->getValue("gui_os_install_net_hostname").toString() );

	m_editDomainName->setText( clConf->getValue("gui_os_install_net_domain").toString() );

	int langIndex = m_cmbboxLanguage->findData( clConf->getValue("gui_install_language") );
	if (langIndex >= 0)
		m_cmbboxLanguage->setCurrentIndex( langIndex );

	int timezoneIndex = m_cmbboxTimezone->findText(
		clConf->getValue("gui_os_install_clock_timezone").toString()
	);
	if (timezoneIndex >= 0)
		m_cmbboxTimezone->setCurrentIndex( timezoneIndex );

	int bootdevIndex = m_cmbboxMbrDevice->findText(
		clConf->getValue("gui_os_device_dev").toString()
	);
	if (bootdevIndex >= 0)
		m_cmbboxMbrDevice->setCurrentIndex( bootdevIndex );

	int videodrvIndex = m_cmbboxVideoDrv->findText(
		clConf->getValue("gui_os_install_x11_video_drv").toString()
	);
	if (videodrvIndex >= 0)
		m_cmbboxVideoDrv->setCurrentIndex( videodrvIndex );

	m_chkboxComposite->setChecked(
		clConf->getValue("gui_os_install_x11_composite").toString() == "on"
	);

	m_chkboxBuilder->setChecked(
		clConf->getValue("gui_os_install_builder").toString() == "on"
	);

	m_editMakeOpts->setText( clConf->getValue("gui_os_install_makeopts").toString() );

	m_editProxy->setText( clConf->getValue("gui_os_install_proxy").toString() );

	m_editNtp->setText( clConf->getValue("gui_os_install_ntp").toString() );

	int clocktypeIndex = m_cmbboxClockType->findData(
		clConf->getValue("gui_os_install_clock_type").toString()
	);
	if (clocktypeIndex >= 0)
		m_cmbboxClockType->setCurrentIndex( clocktypeIndex );

	mapSignals();
}

void PageConfiguration::updateData()
{
	CalculateConfig*	clConf = CalculateConfig::instance();

	clConf->setValue("gui_os_install_net_hostname", m_editHostName->text() );
	clConf->setValue("gui_os_install_net_domain", m_editDomainName->text() );
	clConf->setValue("gui_install_language", m_cmbboxLanguage->itemData( m_cmbboxLanguage->currentIndex() ) );
	clConf->setValue("gui_os_install_clock_timezone", m_cmbboxTimezone->currentText() );
	clConf->setValue("gui_os_device_dev", m_cmbboxMbrDevice->currentText() );
	clConf->setValue("gui_os_install_x11_video_drv", m_cmbboxVideoDrv->currentText() );
	clConf->setValue("gui_os_install_x11_composite", m_chkboxComposite->isChecked() ? "on" : "off" );

	clConf->setValue("gui_expert_mode", m_chkboxExtParameters->isChecked() ? "on" : "off" );
	clConf->setValue("gui_os_install_builder", m_chkboxBuilder->isChecked() ? "on" : "off" );
	clConf->setValue("gui_os_install_makeopts", m_editMakeOpts->text() );
	clConf->setValue("gui_os_install_proxy", m_editProxy->text() );
	clConf->setValue("gui_os_install_ntp", m_editNtp->text() );
	clConf->setValue("gui_os_install_clock_type", m_cmbboxClockType->itemData( m_cmbboxClockType->currentIndex() ) );

	clConf->showInstallParameters();
}

void PageConfiguration::setupTimezones()
{
	m_cmbboxTimezone->clear();
	m_cmbboxTimezone->addItem( "Africa/Abidjan" );
	m_cmbboxTimezone->addItem( "Africa/Accra" );
	m_cmbboxTimezone->addItem( "Africa/Addis_Ababa" );
	m_cmbboxTimezone->addItem( "Africa/Algiers" );
	m_cmbboxTimezone->addItem( "Africa/Asmara" );
	m_cmbboxTimezone->addItem( "Africa/Asmera" );
	m_cmbboxTimezone->addItem( "Africa/Bamako" );
	m_cmbboxTimezone->addItem( "Africa/Bangui" );
	m_cmbboxTimezone->addItem( "Africa/Banjul" );
	m_cmbboxTimezone->addItem( "Africa/Bissau" );
	m_cmbboxTimezone->addItem( "Africa/Blantyre" );
	m_cmbboxTimezone->addItem( "Africa/Brazzaville" );
	m_cmbboxTimezone->addItem( "Africa/Bujumbura" );
	m_cmbboxTimezone->addItem( "Africa/Cairo" );
	m_cmbboxTimezone->addItem( "Africa/Casablanca" );
	m_cmbboxTimezone->addItem( "Africa/Ceuta" );
	m_cmbboxTimezone->addItem( "Africa/Conakry" );
	m_cmbboxTimezone->addItem( "Africa/Dakar" );
	m_cmbboxTimezone->addItem( "Africa/Dar_es_Salaam" );
	m_cmbboxTimezone->addItem( "Africa/Djibouti" );
	m_cmbboxTimezone->addItem( "Africa/Douala" );
	m_cmbboxTimezone->addItem( "Africa/El_Aaiun" );
	m_cmbboxTimezone->addItem( "Africa/Freetown" );
	m_cmbboxTimezone->addItem( "Africa/Gaborone" );
	m_cmbboxTimezone->addItem( "Africa/Harare" );
	m_cmbboxTimezone->addItem( "Africa/Johannesburg" );
	m_cmbboxTimezone->addItem( "Africa/Kampala" );
	m_cmbboxTimezone->addItem( "Africa/Khartoum" );
	m_cmbboxTimezone->addItem( "Africa/Kigali" );
	m_cmbboxTimezone->addItem( "Africa/Kinshasa" );
	m_cmbboxTimezone->addItem( "Africa/Lagos" );
	m_cmbboxTimezone->addItem( "Africa/Libreville" );
	m_cmbboxTimezone->addItem( "Africa/Lome" );
	m_cmbboxTimezone->addItem( "Africa/Luanda" );
	m_cmbboxTimezone->addItem( "Africa/Lubumbashi" );
	m_cmbboxTimezone->addItem( "Africa/Lusaka" );
	m_cmbboxTimezone->addItem( "Africa/Malabo" );
	m_cmbboxTimezone->addItem( "Africa/Maputo" );
	m_cmbboxTimezone->addItem( "Africa/Maseru" );
	m_cmbboxTimezone->addItem( "Africa/Mbabane" );
	m_cmbboxTimezone->addItem( "Africa/Mogadishu" );
	m_cmbboxTimezone->addItem( "Africa/Monrovia" );
	m_cmbboxTimezone->addItem( "Africa/Nairobi" );
	m_cmbboxTimezone->addItem( "Africa/Ndjamena" );
	m_cmbboxTimezone->addItem( "Africa/Niamey" );
	m_cmbboxTimezone->addItem( "Africa/Nouakchott" );
	m_cmbboxTimezone->addItem( "Africa/Ouagadougou" );
	m_cmbboxTimezone->addItem( "Africa/Porto-Novo" );
	m_cmbboxTimezone->addItem( "Africa/Sao_Tome" );
	m_cmbboxTimezone->addItem( "Africa/Timbuktu" );
	m_cmbboxTimezone->addItem( "Africa/Tripoli" );
	m_cmbboxTimezone->addItem( "Africa/Tunis" );
	m_cmbboxTimezone->addItem( "Africa/Windhoek" );
	m_cmbboxTimezone->addItem( "America/Adak" );
	m_cmbboxTimezone->addItem( "America/Anchorage" );
	m_cmbboxTimezone->addItem( "America/Anguilla" );
	m_cmbboxTimezone->addItem( "America/Antigua" );
	m_cmbboxTimezone->addItem( "America/Araguaina" );
	m_cmbboxTimezone->addItem( "America/Argentina/Buenos_Aires" );
	m_cmbboxTimezone->addItem( "America/Argentina/Catamarca" );
	m_cmbboxTimezone->addItem( "America/Argentina/ComodRivadavia" );
	m_cmbboxTimezone->addItem( "America/Argentina/Cordoba" );
	m_cmbboxTimezone->addItem( "America/Argentina/Jujuy" );
	m_cmbboxTimezone->addItem( "America/Argentina/La_Rioja" );
	m_cmbboxTimezone->addItem( "America/Argentina/Mendoza" );
	m_cmbboxTimezone->addItem( "America/Argentina/Rio_Gallegos" );
	m_cmbboxTimezone->addItem( "America/Argentina/Salta" );
	m_cmbboxTimezone->addItem( "America/Argentina/San_Juan" );
	m_cmbboxTimezone->addItem( "America/Argentina/San_Luis" );
	m_cmbboxTimezone->addItem( "America/Argentina/Tucuman" );
	m_cmbboxTimezone->addItem( "America/Argentina/Ushuaia" );
	m_cmbboxTimezone->addItem( "America/Aruba" );
	m_cmbboxTimezone->addItem( "America/Asuncion" );
	m_cmbboxTimezone->addItem( "America/Atikokan" );
	m_cmbboxTimezone->addItem( "America/Atka" );
	m_cmbboxTimezone->addItem( "America/Bahia" );
	m_cmbboxTimezone->addItem( "America/Barbados" );
	m_cmbboxTimezone->addItem( "America/Belem" );
	m_cmbboxTimezone->addItem( "America/Belize" );
	m_cmbboxTimezone->addItem( "America/Blanc-Sablon" );
	m_cmbboxTimezone->addItem( "America/Boa_Vista" );
	m_cmbboxTimezone->addItem( "America/Bogota" );
	m_cmbboxTimezone->addItem( "America/Boise" );
	m_cmbboxTimezone->addItem( "America/Buenos_Aires" );
	m_cmbboxTimezone->addItem( "America/Cambridge_Bay" );
	m_cmbboxTimezone->addItem( "America/Campo_Grande" );
	m_cmbboxTimezone->addItem( "America/Cancun" );
	m_cmbboxTimezone->addItem( "America/Caracas" );
	m_cmbboxTimezone->addItem( "America/Catamarca" );
	m_cmbboxTimezone->addItem( "America/Cayenne" );
	m_cmbboxTimezone->addItem( "America/Cayman" );
	m_cmbboxTimezone->addItem( "America/Chicago" );
	m_cmbboxTimezone->addItem( "America/Chihuahua  " );
	m_cmbboxTimezone->addItem( "America/Coral_Harbour" );
	m_cmbboxTimezone->addItem( "America/Cordoba" );
	m_cmbboxTimezone->addItem( "America/Costa_Rica" );
	m_cmbboxTimezone->addItem( "America/Cuiaba" );
	m_cmbboxTimezone->addItem( "America/Curacao" );
	m_cmbboxTimezone->addItem( "America/Danmarkshavn" );
	m_cmbboxTimezone->addItem( "America/Dawson" );
	m_cmbboxTimezone->addItem( "America/Dawson_Creek" );
	m_cmbboxTimezone->addItem( "America/Denver" );
	m_cmbboxTimezone->addItem( "America/Detroit" );
	m_cmbboxTimezone->addItem( "America/Dominica" );
	m_cmbboxTimezone->addItem( "America/Edmonton" );
	m_cmbboxTimezone->addItem( "America/Eirunepe" );
	m_cmbboxTimezone->addItem( "America/El_Salvador" );
	m_cmbboxTimezone->addItem( "America/Ensenada" );
	m_cmbboxTimezone->addItem( "America/Fortaleza" );
	m_cmbboxTimezone->addItem( "America/Fort_Wayne" );
	m_cmbboxTimezone->addItem( "America/Glace_Bay" );
	m_cmbboxTimezone->addItem( "America/Godthab" );
	m_cmbboxTimezone->addItem( "America/Goose_Bay" );
	m_cmbboxTimezone->addItem( "America/Grand_Turk" );
	m_cmbboxTimezone->addItem( "America/Grenada" );
	m_cmbboxTimezone->addItem( "America/Guadeloupe" );
	m_cmbboxTimezone->addItem( "America/Guatemala" );
	m_cmbboxTimezone->addItem( "America/Guayaquil" );
	m_cmbboxTimezone->addItem( "America/Guyana" );
	m_cmbboxTimezone->addItem( "America/Halifax" );
	m_cmbboxTimezone->addItem( "America/Havana" );
	m_cmbboxTimezone->addItem( "America/Hermosillo" );
	m_cmbboxTimezone->addItem( "America/Indiana/Indianapolis" );
	m_cmbboxTimezone->addItem( "America/Indiana/Knox" );
	m_cmbboxTimezone->addItem( "America/Indiana/Marengo" );
	m_cmbboxTimezone->addItem( "America/Indiana/Petersburg" );
	m_cmbboxTimezone->addItem( "America/Indiana/Tell_City" );
	m_cmbboxTimezone->addItem( "America/Indiana/Vevay" );
	m_cmbboxTimezone->addItem( "America/Indiana/Vincennes" );
	m_cmbboxTimezone->addItem( "America/Indiana/Winamac" );
	m_cmbboxTimezone->addItem( "America/Indianapolis" );
	m_cmbboxTimezone->addItem( "America/Inuvik" );
	m_cmbboxTimezone->addItem( "America/Iqaluit" );
	m_cmbboxTimezone->addItem( "America/Jamaica" );
	m_cmbboxTimezone->addItem( "America/Jujuy" );
	m_cmbboxTimezone->addItem( "America/Juneau" );
	m_cmbboxTimezone->addItem( "America/Kentucky/Louisville" );
	m_cmbboxTimezone->addItem( "America/Kentucky/Monticello" );
	m_cmbboxTimezone->addItem( "America/Knox_IN" );
	m_cmbboxTimezone->addItem( "America/La_Paz" );
	m_cmbboxTimezone->addItem( "America/Lima" );
	m_cmbboxTimezone->addItem( "America/Los_Angeles" );
	m_cmbboxTimezone->addItem( "America/Louisville" );
	m_cmbboxTimezone->addItem( "America/Maceio" );
	m_cmbboxTimezone->addItem( "America/Managua" );
	m_cmbboxTimezone->addItem( "America/Manaus" );
	m_cmbboxTimezone->addItem( "America/Marigot" );
	m_cmbboxTimezone->addItem( "America/Martinique" );
	m_cmbboxTimezone->addItem( "America/Matamoros" );
	m_cmbboxTimezone->addItem( "America/Mazatlan" );
	m_cmbboxTimezone->addItem( "America/Mendoza" );
	m_cmbboxTimezone->addItem( "America/Menominee" );
	m_cmbboxTimezone->addItem( "America/Merida" );
	m_cmbboxTimezone->addItem( "America/Mexico_City" );
	m_cmbboxTimezone->addItem( "America/Miquelon" );
	m_cmbboxTimezone->addItem( "America/Moncton" );
	m_cmbboxTimezone->addItem( "America/Monterrey" );
	m_cmbboxTimezone->addItem( "America/Montevideo" );
	m_cmbboxTimezone->addItem( "America/Montreal" );
	m_cmbboxTimezone->addItem( "America/Montserrat" );
	m_cmbboxTimezone->addItem( "America/Nassau" );
	m_cmbboxTimezone->addItem( "America/New_York" );
	m_cmbboxTimezone->addItem( "America/Nipigon" );
	m_cmbboxTimezone->addItem( "America/Nome" );
	m_cmbboxTimezone->addItem( "America/Noronha" );
	m_cmbboxTimezone->addItem( "America/North_Dakota/Center" );
	m_cmbboxTimezone->addItem( "America/North_Dakota/New_Salem" );
	m_cmbboxTimezone->addItem( "America/Ojinaga" );
	m_cmbboxTimezone->addItem( "America/Panama" );
	m_cmbboxTimezone->addItem( "America/Pangnirtung" );
	m_cmbboxTimezone->addItem( "America/Paramaribo" );
	m_cmbboxTimezone->addItem( "America/Phoenix" );
	m_cmbboxTimezone->addItem( "America/Port-au-Prince" );
	m_cmbboxTimezone->addItem( "America/Porto_Acre" );
	m_cmbboxTimezone->addItem( "America/Port_of_Spain" );
	m_cmbboxTimezone->addItem( "America/Porto_Velho" );
	m_cmbboxTimezone->addItem( "America/Puerto_Rico" );
	m_cmbboxTimezone->addItem( "America/Rainy_River" );
	m_cmbboxTimezone->addItem( "America/Rankin_Inlet" );
	m_cmbboxTimezone->addItem( "America/Recife" );
	m_cmbboxTimezone->addItem( "America/Regina" );
	m_cmbboxTimezone->addItem( "America/Resolute" );
	m_cmbboxTimezone->addItem( "America/Rio_Branco" );
	m_cmbboxTimezone->addItem( "America/Rosario" );
	m_cmbboxTimezone->addItem( "America/Santa_Isabel" );
	m_cmbboxTimezone->addItem( "America/Santarem" );
	m_cmbboxTimezone->addItem( "America/Santiago" );
	m_cmbboxTimezone->addItem( "America/Santo_Domingo" );
	m_cmbboxTimezone->addItem( "America/Sao_Paulo" );
	m_cmbboxTimezone->addItem( "America/Scoresbysund" );
	m_cmbboxTimezone->addItem( "America/Shiprock" );
	m_cmbboxTimezone->addItem( "America/St_Barthelemy" );
	m_cmbboxTimezone->addItem( "America/St_Johns" );
	m_cmbboxTimezone->addItem( "America/St_Kitts" );
	m_cmbboxTimezone->addItem( "America/St_Lucia" );
	m_cmbboxTimezone->addItem( "America/St_Thomas" );
	m_cmbboxTimezone->addItem( "America/St_Vincent" );
	m_cmbboxTimezone->addItem( "America/Swift_Current" );
	m_cmbboxTimezone->addItem( "America/Tegucigalpa" );
	m_cmbboxTimezone->addItem( "America/Thule" );
	m_cmbboxTimezone->addItem( "America/Thunder_Bay" );
	m_cmbboxTimezone->addItem( "America/Tijuana" );
	m_cmbboxTimezone->addItem( "America/Toronto" );
	m_cmbboxTimezone->addItem( "America/Tortola" );
	m_cmbboxTimezone->addItem( "America/Vancouver" );
	m_cmbboxTimezone->addItem( "America/Virgin" );
	m_cmbboxTimezone->addItem( "America/Whitehorse" );
	m_cmbboxTimezone->addItem( "America/Winnipeg" );
	m_cmbboxTimezone->addItem( "America/Yakutat" );
	m_cmbboxTimezone->addItem( "America/Yellowknife" );
	m_cmbboxTimezone->addItem( "Antarctica/Casey" );
	m_cmbboxTimezone->addItem( "Antarctica/Davis" );
	m_cmbboxTimezone->addItem( "Antarctica/DumontDUrville" );
	m_cmbboxTimezone->addItem( "Antarctica/Macquarie" );
	m_cmbboxTimezone->addItem( "Antarctica/Mawson" );
	m_cmbboxTimezone->addItem( "Antarctica/McMurdo" );
	m_cmbboxTimezone->addItem( "Antarctica/Palmer" );
	m_cmbboxTimezone->addItem( "Antarctica/Rothera" );
	m_cmbboxTimezone->addItem( "Antarctica/South_Pole" );
	m_cmbboxTimezone->addItem( "Antarctica/Syowa" );
	m_cmbboxTimezone->addItem( "Antarctica/Vostok" );
	m_cmbboxTimezone->addItem( "Arctic/Longyearbyen" );
	m_cmbboxTimezone->addItem( "Asia/Aden" );
	m_cmbboxTimezone->addItem( "Asia/Almaty" );
	m_cmbboxTimezone->addItem( "Asia/Amman" );
	m_cmbboxTimezone->addItem( "Asia/Anadyr" );
	m_cmbboxTimezone->addItem( "Asia/Aqtau" );
	m_cmbboxTimezone->addItem( "Asia/Aqtobe" );
	m_cmbboxTimezone->addItem( "Asia/Ashgabat" );
	m_cmbboxTimezone->addItem( "Asia/Ashkhabad" );
	m_cmbboxTimezone->addItem( "Asia/Baghdad" );
	m_cmbboxTimezone->addItem( "Asia/Bahrain" );
	m_cmbboxTimezone->addItem( "Asia/Baku" );
	m_cmbboxTimezone->addItem( "Asia/Bangkok" );
	m_cmbboxTimezone->addItem( "Asia/Beirut" );
	m_cmbboxTimezone->addItem( "Asia/Bishkek" );
	m_cmbboxTimezone->addItem( "Asia/Brunei" );
	m_cmbboxTimezone->addItem( "Asia/Calcutta" );
	m_cmbboxTimezone->addItem( "Asia/Choibalsan" );
	m_cmbboxTimezone->addItem( "Asia/Chongqing" );
	m_cmbboxTimezone->addItem( "Asia/Chungking" );
	m_cmbboxTimezone->addItem( "Asia/Colombo" );
	m_cmbboxTimezone->addItem( "Asia/Dacca" );
	m_cmbboxTimezone->addItem( "Asia/Damascus" );
	m_cmbboxTimezone->addItem( "Asia/Dhaka" );
	m_cmbboxTimezone->addItem( "Asia/Dili" );
	m_cmbboxTimezone->addItem( "Asia/Dubai" );
	m_cmbboxTimezone->addItem( "Asia/Dushanbe" );
	m_cmbboxTimezone->addItem( "Asia/Gaza" );
	m_cmbboxTimezone->addItem( "Asia/Harbin" );
	m_cmbboxTimezone->addItem( "Asia/Ho_Chi_Minh" );
	m_cmbboxTimezone->addItem( "Asia/Hong_Kong" );
	m_cmbboxTimezone->addItem( "Asia/Hovd" );
	m_cmbboxTimezone->addItem( "Asia/Irkutsk" );
	m_cmbboxTimezone->addItem( "Asia/Istanbul" );
	m_cmbboxTimezone->addItem( "Asia/Jakarta" );
	m_cmbboxTimezone->addItem( "Asia/Jayapura" );
	m_cmbboxTimezone->addItem( "Asia/Jerusalem" );
	m_cmbboxTimezone->addItem( "Asia/Kabul" );
	m_cmbboxTimezone->addItem( "Asia/Kamchatka" );
	m_cmbboxTimezone->addItem( "Asia/Karachi" );
	m_cmbboxTimezone->addItem( "Asia/Kashgar" );
	m_cmbboxTimezone->addItem( "Asia/Kathmandu" );
	m_cmbboxTimezone->addItem( "Asia/Katmandu" );
	m_cmbboxTimezone->addItem( "Asia/Kolkata" );
	m_cmbboxTimezone->addItem( "Asia/Krasnoyarsk" );
	m_cmbboxTimezone->addItem( "Asia/Kuala_Lumpur" );
	m_cmbboxTimezone->addItem( "Asia/Kuching" );
	m_cmbboxTimezone->addItem( "Asia/Kuwait" );
	m_cmbboxTimezone->addItem( "Asia/Macao" );
	m_cmbboxTimezone->addItem( "Asia/Macau" );
	m_cmbboxTimezone->addItem( "Asia/Magadan" );
	m_cmbboxTimezone->addItem( "Asia/Makassar" );
	m_cmbboxTimezone->addItem( "Asia/Manila" );
	m_cmbboxTimezone->addItem( "Asia/Muscat" );
	m_cmbboxTimezone->addItem( "Asia/Nicosia" );
	m_cmbboxTimezone->addItem( "Asia/Novokuznetsk" );
	m_cmbboxTimezone->addItem( "Asia/Novosibirsk" );
	m_cmbboxTimezone->addItem( "Asia/Omsk" );
	m_cmbboxTimezone->addItem( "Asia/Oral" );
	m_cmbboxTimezone->addItem( "Asia/Phnom_Penh" );
	m_cmbboxTimezone->addItem( "Asia/Pontianak" );
	m_cmbboxTimezone->addItem( "Asia/Pyongyang" );
	m_cmbboxTimezone->addItem( "Asia/Qatar" );
	m_cmbboxTimezone->addItem( "Asia/Qyzylorda" );
	m_cmbboxTimezone->addItem( "Asia/Rangoon" );
	m_cmbboxTimezone->addItem( "Asia/Riyadh" );
	m_cmbboxTimezone->addItem( "Asia/Riyadh87" );
	m_cmbboxTimezone->addItem( "Asia/Riyadh88" );
	m_cmbboxTimezone->addItem( "Asia/Riyadh89" );
	m_cmbboxTimezone->addItem( "Asia/Saigon" );
	m_cmbboxTimezone->addItem( "Asia/Sakhalin" );
	m_cmbboxTimezone->addItem( "Asia/Samarkand" );
	m_cmbboxTimezone->addItem( "Asia/Seoul" );
	m_cmbboxTimezone->addItem( "Asia/Shanghai" );
	m_cmbboxTimezone->addItem( "Asia/Singapore" );
	m_cmbboxTimezone->addItem( "Asia/Taipei" );
	m_cmbboxTimezone->addItem( "Asia/Tashkent" );
	m_cmbboxTimezone->addItem( "Asia/Tbilisi" );
	m_cmbboxTimezone->addItem( "Asia/Tehran" );
	m_cmbboxTimezone->addItem( "Asia/Tel_Aviv" );
	m_cmbboxTimezone->addItem( "Asia/Thimbu" );
	m_cmbboxTimezone->addItem( "Asia/Thimphu" );
	m_cmbboxTimezone->addItem( "Asia/Tokyo" );
	m_cmbboxTimezone->addItem( "Asia/Ujung_Pandang" );
	m_cmbboxTimezone->addItem( "Asia/Ulaanbaatar" );
	m_cmbboxTimezone->addItem( "Asia/Ulan_Bator" );
	m_cmbboxTimezone->addItem( "Asia/Urumqi" );
	m_cmbboxTimezone->addItem( "Asia/Vientiane" );
	m_cmbboxTimezone->addItem( "Asia/Vladivostok" );
	m_cmbboxTimezone->addItem( "Asia/Yakutsk" );
	m_cmbboxTimezone->addItem( "Asia/Yekaterinburg" );
	m_cmbboxTimezone->addItem( "Asia/Yerevan" );
	m_cmbboxTimezone->addItem( "Atlantic/Azores" );
	m_cmbboxTimezone->addItem( "Atlantic/Bermuda" );
	m_cmbboxTimezone->addItem( "Atlantic/Canary" );
	m_cmbboxTimezone->addItem( "Atlantic/Cape_Verde" );
	m_cmbboxTimezone->addItem( "Atlantic/Faeroe" );
	m_cmbboxTimezone->addItem( "Atlantic/Faroe" );
	m_cmbboxTimezone->addItem( "Atlantic/Jan_Mayen" );
	m_cmbboxTimezone->addItem( "Atlantic/Madeira" );
	m_cmbboxTimezone->addItem( "Atlantic/Reykjavik" );
	m_cmbboxTimezone->addItem( "Atlantic/South_Georgia" );
	m_cmbboxTimezone->addItem( "Atlantic/Stanley" );
	m_cmbboxTimezone->addItem( "Atlantic/St_Helena" );
	m_cmbboxTimezone->addItem( "Australia/ACT" );
	m_cmbboxTimezone->addItem( "Australia/Adelaide" );
	m_cmbboxTimezone->addItem( "Australia/Brisbane" );
	m_cmbboxTimezone->addItem( "Australia/Broken_Hill" );
	m_cmbboxTimezone->addItem( "Australia/Canberra" );
	m_cmbboxTimezone->addItem( "Australia/Currie" );
	m_cmbboxTimezone->addItem( "Australia/Darwin" );
	m_cmbboxTimezone->addItem( "Australia/Eucla" );
	m_cmbboxTimezone->addItem( "Australia/Hobart" );
	m_cmbboxTimezone->addItem( "Australia/LHI" );
	m_cmbboxTimezone->addItem( "Australia/Lindeman" );
	m_cmbboxTimezone->addItem( "Australia/Lord_Howe" );
	m_cmbboxTimezone->addItem( "Australia/Melbourne" );
	m_cmbboxTimezone->addItem( "Australia/North" );
	m_cmbboxTimezone->addItem( "Australia/NSW" );
	m_cmbboxTimezone->addItem( "Australia/Perth" );
	m_cmbboxTimezone->addItem( "Australia/Queensland" );
	m_cmbboxTimezone->addItem( "Australia/South" );
	m_cmbboxTimezone->addItem( "Australia/Sydney" );
	m_cmbboxTimezone->addItem( "Australia/Tasmania" );
	m_cmbboxTimezone->addItem( "Australia/Victoria" );
	m_cmbboxTimezone->addItem( "Australia/West" );
	m_cmbboxTimezone->addItem( "Australia/Yancowinna" );
	m_cmbboxTimezone->addItem( "Brazil/Acre" );
	m_cmbboxTimezone->addItem( "Brazil/DeNoronha" );
	m_cmbboxTimezone->addItem( "Brazil/East" );
	m_cmbboxTimezone->addItem( "Brazil/West" );
	m_cmbboxTimezone->addItem( "Canada/Atlantic" );
	m_cmbboxTimezone->addItem( "Canada/Central" );
	m_cmbboxTimezone->addItem( "Canada/Eastern" );
	m_cmbboxTimezone->addItem( "Canada/East-Saskatchewan" );
	m_cmbboxTimezone->addItem( "Canada/Mountain" );
	m_cmbboxTimezone->addItem( "Canada/Newfoundland" );
	m_cmbboxTimezone->addItem( "Canada/Pacific" );
	m_cmbboxTimezone->addItem( "Canada/Saskatchewan" );
	m_cmbboxTimezone->addItem( "Canada/Yukon" );
	m_cmbboxTimezone->addItem( "Chile/Continental" );
	m_cmbboxTimezone->addItem( "Chile/EasterIsland" );
	m_cmbboxTimezone->addItem( "Etc/GMT" );
	m_cmbboxTimezone->addItem( "Etc/GMT0" );
	m_cmbboxTimezone->addItem( "Etc/GMT-0" );
	m_cmbboxTimezone->addItem( "Etc/GMT+0" );
	m_cmbboxTimezone->addItem( "Etc/GMT-1" );
	m_cmbboxTimezone->addItem( "Etc/GMT+1" );
	m_cmbboxTimezone->addItem( "Etc/GMT-10" );
	m_cmbboxTimezone->addItem( "Etc/GMT+10" );
	m_cmbboxTimezone->addItem( "Etc/GMT-11" );
	m_cmbboxTimezone->addItem( "Etc/GMT+11" );
	m_cmbboxTimezone->addItem( "Etc/GMT-12" );
	m_cmbboxTimezone->addItem( "Etc/GMT+12" );
	m_cmbboxTimezone->addItem( "Etc/GMT-13" );
	m_cmbboxTimezone->addItem( "Etc/GMT-14" );
	m_cmbboxTimezone->addItem( "Etc/GMT-2" );
	m_cmbboxTimezone->addItem( "Etc/GMT+2" );
	m_cmbboxTimezone->addItem( "Etc/GMT-3" );
	m_cmbboxTimezone->addItem( "Etc/GMT+3" );
	m_cmbboxTimezone->addItem( "Etc/GMT-4" );
	m_cmbboxTimezone->addItem( "Etc/GMT+4" );
	m_cmbboxTimezone->addItem( "Etc/GMT-5" );
	m_cmbboxTimezone->addItem( "Etc/GMT+5" );
	m_cmbboxTimezone->addItem( "Etc/GMT-6" );
	m_cmbboxTimezone->addItem( "Etc/GMT+6" );
	m_cmbboxTimezone->addItem( "Etc/GMT-7" );
	m_cmbboxTimezone->addItem( "Etc/GMT+7" );
	m_cmbboxTimezone->addItem( "Etc/GMT-8" );
	m_cmbboxTimezone->addItem( "Etc/GMT+8" );
	m_cmbboxTimezone->addItem( "Etc/GMT-9" );
	m_cmbboxTimezone->addItem( "Etc/GMT+9" );
	m_cmbboxTimezone->addItem( "Etc/Greenwich" );
	m_cmbboxTimezone->addItem( "Etc/UCT" );
	m_cmbboxTimezone->addItem( "Etc/Universal" );
	m_cmbboxTimezone->addItem( "Etc/UTC" );
	m_cmbboxTimezone->addItem( "Etc/Zulu" );
	m_cmbboxTimezone->addItem( "Europe/Amsterdam" );
	m_cmbboxTimezone->addItem( "Europe/Andorra" );
	m_cmbboxTimezone->addItem( "Europe/Athens" );
	m_cmbboxTimezone->addItem( "Europe/Belfast" );
	m_cmbboxTimezone->addItem( "Europe/Belgrade" );
	m_cmbboxTimezone->addItem( "Europe/Berlin" );
	m_cmbboxTimezone->addItem( "Europe/Bratislava" );
	m_cmbboxTimezone->addItem( "Europe/Brussels" );
	m_cmbboxTimezone->addItem( "Europe/Bucharest" );
	m_cmbboxTimezone->addItem( "Europe/Budapest" );
	m_cmbboxTimezone->addItem( "Europe/Chisinau" );
	m_cmbboxTimezone->addItem( "Europe/Copenhagen" );
	m_cmbboxTimezone->addItem( "Europe/Dublin" );
	m_cmbboxTimezone->addItem( "Europe/Gibraltar" );
	m_cmbboxTimezone->addItem( "Europe/Guernsey" );
	m_cmbboxTimezone->addItem( "Europe/Helsinki" );
	m_cmbboxTimezone->addItem( "Europe/Isle_of_Man" );
	m_cmbboxTimezone->addItem( "Europe/Istanbul" );
	m_cmbboxTimezone->addItem( "Europe/Jersey" );
	m_cmbboxTimezone->addItem( "Europe/Kaliningrad" );
	m_cmbboxTimezone->addItem( "Europe/Kiev" );
	m_cmbboxTimezone->addItem( "Europe/Lisbon" );
	m_cmbboxTimezone->addItem( "Europe/Ljubljana" );
	m_cmbboxTimezone->addItem( "Europe/London" );
	m_cmbboxTimezone->addItem( "Europe/Luxembourg" );
	m_cmbboxTimezone->addItem( "Europe/Madrid" );
	m_cmbboxTimezone->addItem( "Europe/Malta" );
	m_cmbboxTimezone->addItem( "Europe/Mariehamn" );
	m_cmbboxTimezone->addItem( "Europe/Minsk" );
	m_cmbboxTimezone->addItem( "Europe/Monaco" );
	m_cmbboxTimezone->addItem( "Europe/Moscow" );
	m_cmbboxTimezone->addItem( "Europe/Nicosia" );
	m_cmbboxTimezone->addItem( "Europe/Oslo" );
	m_cmbboxTimezone->addItem( "Europe/Paris" );
	m_cmbboxTimezone->addItem( "Europe/Podgorica" );
	m_cmbboxTimezone->addItem( "Europe/Prague" );
	m_cmbboxTimezone->addItem( "Europe/Riga" );
	m_cmbboxTimezone->addItem( "Europe/Rome" );
	m_cmbboxTimezone->addItem( "Europe/Samara" );
	m_cmbboxTimezone->addItem( "Europe/San_Marino" );
	m_cmbboxTimezone->addItem( "Europe/Sarajevo" );
	m_cmbboxTimezone->addItem( "Europe/Simferopol" );
	m_cmbboxTimezone->addItem( "Europe/Skopje" );
	m_cmbboxTimezone->addItem( "Europe/Sofia" );
	m_cmbboxTimezone->addItem( "Europe/Stockholm" );
	m_cmbboxTimezone->addItem( "Europe/Tallinn" );
	m_cmbboxTimezone->addItem( "Europe/Tirane" );
	m_cmbboxTimezone->addItem( "Europe/Tiraspol" );
	m_cmbboxTimezone->addItem( "Europe/Uzhgorod" );
	m_cmbboxTimezone->addItem( "Europe/Vaduz" );
	m_cmbboxTimezone->addItem( "Europe/Vatican" );
	m_cmbboxTimezone->addItem( "Europe/Vienna" );
	m_cmbboxTimezone->addItem( "Europe/Vilnius" );
	m_cmbboxTimezone->addItem( "Europe/Volgograd" );
	m_cmbboxTimezone->addItem( "Europe/Warsaw" );
	m_cmbboxTimezone->addItem( "Europe/Zagreb" );
	m_cmbboxTimezone->addItem( "Europe/Zaporozhye" );
	m_cmbboxTimezone->addItem( "Europe/Zurich" );
	m_cmbboxTimezone->addItem( "Indian/Antananarivo" );
	m_cmbboxTimezone->addItem( "Indian/Chagos" );
	m_cmbboxTimezone->addItem( "Indian/Christmas" );
	m_cmbboxTimezone->addItem( "Indian/Cocos" );
	m_cmbboxTimezone->addItem( "Indian/Comoro" );
	m_cmbboxTimezone->addItem( "Indian/Kerguelen" );
	m_cmbboxTimezone->addItem( "Indian/Mahe" );
	m_cmbboxTimezone->addItem( "Indian/Maldives" );
	m_cmbboxTimezone->addItem( "Indian/Mauritius" );
	m_cmbboxTimezone->addItem( "Indian/Mayotte" );
	m_cmbboxTimezone->addItem( "Indian/Reunion" );
	m_cmbboxTimezone->addItem( "Mexico/BajaNorte" );
	m_cmbboxTimezone->addItem( "Mexico/BajaSur" );
	m_cmbboxTimezone->addItem( "Mexico/General" );
	m_cmbboxTimezone->addItem( "Mideast/Riyadh87" );
	m_cmbboxTimezone->addItem( "Mideast/Riyadh88" );
	m_cmbboxTimezone->addItem( "Mideast/Riyadh89" );
	m_cmbboxTimezone->addItem( "Pacific/Apia" );
	m_cmbboxTimezone->addItem( "Pacific/Auckland" );
	m_cmbboxTimezone->addItem( "Pacific/Chatham" );
	m_cmbboxTimezone->addItem( "Pacific/Easter" );
	m_cmbboxTimezone->addItem( "Pacific/Efate" );
	m_cmbboxTimezone->addItem( "Pacific/Enderbury" );
	m_cmbboxTimezone->addItem( "Pacific/Fakaofo" );
	m_cmbboxTimezone->addItem( "Pacific/Fiji" );
	m_cmbboxTimezone->addItem( "Pacific/Funafuti" );
	m_cmbboxTimezone->addItem( "Pacific/Galapagos" );
	m_cmbboxTimezone->addItem( "Pacific/Gambier" );
	m_cmbboxTimezone->addItem( "Pacific/Guadalcanal" );
	m_cmbboxTimezone->addItem( "Pacific/Guam" );
	m_cmbboxTimezone->addItem( "Pacific/Honolulu" );
	m_cmbboxTimezone->addItem( "Pacific/Johnston" );
	m_cmbboxTimezone->addItem( "Pacific/Kiritimati" );
	m_cmbboxTimezone->addItem( "Pacific/Kosrae" );
	m_cmbboxTimezone->addItem( "Pacific/Kwajalein" );
	m_cmbboxTimezone->addItem( "Pacific/Majuro" );
	m_cmbboxTimezone->addItem( "Pacific/Marquesas" );
	m_cmbboxTimezone->addItem( "Pacific/Midway" );
	m_cmbboxTimezone->addItem( "Pacific/Nauru" );
	m_cmbboxTimezone->addItem( "Pacific/Niue" );
	m_cmbboxTimezone->addItem( "Pacific/Norfolk" );
	m_cmbboxTimezone->addItem( "Pacific/Noumea" );
	m_cmbboxTimezone->addItem( "Pacific/Pago_Pago" );
	m_cmbboxTimezone->addItem( "Pacific/Palau" );
	m_cmbboxTimezone->addItem( "Pacific/Pitcairn" );
	m_cmbboxTimezone->addItem( "Pacific/Ponape" );
	m_cmbboxTimezone->addItem( "Pacific/Port_Moresby" );
	m_cmbboxTimezone->addItem( "Pacific/Rarotonga" );
	m_cmbboxTimezone->addItem( "Pacific/Saipan" );
	m_cmbboxTimezone->addItem( "Pacific/Samoa" );
	m_cmbboxTimezone->addItem( "Pacific/Tahiti" );
	m_cmbboxTimezone->addItem( "Pacific/Tarawa" );
	m_cmbboxTimezone->addItem( "Pacific/Tongatapu" );
	m_cmbboxTimezone->addItem( "Pacific/Truk" );
	m_cmbboxTimezone->addItem( "Pacific/Wake" );
	m_cmbboxTimezone->addItem( "Pacific/Wallis" );
	m_cmbboxTimezone->addItem( "Pacific/Yap" );
	m_cmbboxTimezone->addItem( "US/Alaska" );
	m_cmbboxTimezone->addItem( "US/Aleutian" );
	m_cmbboxTimezone->addItem( "US/Arizona" );
	m_cmbboxTimezone->addItem( "US/Central" );
	m_cmbboxTimezone->addItem( "US/Eastern" );
	m_cmbboxTimezone->addItem( "US/East-Indiana" );
	m_cmbboxTimezone->addItem( "US/Hawaii" );
	m_cmbboxTimezone->addItem( "US/Indiana-Starke" );
	m_cmbboxTimezone->addItem( "US/Michigan" );
	m_cmbboxTimezone->addItem( "US/Mountain" );
	m_cmbboxTimezone->addItem( "US/Pacific" );
	m_cmbboxTimezone->addItem( "US/Pacific-New" );
	m_cmbboxTimezone->addItem( "US/Samoa" );
	m_cmbboxTimezone->addItem( "CET" );
	m_cmbboxTimezone->addItem( "CST6CDT" );
	m_cmbboxTimezone->addItem( "Cuba" );
	m_cmbboxTimezone->addItem( "EET" );
	m_cmbboxTimezone->addItem( "Egypt" );
	m_cmbboxTimezone->addItem( "Eire" );
	m_cmbboxTimezone->addItem( "EST" );
	m_cmbboxTimezone->addItem( "EST5EDT" );
	m_cmbboxTimezone->addItem( "Factory" );
	m_cmbboxTimezone->addItem( "GB" );
	m_cmbboxTimezone->addItem( "GB-Eire" );
	m_cmbboxTimezone->addItem( "GMT" );
	m_cmbboxTimezone->addItem( "GMT0" );
	m_cmbboxTimezone->addItem( "GMT-0" );
	m_cmbboxTimezone->addItem( "GMT+0" );
	m_cmbboxTimezone->addItem( "Greenwich" );
	m_cmbboxTimezone->addItem( "Hongkong" );
	m_cmbboxTimezone->addItem( "HST" );
	m_cmbboxTimezone->addItem( "Iceland" );
	m_cmbboxTimezone->addItem( "Iran" );
	m_cmbboxTimezone->addItem( "Israel" );
	m_cmbboxTimezone->addItem( "Jamaica" );
	m_cmbboxTimezone->addItem( "Japan" );
	m_cmbboxTimezone->addItem( "Kwajalein" );
	m_cmbboxTimezone->addItem( "Libya" );
	m_cmbboxTimezone->addItem( "MET" );
	m_cmbboxTimezone->addItem( "MST" );
	m_cmbboxTimezone->addItem( "MST7MDT" );
	m_cmbboxTimezone->addItem( "Navajo" );
	m_cmbboxTimezone->addItem( "NZ" );
	m_cmbboxTimezone->addItem( "NZ-CHAT" );
	m_cmbboxTimezone->addItem( "Pacific" );
	m_cmbboxTimezone->addItem( "Poland" );
	m_cmbboxTimezone->addItem( "Portugal" );
	m_cmbboxTimezone->addItem( "PRC" );
	m_cmbboxTimezone->addItem( "PST8PDT" );
	m_cmbboxTimezone->addItem( "ROC" );
	m_cmbboxTimezone->addItem( "ROK" );
	m_cmbboxTimezone->addItem( "Singapore" );
	m_cmbboxTimezone->addItem( "Turkey" );
	m_cmbboxTimezone->addItem( "UCT" );
	m_cmbboxTimezone->addItem( "Universal" );
	m_cmbboxTimezone->addItem( "UTC" );
	m_cmbboxTimezone->addItem( "WET" );
	m_cmbboxTimezone->addItem( "W-SU" );
	m_cmbboxTimezone->addItem( "Zulu" );
}
