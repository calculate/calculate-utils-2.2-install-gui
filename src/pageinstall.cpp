#include "pageinstall.h"

#include <QBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QProgressBar>

#include <QFile>

#include <QDebug>

#include <sys/types.h>
#include <signal.h>

#include "calculateconfig.h"

PageInstall::PageInstall() :
	InstallerPage(),
	m_clProc(0),
	m_doTerminate(false)
{
	setupUi();
}

PageInstall::~PageInstall()
{
	if ( m_clProc )
	{
		// отключить все сигналы
		disconnect(m_clProc);
		// на всякий случай
		m_doTerminate = true;
		qDebug() << QString("Terminate cl-install: kill(%1, %2)").arg(m_clProc->pid()).arg(SIGINT);

		// пробовал посылать сигнал SIGINT всем дочерним процессам (игнорируя в основном) (не помогло)
		//signal(SIGINT, SIG_IGN);
		//::kill( 0, SIGINT);

		// посылаю сигнал
		::kill( m_clProc->pid(), SIGINT);
		// ожидаю завершения cl-install
		m_clProc->waitForFinished(-1);
		// вывожу в лог из буферов
		qDebug() << "Output: " << endl << QString(m_clProc->readAll());

		delete m_clProc;
		m_clProc = 0;
	}
}

void PageInstall::setupUi()
{
	m_Output = new QTextEdit;
	m_Output->setReadOnly(true);
	QFont	font = m_Output->currentFont();
	font.setFamily("Droid Sans Mono");
	font.setPointSize(8);
	m_Output->setFont( font );
	m_Output->setLineWrapMode( QTextEdit::NoWrap );

	m_Progress = new QProgressBar(0);

	QHBoxLayout*	hbox = new QHBoxLayout;
	hbox->addWidget(m_Progress);

	QVBoxLayout*	vbox = new QVBoxLayout;

	vbox->addWidget(m_Output);
	vbox->addLayout(hbox);

	setLayout(vbox);

	retranslateUi();
}

void PageInstall::retranslateUi()
{
	setTitle(  tr("Installing") );
}

void PageInstall::show()
{
	qDebug() << "install show()";
	m_Progress->setMaximum(100);
	m_Output->clear();

	CalculateConfig* clConf = CalculateConfig::instance();

	emit	changeNext(false);
	emit	changePrev(false);

	if ( !m_clProc )
	{
		// check password for user
		QStringList passwdNeed = clConf->getPasswordUsers();
		qDebug() << "Passwords need: " + passwdNeed.join(",");

		QStringList passwd;
		foreach(const QString& user, passwdNeed)
		{
			if (user == "root")
			{
				passwd << clConf->getValue("gui_root_psw").toString();
			}
			else
			{
				QString	pass;
				for (int i(0); i != clConf->getValue("gui_users").toStringList().count(); ++i)
				{
					if ( clConf->getValue("gui_users").toStringList().at(i) == user )
					{
						pass = clConf->getValue("gui_passwds").toStringList().at(i);
						break;
					}
				}
				if ( !pass.isEmpty() )
				{
					passwd << pass;
				}
				else
				{
					qDebug() << "Unknown user " << user;
					emit	changePrev(true);
					m_Output->insertPlainText( tr("Users parsing error") );
					return;
				}
			}
		}

		m_clProc = new QProcess(this);

		connect( m_clProc, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)) );
		connect( m_clProc, SIGNAL(readyReadStandardOutput()), this, SLOT(showStdOut()) );
		connect( m_clProc, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(onFinish(int,QProcess::ExitStatus)) );

		QStringList args = CalculateConfig::instance()->getInstallParameters();

		qDebug() << "Start: cl-install ";

		foreach(const QString& arg, args)
		    qDebug() << arg;

		m_clProc->setStandardErrorFile("/var/log/calculate/cl-install-gui-err.log");

		m_clProc->start( "cl-install -f --nospinner --color never -P " +  args.join(" ") );
		//m_Output->insertPlainText( "cl-install -f --color never -P " +  args.join(" ") );

		m_Progress->setMinimum(0);
		m_Progress->setMaximum(0);

		m_clProc->write( (passwd.join("\n") + "\n").toLocal8Bit() );
	}
}

void PageInstall::onError(QProcess::ProcessError error)
{
	m_Progress->setMinimum(0);
	m_Progress->setMaximum(100);
	qDebug() << "failed to run process 'cl-install' error=" << error ;
	onFinish(-error);
}

void PageInstall::showStdOut()
{
	if (m_clProc)
	{
		if (m_doTerminate)
			return;

		QString str = m_clProc->readAllStandardOutput();

		QFile	std_log("/var/log/calculate/cl-install-gui_stdlog.txt");
		if ( std_log.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append) )
		{
			std_log.write( (str + "\n").toLocal8Bit() );
		}

		m_Output->moveCursor(QTextCursor::End);
		m_Output->insertPlainText( str );
	}
}

void PageInstall::onFinish(int exitCode, QProcess::ExitStatus exitStatus)
{
	if (m_doTerminate)
		return;

	qDebug() << "install onFinish() " << exitCode;

	m_Progress->setMinimum(0);
	m_Progress->setMaximum(100);

	emit	changeNext(true);
	emit	changePrev(true);

	if (exitCode == 0)
	{
		m_Progress->setValue(100);

		// instalation complete
		emit	changePrev(false);
	}
	else
	{
		m_Output->moveCursor(QTextCursor::End);
		m_Output->insertPlainText(
			tr("Error. Additional information in /var/log/calculate/cl-install-gui-err.log")
		);
		emit	changeNext(false);
	}

	m_Progress->setValue(100);

	disconnect(m_clProc);
	delete m_clProc;
	m_clProc = 0;
}

