#pragma once

#include "installerpage.h"

#include <QMap>

#include "commons.h"

class QLabel;
class QToolButton;
class QTableWidget;

class PageMountPoints : public InstallerPage
{
	Q_OBJECT
public:
	PageMountPoints();

	bool 		validate();
	void 		retranslateUi();

public slots:
	void 		show();

private slots:
	void		addMountPoint();
	void		delMountPoint();
	void		modifyMountPoint();

protected:
	void 		setupUi();

private:
	void		initMountPoints();
	void 		correctMountPoints();
	void 		showMountPoints();
	void		getPartitions();

	void		generateCmdDisk();
	QStringList parseMountPoint();
	QString		parseSwap();

private:
	QLabel*			m_labMountPointHelp;
	QToolButton*	m_butAddMountPoint;
	QToolButton*	m_butDelMountPoint;
	QTableWidget*	m_tabMountPoints;

	MountPointsList m_MountPointsList;

	MountPointsMap	m_Partitions;
};