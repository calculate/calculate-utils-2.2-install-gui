#include "pagemanager.h"

#include <QStackedWidget>
#include <QLabel>
#include <QApplication>
#include <QEvent>
#include <QDebug>

#include "installerpage.h"

PageManager::PageManager(QStackedWidget* stackedWidget, QLabel* listLabel, QObject* parent) :
	QObject(parent),
	m_StackWidget(stackedWidget),
	m_ListLabel(listLabel),
	m_isSinglePage(false)
{
	Q_ASSERT_X( stackedWidget != 0, "PageManager::PageManager", "stackedWidget pointer is 0");
	Q_ASSERT_X( listLabel != 0, "PageManager::PageManager", "listLabel pointer is 0");

	m_ListLabel->setWordWrap(true);
	m_ListLabel->setAlignment ( Qt::AlignLeft | Qt::AlignTop );
}

PageManager::~PageManager()
{
	// clear stack widget
	removeStackedPage();

	qDeleteAll(m_Pages);
}

void PageManager::addPage(InstallerPage* page)
{
	Q_ASSERT_X( page != 0, "PageManager::addPage", "page pointer is 0");

	connect( page, SIGNAL(changeNext(bool)), this, SIGNAL(changeNext(bool)) );
	connect( page, SIGNAL(changePrev(bool)), this, SIGNAL(changePrev(bool)) );
	connect( page, SIGNAL(skipNext()), this, SLOT(showNext()) );

	m_Pages.push_back(page);

	m_CurPage = m_Pages.begin();
	pageUpdate();

}

unsigned int PageManager::getPageCount()
{
	return m_Pages.count();
}

void PageManager::showOnce(InstallerPage* page)
{
	Q_ASSERT_X( page != 0, "PageManager::showOnce", "page pointer is 0");

	if (page)
	{
		removeStackedPage();
		m_isSinglePage = true;
		m_StackWidget->addWidget( page );
		m_StackWidget->setCurrentIndex(0);
		page->show();
	}
}

void PageManager::showFirst()
{
}

void PageManager::showNext()
{
	if ((*m_CurPage)->validate())
	{
		if ( !m_isSinglePage && (m_CurPage != m_Pages.end()) && ((m_CurPage + 1) != m_Pages.end()) )
		{
			++m_CurPage;
			pageUpdate();
		}
		if (m_isSinglePage)
		{
			m_isSinglePage = false;
			pageUpdate();
		}
	}
}

void PageManager::showPrevious()
{
	if ( !m_isSinglePage && (m_CurPage != m_Pages.begin()) )
	{
		--m_CurPage;
		pageUpdate();
	}
	if (m_isSinglePage)
	{
		m_isSinglePage = false;
		pageUpdate();
	}
}

void PageManager::pageUpdate()
{
	QString	label;

	removeStackedPage();

	m_StackWidget->addWidget( *m_CurPage );
	m_StackWidget->setCurrentIndex(0);

	foreach(InstallerPage* page, m_Pages)
	{
		QString title = page->getTitle();
		page->clearActive();

		if (title == (*m_CurPage)->getTitle())
		{
			title = "<b>" + title + "</b>";
			page->setActive();
			// restore active key settings
			emit changeNext(true);
			emit changePrev(true);
			page->show();
		}
		title += "<br>";

		label += title;
	}

	m_ListLabel->setText( label );
}

void PageManager::removeStackedPage()
{
	QWidget*	currWidget = m_StackWidget->currentWidget();
	if (currWidget)
	{
		m_StackWidget->removeWidget( currWidget );
	}
}

void PageManager::retranslatePages()
{
	foreach(InstallerPage* page, m_Pages)
		page->retranslateUi();

	pageUpdate();
}
