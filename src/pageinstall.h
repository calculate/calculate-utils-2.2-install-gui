#pragma once

#include "installerpage.h"
#include "tools.h"

#include <QProcess>

class QTextEdit;
class QProgressBar;
class QLabel;

class PageInstall : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageInstall();
    ~PageInstall();

	void retranslateUi();

protected:
	void setupUi();

public slots:
	void show();

private slots:
	void onError(QProcess::ProcessError error);
	void showStdOut();
	void onFinish(int exitCode, QProcess::ExitStatus exitStatus = QProcess::NormalExit);

private:
	QTextEdit*		m_Output;
//	QLabel*			m_LabelEta;
	QProgressBar*	m_Progress;

	QProcess*		m_clProc;
	bool			m_doTerminate;
};

