#include "pagewelcome.h"

#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>

#include <QDebug>

#include "calculateconfig.h"
#include "tools.h"

typedef QPair<QString,QString>	LangDesc;

PageWelcome::PageWelcome() :
    InstallerPage()
{
	m_Languages["en_US"] = "English";
	m_Languages["ru_RU"] = "Русский";
	m_Languages["bg_BG"] = "Български";
	m_Languages["uk_UA"] = "Український";

    setupUi();
}

void PageWelcome::setupUi()
{
    // widgets
    m_labelWelcome = new QLabel;
    m_labelWelcome->setWordWrap( true );
    m_labelWelcome->setOpenExternalLinks( true );

    m_labelLanguage = new QLabel;
    m_comboboxLanguages = new QComboBox;

    //
    QHBoxLayout*	hbox_1 = new QHBoxLayout;
    hbox_1->addWidget(m_comboboxLanguages);
    hbox_1->addStretch();

    //
    QVBoxLayout*	vbox_1 = new QVBoxLayout;
    vbox_1->addWidget( m_labelWelcome );
    vbox_1->addWidget( m_labelLanguage );
    vbox_1->addLayout( hbox_1 );
    vbox_1->addStretch();

    setLayout(vbox_1);

    retranslateUi();
}

void PageWelcome::retranslateUi()
{
	setTitle( tr("Welcome") );

	m_labelWelcome->setText(
		"<html>" +
		tr(
			"<p>Welcome to Calculate Linux.</p>"
			"<p><a href='http://www.calculate-linux.org'>http://www.calculate-linux.org</a></p>"
		)
		+ "</html>"
	);
	m_labelLanguage->setText( tr("Choose a language: ") );

	m_comboboxLanguages->disconnect(SIGNAL(currentIndexChanged(int)), this, SLOT(changeLanguageIndex(int)));

	m_comboboxLanguages->clear();
	unsigned int	pos = 0;
	unsigned int	selected = 0;
	QMapIterator<QString,QString> it(m_Languages);

	QString	curLang = CalculateConfig::instance()->getValue("gui_locale_language").toString();
	while(it.hasNext())
	{
		it.next();

		m_comboboxLanguages->addItem( it.value(), QVariant(it.key()) );

		if (it.key() == curLang)
			selected = pos;

		++pos;
	}
	m_comboboxLanguages->setCurrentIndex(selected);

	m_comboboxLanguages->setToolTip( tr("Please choose the language which should be used for this application.") );

	connect(m_comboboxLanguages, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLanguageIndex(int)));
}

void PageWelcome::changeLanguageIndex(int indx)
{
	QVariant lang = m_comboboxLanguages->itemData(indx);

	if (lang != QVariant::Invalid)
		emit changeLanguage( lang.toString() );
}

void PageWelcome::show()
{
	emit changeNext(true);
}
