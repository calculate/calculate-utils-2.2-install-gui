#pragma once

#include <QWidget>
#include <QEvent>

class InstallerPage : public QWidget
{
	Q_OBJECT
public:
	InstallerPage( QWidget* parent = 0, const QString& title = QString() )
		: QWidget(parent), m_Active(false), m_Title(title)
	{}

	virtual ~InstallerPage() {}

protected:
	virtual void setupUi() = 0;


public:
	QString		getTitle() const { return m_Title; }
	void		setTitle(const QString& title ) { m_Title = title; }
	void		setActive() { m_Active = true; };
	void		clearActive() { m_Active = false; };

	virtual bool validate() { return true; }
	virtual void retranslateUi() = 0;

signals:
	void 		changeNext(bool);
	void 		changePrev(bool);
	void 		skipNext();

public slots:
	virtual void show() {}

protected:
	bool		m_Active;
	QString		m_Title;
};
