#include "userinfodialog.h"

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QBoxLayout>

#include <QRegExp>
#include <QRegExpValidator>

#include <QMessageBox>

UserInfoDialog::UserInfoDialog ( QWidget* parent, const UserInfo& userInfo)
  :	QDialog(parent)
{
	setupUi();

	if (userInfo.name.isEmpty())
	{
		setWindowTitle( tr("Add user") );
	}
	else
	{
		setWindowTitle( tr("Modify user") );

		m_edUserName->setText(userInfo.name);
		m_edPsw->setText(userInfo.psw);
		m_edPswRep->setText(userInfo.psw);
	}

	connect( m_edPsw, SIGNAL(textChanged(QString)), this, SLOT(checkPasswords()) );
	connect( m_edPswRep, SIGNAL(textChanged(QString)), this, SLOT(checkPasswords()) );

	connect( m_butOk, SIGNAL(clicked(bool)), this, SLOT(preAccept()) );
	connect( m_butCancel, SIGNAL(clicked(bool)), this, SLOT(reject()) );

	checkPasswords();
}

UserInfoDialog::~UserInfoDialog()
{
}

void UserInfoDialog::setupUi()
{
	QRegExp	rxName("[a-z][0-9a-z_\\-]{0,63}");

	m_labUserName = new QLabel( tr("User name:") );
	m_edUserName = new QLineEdit;
	m_edUserName->setValidator( new QRegExpValidator(rxName, this) );

	QHBoxLayout* hbox_0 = new QHBoxLayout;
	hbox_0->addWidget(m_labUserName);
	hbox_0->addWidget(m_edUserName);

	m_labPsw = new QLabel( tr("Password") );
	m_edPsw = new QLineEdit;
	m_edPsw->setEchoMode( QLineEdit::Password );

	m_labPswRep = new QLabel( tr("Confirm Password") );
	m_edPswRep = new QLineEdit;
	m_edPswRep->setEchoMode( QLineEdit::Password );

	QGridLayout*	gbox_0 = new QGridLayout;
	gbox_0->setContentsMargins(0, 0, 0, 0);
	gbox_0->addWidget(m_labPsw, 0, 0);
	gbox_0->addWidget(m_edPsw, 0, 1);
	gbox_0->addWidget(m_labPswRep, 1, 0);
	gbox_0->addWidget(m_edPswRep, 1, 1);

	m_labMatch = new QLabel;
	QHBoxLayout* hbox_1 = new QHBoxLayout;
	hbox_1->addStretch();
	hbox_1->addWidget(m_labMatch);

	m_butOk = new QPushButton( tr("OK") );
	m_butCancel = new QPushButton( tr("Cancel") );

	QHBoxLayout*	hbox_but = new QHBoxLayout;
	hbox_but->addStretch();
	hbox_but->addWidget(m_butOk);
	hbox_but->addWidget(m_butCancel);

	QVBoxLayout*	vbox_0 = new QVBoxLayout;
	vbox_0->addLayout(hbox_0);
	vbox_0->addLayout(gbox_0);
	vbox_0->addLayout(hbox_1);
	vbox_0->addLayout(hbox_but);

	setLayout(vbox_0);
}

void UserInfoDialog::checkPasswords()
{
	bool	pswMatch = false;

	if ( (!m_edPsw->text().isEmpty()) && (m_edPsw->text() == m_edPswRep->text()) )
		pswMatch = true;

	m_butOk->setEnabled( pswMatch );

	QString	pswState;
	if ( pswMatch )
	{
		pswState = tr("Passwords match");
	}
	else
	{
		pswState = tr("Passwords do not match");
	}
	m_labMatch->setText( pswState );
}

void UserInfoDialog::preAccept()
{
	QString name = m_edUserName->text();
	if ( name.isEmpty() )
	{
		QMessageBox::critical(this, tr("Error"), tr("User name is empty") );
		m_edUserName->setFocus();
		return;
	}
	if ( name == "root" )
	{
		QMessageBox::critical(this, tr("Error"), tr("User root can't added") );
		m_edUserName->setFocus();
		return;
	}

	m_UserInfo.name = name;
	m_UserInfo.psw = m_edPsw->text();

	accept();
}


