#pragma once

#include <QMap>

class QString;

struct UserInfo
{
	QString	name;
	QString	psw;
};

struct MountPoint {
	// constant info
	QString		dev;
	QString		label;
	QString		size;
	QString		fs;
	// from gui to installer
	QString		mountpoint;
	QString		fs_new;
	bool		format;
	bool		migrated;
	QString		migrated_dev;

	MountPoint() : format(false), migrated(false) {};
};

typedef	QList<MountPoint>			MountPointsList;
typedef QMap<QString,MountPoint>	MountPointsMap;