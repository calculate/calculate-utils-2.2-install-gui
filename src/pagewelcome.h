#pragma once

#include "installerpage.h"
#include <QString>
#include <QMap>

class QLabel;
class QComboBox;
class QTextBrowser;

class PageWelcome : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageWelcome();

	void retranslateUi();

private:
	void setupUi();

private slots:
	void changeLanguageIndex(int indx);

public slots:
	void show();

signals:
	void changeLanguage(QString);

private:
	QLabel*		m_labelWelcome;
	QLabel*		m_labelLanguage;
	QComboBox*	m_comboboxLanguages;

	QMap<QString, QString>	m_Languages;
};

