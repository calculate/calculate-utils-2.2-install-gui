#pragma once

#include "installerpage.h"

class QTermWidget;
class QWidget;
class QLabel;

class PageCfdisk : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageCfdisk(const QString& command, const QString& disk);

	void retranslateUi();

protected:
	void setupUi();

public slots:
	void show();
	//

signals:
	void completed();
	//

private:
	const QString	m_Cmd;
	const QString	m_Disk;
	const QString	m_Lang;
	const QString	m_envTerm;
	QWidget*		m_widgetTerm;
	QLabel*			m_labelHelp;

	QTermWidget*	m_Term;
};

