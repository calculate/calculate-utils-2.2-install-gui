#pragma once

#include "installerpage.h"

#include <QString>
#include <QMap>

class QLabel;
class QRadioButton;
class QPushButton;
class QComboBox;

class PagePartitioning : public InstallerPage
{
	Q_OBJECT
public:
	explicit PagePartitioning();

	bool validate();
	void retranslateUi();

private:
	void setupUi();
	void mapSignals();
	void unmapSignals();

public slots:
	void show();

private slots:
	void updateData();
	void partitioningCurrentDisk();

signals:
	void manualyPartitioning(QString);
	void selectedVolume(QString);

private:
	QRadioButton*	m_butExistPartitions;
	QRadioButton*	m_butAllDisk;
	QPushButton*	m_butManualPartitioning;
	QLabel*			m_labDisk;

	QComboBox*		m_cmbxDisks;
};

