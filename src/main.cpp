#include <QtGui/QApplication>

#include <QTextCodec>
#include <QLocale>
#include <QTranslator>
#include <QMessageBox>
#include <QFile>
#include <QDateTime>

#include <QDebug>

#include <QProcess>
#include <QSharedMemory>

#include "systeminstaller.h"

static QFile	debugLog;

void toDebugLog(const QString& str);
void MsgHandler(QtMsgType type, const char *msg);

int main(int argc, char** argv)
{
	QTextCodec::setCodecForLocale( QTextCodec::codecForName("UTF-8") );
	QTextCodec::setCodecForTr( QTextCodec::codecForName("UTF-8") );
	QTextCodec::setCodecForCStrings( QTextCodec::codecForName("UTF-8") );

	QApplication	app(argc, argv);

	QSharedMemory	lock("calculate-install-gui");

	if (lock.attach(QSharedMemory::ReadOnly))
	{
		QMessageBox::critical(0, "cl-install-gui", "Application is already running");
		return -2;
	}
	lock.create(1);

	qInstallMsgHandler( MsgHandler );

	debugLog.setFileName("/var/log/calculate/cl-install-gui-debug.log");
	if ( debugLog.open(QIODevice::Append | QIODevice::WriteOnly | QIODevice::Text | QIODevice::Unbuffered) )
	{
		qDebug() << "Installation started";
	}

	SystemInstaller	installer;

	if ( getuid() != 0)
	{
		QMessageBox::critical(
			0,
			QObject::tr("Error"),
			QObject::tr("You do not have administrative privileges.")
		);
		return -1;
	}

	installer.show();

	return app.exec();
}

void MsgHandler(QtMsgType type, const char *msg)
{
	switch (type) {
		case QtDebugMsg:
			toDebugLog( QString(msg) );
			break;
		default:
			break;
	}
}

void toDebugLog(const QString& str)
{
	if (debugLog.isOpen())
		debugLog.write(
			("[" + QDateTime::currentDateTime().toString(Qt::ISODate) + "] " +
			str + "\n\r").toLocal8Bit()
		);
}
