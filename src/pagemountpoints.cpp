#include "pagemountpoints.h"

#include <QLabel>
#include <QToolButton>
#include <QTableWidget>
#include <QHeaderView>
#include <QBoxLayout>
#include <QMessageBox>

#include <QDebug>

#include "calculateconfig.h"
#include "mountpointdialog.h"

PageMountPoints::PageMountPoints()
  :	InstallerPage()
{
	setupUi();

	initMountPoints();
}

void PageMountPoints::setupUi()
{
	m_labMountPointHelp = new QLabel;
	m_labMountPointHelp->setWordWrap(true);

	m_butAddMountPoint = new QToolButton;
	m_butAddMountPoint->setIcon( QIcon(":/img/db_add.png") );
	m_butDelMountPoint = new QToolButton;
	m_butDelMountPoint->setIcon( QIcon(":/img/db_remove.png") );
	QHBoxLayout* hbox_but = new QHBoxLayout;
	hbox_but->addWidget(m_butAddMountPoint);
	hbox_but->addWidget(m_butDelMountPoint);
	hbox_but->addStretch();

	m_tabMountPoints = new QTableWidget;

	QVBoxLayout* vbox_0 = new QVBoxLayout;
	vbox_0->addWidget(m_labMountPointHelp);
	vbox_0->addLayout(hbox_but);
	vbox_0->addWidget(m_tabMountPoints);

	setLayout(vbox_0);

	m_tabMountPoints->setColumnCount(6);
	QTableWidgetItem*	tabMpItem0 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(0, tabMpItem0);
	QTableWidgetItem*	tabMpItem1 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(1, tabMpItem1);
	QTableWidgetItem*	tabMpItem2 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(2, tabMpItem2);
	QTableWidgetItem*	tabMpItem3 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(3, tabMpItem3);
	QTableWidgetItem*	tabMpItem4 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(4, tabMpItem4);
	QTableWidgetItem*	tabMpItem5 = new QTableWidgetItem();
	m_tabMountPoints->setHorizontalHeaderItem(5, tabMpItem5);

	m_tabMountPoints->horizontalHeader()->setStretchLastSection(true);
	m_tabMountPoints->verticalHeader()->setVisible(false);

	connect( m_tabMountPoints, SIGNAL(itemDoubleClicked(QTableWidgetItem*)),
			this, SLOT(modifyMountPoint())
	);

	connect( m_butAddMountPoint, SIGNAL(clicked(bool)), this, SLOT(addMountPoint()) );
	connect( m_butDelMountPoint, SIGNAL(clicked(bool)), this, SLOT(delMountPoint()) );

	retranslateUi();
}

void PageMountPoints::retranslateUi()
{
	setTitle( tr("Mount points") );

	m_butAddMountPoint->setToolTip( tr("Add a new mount point") );
	m_butDelMountPoint->setToolTip( tr("Remove a selected mount point") );

	QTableWidgetItem*	tab0 = m_tabMountPoints->horizontalHeaderItem(0);
	tab0->setText( tr("Mount\npoint") );
	QTableWidgetItem*	tab1 = m_tabMountPoints->horizontalHeaderItem(1);
	tab1->setText( tr("Partition") );
	QTableWidgetItem*	tab2 = m_tabMountPoints->horizontalHeaderItem(2);
	tab2->setText( tr("Format") );
	QTableWidgetItem*	tab3 = m_tabMountPoints->horizontalHeaderItem(3);
	tab3->setText( tr("File\nsystem") );
	QTableWidgetItem*	tab4 = m_tabMountPoints->horizontalHeaderItem(4);
	tab4->setText( tr("Label") );
	QTableWidgetItem*	tab5 = m_tabMountPoints->horizontalHeaderItem(5);
	tab5->setText( tr("Size") );

	m_tabMountPoints->setSelectionMode(QTableWidget::SingleSelection);
	m_tabMountPoints->setSelectionBehavior(QTableWidget::SelectRows);

	m_labMountPointHelp->setText(
		tr(
			"Choose mount points for the installation. "
			"You need at least a root / partition.\n"
			"To modify the mount point, double-click it."
		)
	);
}

void PageMountPoints::show()
{
	// get from CalculateConfig
	getPartitions();

	showMountPoints();

	m_tabMountPoints->setColumnWidth(0, 110);
	m_tabMountPoints->setColumnWidth(1, 110);
	m_tabMountPoints->setColumnWidth(2, 100);
	m_tabMountPoints->setColumnWidth(3, 90);
	m_tabMountPoints->setColumnWidth(4, 110);

	m_tabMountPoints->setEnabled(true);
	CalculateConfig::instance()->showInstallParameters();
	if ( CalculateConfig::instance()->getValue("gui_partitioning") == "auto" )
	{
		QMessageBox::information(
			this,
			tr("Information"),
			tr("You select auto partitioning. Press \"Next\" to continue.")
		);
		m_tabMountPoints->setEnabled(false);
		emit changeNext(true);
	}
}

bool PageMountPoints::validate()
{
	if ( CalculateConfig::instance()->getValue("gui_partitioning") != "auto" )
	{
		foreach( const MountPoint& mp, m_MountPointsList )
		{
			if ( mp.dev.isEmpty() || mp.mountpoint.isEmpty() )
			{
				QMessageBox::warning(
					this,
					tr("Warning"),
					tr("To continue You need to choose partition for mount point %1.").arg(mp.mountpoint)
				);
				return false;
			}
		}
	}

	return true;
}

void PageMountPoints::addMountPoint()
{
	// all partitions except selected
	MountPointsMap			partitions(m_Partitions);
	foreach(const MountPoint& mp, m_MountPointsList)
	{
		if ( !mp.mountpoint.isEmpty() && !mp.dev.isEmpty() )
		{
			partitions.remove(mp.dev);
		}
	}

	if ( partitions.isEmpty() )
	{
		QMessageBox::warning(
			this,
			tr("Warning"),
			tr("Adding is not possible. All partitions is busy.")
		);
		return;
	}

	QScopedPointer<MountPointDialog> mpDlg( new MountPointDialog(this, m_MountPointsList, partitions) );

	if ( mpDlg->exec() == QDialog::Accepted )
	{
		MountPoint	mp = mpDlg->getMountPoint();
		m_MountPointsList.append(mp);

		showMountPoints();
	}
}

void PageMountPoints::delMountPoint()
{
	int index = m_tabMountPoints->currentRow();
	if (index != -1)
	{
		if (m_MountPointsList.at(index).mountpoint == "/" )
		{
			QMessageBox::warning(this, tr("Warning"), tr("Root partition can't be deleted") );
			return;
		}

		if (m_MountPointsList.at(index).migrated )
		{
			QMessageBox::warning(
				this,
				tr("Warning"),
				tr("Migrated mount point can't be deleted. Use 'none' for disabling migration.")
			);
			return;
		}

		m_MountPointsList.removeAt(index);
		showMountPoints();
	}
}

void PageMountPoints::modifyMountPoint()
{
	int index = m_tabMountPoints->currentRow();
	if (index != -1)
	{
		MountPoint	modMp = m_MountPointsList.at(index);

		MountPointsMap	partitions(m_Partitions);
		foreach(const MountPoint& mp, m_MountPointsList)
		{
			if ( !mp.mountpoint.isEmpty() && !mp.dev.isEmpty() && (mp.dev != modMp.dev) )
			{
				partitions.remove(mp.dev);
			}
		}

		QScopedPointer<MountPointDialog> mpDlg(
			new MountPointDialog(this, m_MountPointsList, partitions, modMp)
		);

		if ( mpDlg->exec() == QDialog::Accepted )
		{
			MountPoint	mp = mpDlg->getMountPoint();
			m_MountPointsList.removeAt(index);
			m_MountPointsList.insert(index, mp);

			showMountPoints();
		}
	}
}

void PageMountPoints::correctMountPoints()
{
	// check use cl-install
	CalculateConfig*	clConf = CalculateConfig::instance();

	clConf->getNewMountpoints();
	QStringList	os_disk_dev = clConf->getValue("os_disk_dev").toStringList();

	QStringList os_install_disk_format = clConf->getValue("os_install_disk_format").toStringList();
	QStringList perform_format = clConf->getValue("os_install_disk_perform_format").toStringList();

	for (int i(0); i < os_disk_dev.size(); ++i)
	{
		if (perform_format.at(i) == "yes")
		{
			qDebug() << "Format: " << os_disk_dev.at(i);
			int index = -1;
			for ( int j = 0; j != m_MountPointsList.size(); ++j)
			{
				if ( m_MountPointsList.at(j).dev == os_disk_dev.at(i) )
				{
					index = j;
					break;
				}
			}
			if (index == -1)
				continue;

			MountPoint mp( m_MountPointsList.at(index) );

			mp.format = true;
			mp.fs_new = os_install_disk_format.at(i);

			m_MountPointsList.removeAt(index);
			m_MountPointsList.insert(index, mp);
		}
	}
}

void PageMountPoints::showMountPoints()
{
	// clear table
	while( m_tabMountPoints->rowCount() )
		m_tabMountPoints->removeRow(0);
	m_tabMountPoints->repaint();

	generateCmdDisk();
	correctMountPoints();

	foreach(const MountPoint& mp, m_MountPointsList)
	{
		int row = m_tabMountPoints->rowCount();
		m_tabMountPoints->insertRow( row );
		QTableWidgetItem*	mpItem = new QTableWidgetItem(mp.mountpoint);
		mpItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 0, mpItem);

		QTableWidgetItem*	partItem = new QTableWidgetItem(mp.dev);
		partItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 1, partItem);

		QTableWidgetItem*	formatItem = new QTableWidgetItem(mp.format ? tr("YES") : tr("no") );
		formatItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 2, formatItem);

		QTableWidgetItem*	fsItem = new QTableWidgetItem(mp.format ? mp.fs_new : mp.fs);
		fsItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 3, fsItem);

		QTableWidgetItem*	labItem = new QTableWidgetItem(mp.label);
		labItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 4, labItem);

		QTableWidgetItem*	sizeItem = new QTableWidgetItem(mp.size);
		sizeItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		m_tabMountPoints->setItem(row, 5, sizeItem);
	}

	generateCmdDisk();
}

void PageMountPoints::getPartitions()
{
	m_Partitions.clear();

	CalculateConfig*	clConf = CalculateConfig::instance();
	clConf->getNewPartitioning();


	QStringList	os_disk_dev = clConf->getValue("os_disk_dev").toStringList();
	QStringList	os_disk_format = clConf->getValue("os_disk_format").toStringList();
	QStringList	os_disk_name = clConf->getValue("os_disk_name").toStringList();
	QStringList	os_disk_part = clConf->getValue("os_disk_part").toStringList();
	QStringList	os_disk_size = clConf->getValue("os_disk_size").toStringList();

	QStringList	os_disk_mount = clConf->getValue("os_disk_mount").toStringList();

	QStringList perform_format = clConf->getValue("os_install_disk_perform_format").toStringList();

	for (int i(0); i < os_disk_dev.size(); ++i)
	{
		if (
			(os_disk_part.at(i) == "extended") ||							// skip extended partition
			(os_disk_mount.at(i) == "/")									// skip root
		)
			continue;

		MountPoint mp;

		mp.dev = os_disk_dev.at(i);
		mp.fs = os_disk_format.at(i);
		mp.format = perform_format.at(i) == "yes";
		mp.label = os_disk_name.at(i);
		mp.size = os_disk_size.at(i);

		m_Partitions[ os_disk_dev.at(i) ] = mp;
	}
}

void PageMountPoints::initMountPoints()
{
	bool				rootFound = false;
	CalculateConfig*	clConf = CalculateConfig::instance();

	// check new format parameters
	clConf->getNewMountpoints();

	QStringList	os_disk_dev = clConf->getValue("os_disk_dev").toStringList();
	QStringList	os_disk_format = clConf->getValue("os_disk_format").toStringList();
	QStringList	os_disk_name = clConf->getValue("os_disk_name").toStringList();
	QStringList	os_disk_part = clConf->getValue("os_disk_part").toStringList();
	QStringList	os_disk_size = clConf->getValue("os_disk_size").toStringList();

	QStringList	os_disk_mount = clConf->getValue("os_disk_mount").toStringList();

	QStringList def_os_install_disk_mount = clConf->getValue("def_os_install_disk_mount").toStringList();
	QStringList	os_install_disk_format = clConf->getValue("os_install_disk_format").toStringList();
	QStringList perform_format = clConf->getValue("os_install_disk_perform_format").toStringList();

	for (int i(0); i < os_disk_dev.size(); ++i)
	{
		if (
			(os_disk_part.at(i) == "extended") ||							// skip extended partition
			(os_disk_mount.at(i) == "/")									// skip root
		)
			continue;

		if ( !def_os_install_disk_mount.at(i).isEmpty() )
		{
			MountPoint	mp;
			mp.mountpoint = def_os_install_disk_mount.at(i);
			if (mp.mountpoint == "/")
				rootFound = true;

			mp.dev = os_disk_dev.at(i);
			mp.fs = os_disk_format.at(i);
			mp.size = os_disk_size.at(i);
			mp.label = os_disk_name.at(i);
			mp.format = (perform_format.at(i) == "yes");
			mp.fs_new = os_install_disk_format.at(i);
			mp.migrated = true;
			mp.migrated_dev = mp.dev;

			m_MountPointsList.append(mp);
		}
	}

	// if root not found, create default mount point for root partition and migrated mount points
	if ( !rootFound )
	{
		MountPoint	root;
		root.mountpoint = "/";

		m_MountPointsList.prepend( root );
	}

	getPartitions();
}

void PageMountPoints::generateCmdDisk()
{
	if ( CalculateConfig::instance()->getValue("gui_partitioning") != "auto" )
	{
		QStringList diskCfg = parseMountPoint();
		CalculateConfig::instance()->setValue( "--disk", diskCfg );

		QString swapCfg = parseSwap();
		CalculateConfig::instance()->setValue( "--swap", swapCfg );
	}
	CalculateConfig::instance()->showInstallParameters();
}

QStringList PageMountPoints::parseMountPoint()
{
	QStringList result;

	foreach(const MountPoint& mp, m_MountPointsList)
	{
		if (mp.mountpoint.isEmpty() || mp.dev.isEmpty() || mp.mountpoint == "swap")
			continue;

		if (mp.migrated && (mp.dev == "none") )
		{
			result << mp.migrated_dev + ":none";
		}
		else if (mp.migrated && mp.format )
		{
			result << mp.dev + ":" + mp.mountpoint + ":" + mp.fs_new;
		}
		else if (mp.dev != mp.migrated_dev)
		{
			result << mp.dev + ":" + mp.mountpoint + (mp.format ? (":" + mp.fs_new) : "");
		}
	}

	QList<int> toRemove;

	// remove none if other mount points selected
	for (int i(0); i != result.size(); ++i)
	{
		for (int j(i+1); j != result.size(); ++j)
		{
			if ( result.at(i).split(":").at(0) == result.at(j).split(":").at(0) )
			{
				if ( result.at(i).split(":").at(1) == "none" )
					toRemove << i;
				else
					toRemove << j;
				break;
			}
		}
	}

	if ( !toRemove.isEmpty() )
	{
		qSort(toRemove.begin(), toRemove.end(), qGreater<int>());
		foreach(const int& i, toRemove)
			result.removeAt(i);
	}

	return result;
}

QString PageMountPoints::parseSwap()
{
	QString result;

	foreach(const MountPoint& mp, m_MountPointsList)
	{
		if (mp.mountpoint != "swap")
			continue;

		if (mp.migrated && (mp.dev == "none") )
		{
			return "none";
		}
		else if (mp.dev != mp.migrated_dev)
		{
			return mp.dev;
		}
	}

	return QString();
}
