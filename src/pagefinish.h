#pragma once

#include "installerpage.h"

class QLabel;

class PageFinish : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageFinish();

	void retranslateUi();

protected:
	void setupUi();

public slots:
	void show();

signals:
	void toggleButtons(bool);

private:
	static const QString	m_messageComplete;

	QLabel*		m_labelComplete;

};


