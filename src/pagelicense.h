#pragma once

#include "installerpage.h"

class QTextEdit;
class QCheckBox;

class PageLicense : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageLicense();

	void retranslateUi();

protected:
	void setupUi();

public slots:
	void show();

private:
	QTextEdit*	m_textLicense;
	QCheckBox*	m_checkAccept;

};

