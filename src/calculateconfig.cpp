#include "calculateconfig.h"

#include <QProcess>
#include <QDebug>
#include <QMessageBox>

#include <QtXml>


CalculateConfig* CalculateConfig::_instance = 0;

CalculateConfig::CalculateConfig()
{
}

CalculateConfig::~CalculateConfig()
{
}

CalculateConfig* CalculateConfig::instance()
{
	if ( !_instance )
		_instance = new CalculateConfig();
	return _instance;
}

bool CalculateConfig::getDefaultConfig()
{
	m_Config.clear();

	// start cl-install -v and parse out
	QProcess	cl_install;

	qDebug() << "Start cl-install -v --xml --filter \"os_install|os_locale_lang$|os_disk|os_device|^cl_migrate_user$\"";

	cl_install.start(
		"cl-install -v --xml --filter \"os_install|os_locale_lang|^os_lang$|os_disk|os_device|^cl_migrate_user$\""
	);

	if ( !cl_install.waitForStarted() )
		return false;

	if ( !cl_install.waitForFinished() )
		return false;

	QString			outVars = cl_install.readAll();

	qDebug() << endl << outVars;

	QDomDocument	xmlVars;
	QString			errMsg;
	int				errLine;
	int				errColumn;

	if ( xmlVars.setContent(outVars, true, &errMsg, &errLine, &errColumn) )
	{
		QDomElement domRoot = xmlVars.documentElement();
		if (domRoot.tagName() == "variables") {
			parseVariables(domRoot, m_Config);
		} else {
			qDebug() << "Section \"variables\" not found";
			return false;
		}
	}

	// installer settings
	// gui_partitioning - install type: auto, manual
	m_Config["gui_install_language"] = m_Config["os_locale_lang"];

	QStringList	devs = m_Config["os_device_dev"].toStringList();
	m_Config.remove("os_device_dev");
	m_Config["os_device_dev"] = devs;

	if ( !m_Config["os_device_dev"].toStringList().isEmpty() )
		m_Config["gui_os_device_dev"] = m_Config["os_device_dev"].toStringList().at(0);
	m_Config["gui_os_device_dev_def"] = m_Config["gui_os_device_dev"];

	// copy default values
	m_Config["gui_os_install_net_hostname"] = m_Config["os_install_net_hostname"];
	m_Config["gui_os_install_net_domain"] = m_Config["os_install_net_domain"];
	m_Config["gui_os_install_net_domain"] = m_Config["os_install_net_domain"];
	m_Config["gui_os_install_clock_timezone"] = m_Config["os_install_clock_timezone"];
	m_Config["gui_os_install_x11_video_drv"] = m_Config["os_install_x11_video_drv"];
	m_Config["gui_os_install_x11_composite"] = m_Config["os_install_x11_composite"];
	m_Config["gui_os_install_makeopts"] = m_Config["os_install_makeopts"];
	m_Config["gui_os_install_proxy"] = m_Config["os_install_proxy"];
	m_Config["gui_os_install_ntp"] = m_Config["os_install_ntp"];
	m_Config["gui_os_install_clock_type"] = m_Config["os_install_clock_type"];

	m_Config["def_os_install_disk_format"] = m_Config["os_install_disk_format"];
	m_Config["def_os_install_disk_mount"] = m_Config["os_install_disk_mount"];
	m_Config["def_os_install_disk_perform_format"] = m_Config["os_install_disk_perform_format"];

	m_Config["def_cl_migrate_user"] = m_Config["cl_migrate_user"];

	qDebug() << endl << "Start variables: ";
	MapConfig::ConstIterator	cfgIt = m_Config.constBegin();
	while(cfgIt != m_Config.constEnd())
	{
		qDebug() <<
			cfgIt.key() +
			" = " +
			(( QString(cfgIt.value().typeName()) == "QStringList") ?
				("[" + cfgIt.value().toStringList().join(", ") + "]") :
				( cfgIt.value().toString() ) );
		++cfgIt;
	}


	return true;
}

bool CalculateConfig::getNewPartitioning()
{
	// start cl-install -v and parse out
	MapConfig	confDisk;
	QProcess	cl_install;

	qDebug() << "Start cl-install -v --xml --filter (os_disk|os_device|os_install_disk)";
	cl_install.start( "cl-install -v --xml --filter \"(os_disk|os_device|os_install_disk)\" " );

	if ( !cl_install.waitForStarted() )
		return false;

	if ( !cl_install.waitForFinished() )
		return false;

	QString			outVars = cl_install.readAll();

	QDomDocument	xmlVars;
	QString			errMsg;
	int				errLine;
	int				errColumn;

	if ( xmlVars.setContent(outVars, true, &errMsg, &errLine, &errColumn) )
	{
		QDomElement domRoot = xmlVars.documentElement();
		if (domRoot.tagName() == "variables") {
			parseVariables(domRoot, confDisk);
		} else {
			qDebug() << "Section \"variables\" not found";
			return false;
		}
	}

	QStringList	devs = confDisk["os_device_dev"].toStringList();

	m_Config.remove("os_device_dev");
	m_Config["os_device_dev"] = devs;

	m_Config.remove("os_disk_dev");
	m_Config["os_disk_dev"] = confDisk["os_disk_dev"];

	m_Config.remove("os_disk_format");
	m_Config["os_disk_format"] = confDisk["os_disk_format"];

	m_Config.remove("os_disk_name");
	m_Config["os_disk_name"] = confDisk["os_disk_name"];

	m_Config.remove("os_disk_part");
	m_Config["os_disk_part"] = confDisk["os_disk_part"];

	m_Config.remove("os_disk_size");
	m_Config["os_disk_size"] = confDisk["os_disk_size"];

	m_Config.remove("os_disk_mount");
	m_Config["os_disk_mount"] = confDisk["os_disk_mount"];

	m_Config.remove("os_install_disk_perform_format");
	m_Config["os_install_disk_perform_format"] = confDisk["os_install_disk_perform_format"];

	return true;
}

bool CalculateConfig::getNewMountpoints()
{
	// start cl-install -v and parse out
	MapConfig	confMp;
	QProcess	cl_install;

	QStringList	newMpParam = getDisksParameters();

	qDebug() << "Start cl-install " + newMpParam.join(" ") + " -v --xml --filter os_install_disk_";
	cl_install.start("cl-install " + newMpParam.join(" ") + " --color never -v --xml --filter os_install_disk_" );

	if ( !cl_install.waitForStarted() )
		return false;

	if ( !cl_install.waitForFinished() )
		return false;

	if ( cl_install.exitCode() )
	{
		QMessageBox::critical(0, tr("Critical error"), tr("cl-install error: ") + cl_install.readAllStandardError());
		return false;
	}

	QString			outVars = cl_install.readAll();

	QDomDocument	xmlVars;
	QString			errMsg;
	int				errLine;
	int				errColumn;

	if ( xmlVars.setContent(outVars, true, &errMsg, &errLine, &errColumn) )
	{
		QDomElement domRoot = xmlVars.documentElement();
		if (domRoot.tagName() == "variables") {
			parseVariables(domRoot, confMp);
		} else {
			qDebug() << "Section \"variables\" not found";
			return false;
		}
	}

	m_Config.remove("os_install_disk_format");
	m_Config["os_install_disk_format"] = confMp["os_install_disk_format"];

	m_Config.remove("os_install_disk_mount");
	m_Config["os_install_disk_mount"] = confMp["os_install_disk_mount"];

	m_Config.remove("os_install_disk_perform_format");
	m_Config["os_install_disk_perform_format"] = confMp["os_install_disk_perform_format"];

	return true;
}

QStringList CalculateConfig::getPasswordUsers()
{
	QStringList	result;

	MapConfig	migrateUser;

	QProcess	cl_install;

	QString usersParam;

	foreach( const QString& user, m_Config["gui_users"].toStringList() )
	{
		usersParam += " --user " + user;
	}

	qDebug() << "Start cl-install" +  usersParam + " -v --xml --filter migrate";
	cl_install.start("cl-install" + usersParam + " -v --xml --filter migrate" );

	if ( !cl_install.waitForStarted() )
	{
		qDebug() << "Process not started";
		return QStringList();
	}

	if ( !cl_install.waitForFinished() )
	{
		qDebug() << "Process finished with error";
		return QStringList();
	}

	QString			outVars = cl_install.readAll();

	QDomDocument	xmlVars;
	QString			errMsg;
	int				errLine;
	int				errColumn;

	if ( xmlVars.setContent(outVars, true, &errMsg, &errLine, &errColumn) )
	{
		QDomElement domRoot = xmlVars.documentElement();
		if (domRoot.tagName() == "variables") {
			parseVariables(domRoot, migrateUser);
		} else {
			qDebug() << "Section \"variables\" not found";
			return QStringList();
		}
	}

	if ( migrateUser["cl_migrate_user"].toStringList().count() !=
		migrateUser["cl_migrate_user_pwd"].toStringList().count()
	)
	{
		qDebug() << "Error result";
		return QStringList();
	}

	for ( int i(0); i != migrateUser["cl_migrate_user"].toStringList().count(); ++i )
	{
		if ( migrateUser["cl_migrate_user_pwd"].toStringList().at(i) == "yes" )
			result << migrateUser["cl_migrate_user"].toStringList().at(i);
	}

	return result;
}

void CalculateConfig::parseVariables(const QDomNode& node, MapConfig& cfg)
{
	QDomNode domNode = node.firstChild();
	while ( !domNode.isNull() )
	{
		if ( domNode.isElement() )
		{
			QDomElement domElement = domNode.toElement();
			if ( !domElement.isNull() )
			{
				if (domElement.tagName() == "var")
				{
					if (domElement.attribute("type") == "var")
					{
						parseVar(domElement, cfg);
					}
					else if (domElement.attribute("type") == "list")
					{
						parseVarList(domElement, cfg);
					}
				}
			}
		}
		domNode = domNode.nextSibling();
	}
}

void CalculateConfig::parseVar(const QDomNode& node, MapConfig& cfg)
{
	QString		name = node.toElement().attribute("name");
	QDomNode 	domNode = node.firstChild();

	while ( !domNode.isNull() )
	{
		if ( domNode.isElement() )
		{
			QDomElement domElement = domNode.toElement();
			if ( !domElement.isNull() )
			{
				if (domElement.tagName() == "value")
				{
					//qDebug() << "var  " << name << " = " << domElement.text();
					cfg[name] = QVariant( domElement.text() );
				}
			}
		}
		domNode = domNode.nextSibling();
	}
}

void CalculateConfig::parseVarList(const QDomNode& node, MapConfig& cfg)
{
	QStringList	resList;
	QString		name = node.toElement().attribute("name");
	QDomNode 	domNode = node.firstChild();

	while ( !domNode.isNull() )
	{
		if ( domNode.isElement() )
		{
			QDomElement domElement = domNode.toElement();
			if ( !domElement.isNull() )
			{
				if (domElement.tagName() == "value")
				{
					//qDebug() << "list " << name << " = " << domElement.text();
					resList << domElement.text();
				}
			}
		}
		domNode = domNode.nextSibling();
	}
	if ( !resList.isEmpty() )
	{
		cfg[name] = QVariant( resList );
	}
}

void CalculateConfig::setValue ( const QString& key, const QVariant& value )
{
	if ( m_Config.contains(key) )
		m_Config.remove(key);

	m_Config[key] = value;
}

void CalculateConfig::showInstallParameters()
{
	QStringList params = getInstallParameters();

	emit sendParameters(params);
}

QStringList CalculateConfig::getDisksParameters()
{
	QStringList params;

	if ( m_Config["--disk"].toStringList().count() > 0 )
	{
		foreach(const QString& disk, m_Config["--disk"].toStringList())
		{
			QString disk_t(disk);
			disk_t = disk_t.simplified();
			params << QString("--disk=") + disk_t;
		}
	}

	if ( !m_Config["--swap"].toString().isEmpty() )
		params << QString("--swap=") + m_Config["--swap"].toString();

	return params;
}

QStringList CalculateConfig::getInstallParameters()
{
	QStringList params;

	// builder
	if ( m_Config["gui_os_install_builder"].toString() == "on" )
		params << "--build";

	// disks
	params << getDisksParameters();

	// mbr
	QString mbr_dev = m_Config["gui_os_device_dev"].toString();
	if ( !mbr_dev.isEmpty() && (mbr_dev != m_Config["gui_os_device_dev_def"].toString()) )
	{
		if (mbr_dev == "-")
			mbr_dev = "off";
		params << QString("--mbr=") + mbr_dev;
	}

	// config
	QString hostname = m_Config["gui_os_install_net_hostname"].toString();
	if ( !hostname.isEmpty() && (hostname != m_Config["os_install_net_hostname"].toString()) )
		params << "--set os_install_net_hostname=" + hostname;

	QString domain = m_Config["gui_os_install_net_domain"].toString();
	if ( !domain.isEmpty() && (domain != m_Config["os_install_net_domain"].toString()) )
		params << "--set os_install_net_domain=" + domain;

	QString timezone = m_Config["gui_os_install_clock_timezone"].toString();
	if ( !timezone.isEmpty() && (timezone != m_Config["os_install_clock_timezone"].toString()) )
		params << "--set os_install_clock_timezone=" + timezone;

	QString video_drv = m_Config["gui_os_install_x11_video_drv"].toString();
	if ( !video_drv.isEmpty() && (video_drv != m_Config["os_install_x11_video_drv"].toString()) )
		params << "--set os_install_x11_video_drv=" + video_drv;

	QString composite = m_Config["gui_os_install_x11_composite"].toString();
	if ( !composite.isEmpty() && (composite != m_Config["os_install_x11_composite"].toString()) )
		params << "--set os_install_x11_composite=" + composite;

	if ( m_Config["gui_expert_mode"].toString() == "on" )
	{
		QString	makeopts = m_Config["gui_os_install_makeopts"].toString();
		if ( !makeopts.isEmpty() && (makeopts != m_Config["os_install_makeopts"].toString()) )
			params << "--set os_install_makeopts=" + makeopts;

		QString proxy = m_Config["gui_os_install_proxy"].toString();
		if ( !proxy.isEmpty() && (proxy != m_Config["os_install_proxy"].toString()) )
			params << "--set os_install_proxy=" + proxy;

		QString ntp = m_Config["gui_os_install_ntp"].toString();
		if ( !ntp.isEmpty() && (ntp != m_Config["os_install_ntp"].toString()) )
			params << "--set os_install_ntp=" + ntp;

		QString clock_type = m_Config["gui_os_install_clock_type"].toString();
		if ( !clock_type.isEmpty() && (clock_type != m_Config["os_install_clock_type"].toString()) )
			params << "--set os_install_clock_type=" + clock_type;
	}

	// lang
	QString lang = m_Config["gui_install_language"].toString();
	if ( !lang.isEmpty() && ( lang != m_Config["os_locale_lang"].toString()))
		params << "--lang=" + lang;

	// users
	if ( m_Config["gui_users"].toStringList().count() > 0 )
	{
		foreach(const QString& user, m_Config["gui_users"].toStringList())
			params << QString("--user=") + user;
	}

	return params;
}
