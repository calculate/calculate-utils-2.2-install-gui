#include "pagecfdisk.h"

#include <QBoxLayout>
#include <QLabel>
#include <QFont>

#include <QMessageBox>

#include "qtermwidget/qtermwidget.h"

PageCfdisk::PageCfdisk( const QString& command, const QString& disk) :
	InstallerPage(),
	m_Cmd(command),
	m_Disk(disk),
	m_Lang("C"),
	m_envTerm("xterm")
{
	setupUi();

	connect(m_Term, SIGNAL(finished()), this, SIGNAL(completed()));
	m_Term->startShellProgram();
}


void PageCfdisk::setupUi()
{
	m_labelHelp = new QLabel;
	m_widgetTerm = new QWidget;
	m_widgetTerm->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	m_Term = new QTermWidget(0);

	QFont	font = QApplication::font();
	font.setFamily("Droid Sans Mono");
	font.setPointSize(10);

	m_Term->setTerminalFont(font);
	m_Term->setParent(m_widgetTerm);
	m_Term->setColorScheme(COLOR_SCHEME_BLACK_ON_LIGHT_YELLOW);

	QVBoxLayout*	vbox_1 = new QVBoxLayout;

	vbox_1->addWidget(m_labelHelp);
	vbox_1->addWidget(m_widgetTerm);

	QHBoxLayout* hbox_1 = new QHBoxLayout;

	hbox_1->addWidget(m_Term);
	m_widgetTerm->setLayout(hbox_1);

	setLayout(vbox_1);

	retranslateUi();
}

void PageCfdisk::retranslateUi()
{
	setTitle( tr("Partitioning") );
	m_labelHelp->setText( tr("Do manual partitioning. To finish, exit from %1").arg(m_Cmd) );
}

void PageCfdisk::show()
{
	QString	cmd = QString("TERM='%1' LANGUAGE='%2' %3 %4").arg(m_envTerm).arg(m_Lang).arg(m_Cmd).arg(m_Disk) + "; exit \r\n";
	m_Term->sendText( cmd );
	m_Term->setFocus();
}

