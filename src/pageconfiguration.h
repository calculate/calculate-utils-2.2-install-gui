#pragma once

#include "installerpage.h"
#include "tools.h"
#include "commons.h"

class QComboBox;
class QLineEdit;
class QLabel;
class QCheckBox;

class PageConfiguration : public InstallerPage
{
	Q_OBJECT
public:
	explicit PageConfiguration();

	bool validate();
	void retranslateUi();

protected:
	void setupUi();

private:
	void mapSignals();
	void unmapSignals();

	void setupTimezones();

public slots:
	void show();

private slots:
	void updateData();

private:
	QLabel*		m_labelSelectParams;

	QLabel*		m_labelHostName;
	QLineEdit*	m_editHostName;

	QLabel*		m_labelDomainName;
	QLineEdit*	m_editDomainName;

	QLabel*		m_labelLanguage;
	QComboBox*	m_cmbboxLanguage;

	QLabel*		m_labelTimezone;
	QComboBox*	m_cmbboxTimezone;

	QLabel*		m_labelMbrDevice;
	QComboBox*	m_cmbboxMbrDevice;

	QLabel*		m_labelVideoDrv;
	QComboBox*	m_cmbboxVideoDrv;

	QLabel*		m_labelComposite;
	QCheckBox*	m_chkboxComposite;

	QCheckBox*	m_chkboxExtParameters;
	QWidget*	m_widgetExtParameters;

	QLabel*		m_labelBuilder;
	QCheckBox*	m_chkboxBuilder;

	QLabel*		m_labelMakeOpts;
	QLineEdit*	m_editMakeOpts;

	QLabel*		m_labelProxy;
	QLineEdit*	m_editProxy;

	QLabel*		m_labelNtp;
	QLineEdit*	m_editNtp;

	QLabel*		m_labelClockType;
	QComboBox*	m_cmbboxClockType;

	QMap<QString, QString>	m_langMap;
};

