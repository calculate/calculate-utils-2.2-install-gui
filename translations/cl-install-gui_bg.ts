<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="bg_BG" sourcelanguage="en_US">
<context>
    <name>CalculateConfig</name>
    <message>
        <source>Critical error</source>
        <translation type="unfinished">Критична грешка</translation>
    </message>
    <message>
        <source>cl-install error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MountPointDialog</name>
    <message>
        <source>Device: </source>
        <translation type="obsolete">Девиз:</translation>
    </message>
    <message>
        <source>Mount point: </source>
        <translation type="unfinished">Точка на монтиране:</translation>
    </message>
    <message>
        <source>Format partition</source>
        <translation type="unfinished">Форматиране на дял</translation>
    </message>
    <message>
        <source>File system: </source>
        <translation type="unfinished">Файлова система:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">Добре</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Отказ</translation>
    </message>
    <message>
        <source>Partition: </source>
        <translation type="unfinished">Дял:</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">Предупреждение</translation>
    </message>
    <message>
        <source>Mount point must be not empty</source>
        <translation type="unfinished">Точка на монтиране не може да бъде празна</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="unfinished">Точка за монтиране на %1 не може да бъде избрана повече от един път</translation>
    </message>
</context>
<context>
    <name>PageCfdisk</name>
    <message>
        <source>Partitioning</source>
        <translation type="unfinished">Разделяне</translation>
    </message>
    <message>
        <source>Do manual partitioning. To finish, exit from %1</source>
        <translation type="unfinished">Разделете ръчно. За да запазите промените излезте от %1</translation>
    </message>
</context>
<context>
    <name>PageConfiguration</name>
    <message>
        <source>Configuring</source>
        <translation type="unfinished">Конфигуриране</translation>
    </message>
    <message>
        <source>Select parameters: </source>
        <translation type="unfinished">Изберете параметри:</translation>
    </message>
    <message>
        <source>Hostname: </source>
        <translation type="unfinished">Име на хост:</translation>
    </message>
    <message>
        <source>Domain: </source>
        <translation type="unfinished">Домейн:</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation type="unfinished">Език:</translation>
    </message>
    <message>
        <source>Timezone:</source>
        <translation type="unfinished">Часови Пояс:</translation>
    </message>
    <message>
        <source>Device for install Grub:</source>
        <translation type="unfinished">Девиз за инсталиране на GRUB:</translation>
    </message>
    <message>
        <source>Video driver:</source>
        <translation type="unfinished">Видео Драйвер:</translation>
    </message>
    <message>
        <source>Use desktop effects</source>
        <translation type="unfinished">Използвайте Десктоп ефекти</translation>
    </message>
    <message>
        <source>Expert settings</source>
        <translation type="unfinished">Експертни настройки</translation>
    </message>
    <message>
        <source>Make options (MAKEOPTS):</source>
        <translation type="unfinished">Опции MAKEOPTS:</translation>
    </message>
    <message>
        <source>Proxy server:</source>
        <translation type="unfinished">Прокси сървър:</translation>
    </message>
    <message>
        <source>NTP server:</source>
        <translation type="unfinished">NTP сървър:</translation>
    </message>
    <message>
        <source>Clock type:</source>
        <translation type="unfinished">Часовник:</translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="unfinished">Локално време</translation>
    </message>
    <message>
        <source>UTC</source>
        <translation type="unfinished">UTC</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">Предупреждение</translation>
    </message>
    <message>
        <source>Hostname is empty.</source>
        <translation type="unfinished">Име на хост не трябва да е празно.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="unfinished">Беларус</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="unfinished">България</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="unfinished">Дания</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="unfinished">Англия [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="unfinished">Англия [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="unfinished">Франция [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="unfinished">Франция [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="unfinished">Франция [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="unfinished">Германия</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="unfinished">Исландия</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished">Италия</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="unfinished">Полша</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="unfinished">Португалия</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="unfinished">Русия</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished">Швеция</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="unfinished">Испания</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="unfinished">Норвегия</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="unfinished">Украйна</translation>
    </message>
    <message>
        <source>Installation for assembling</source>
        <translation type="unfinished">Инсталация за сглобяване</translation>
    </message>
</context>
<context>
    <name>PageFinish</name>
    <message>
        <source>Complete</source>
        <translation type="unfinished">Завърши</translation>
    </message>
    <message>
        <source>&lt;h4&gt;Congratulation!&lt;/h4&gt;&lt;p&gt;Installation complete.Press Finish for exit.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;h4&gt;Поздравления!&lt;/h4&gt;&lt;p&gt;Инсталацията завърши.Може да рестартирате.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PageInstall</name>
    <message>
        <source>Installing</source>
        <translation type="unfinished">Инсталиране</translation>
    </message>
    <message>
        <source>Error. Additional information in /var/log/calculate/cl-install-gui-err.log</source>
        <translation type="unfinished">Грешка. Информацията е съхранена в файла /var/log/calculate/cl-install.gui-err.log</translation>
    </message>
    <message>
        <source>Users parsing error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageLicense</name>
    <message>
        <source>License</source>
        <translation type="unfinished">Лиценз</translation>
    </message>
    <message>
        <source>&lt;h3&gt;License&lt;/h3&gt;&lt;h4&gt;License Agreement&lt;/h4&gt;&lt;p&gt;This operating system (the OS) is composed of many individual software components, the copyrights on each of which belong to their respective owners. Each component is distributed under their own license agreement.&lt;/p&gt;&lt;p&gt;Installing, modifying or distributing this operating system, given to you as free archive, you agree with all of the following.&lt;/p&gt;&lt;h4&gt;Warranties&lt;/h4&gt;&lt;p&gt;This software is distributed without warranty of any kind. You assume all responsibility for the use of the operating system.&lt;/p&gt;&lt;h4&gt;Installing&lt;/h4&gt;&lt;p&gt;OS can be installed on any number of computers.&lt;/p&gt;&lt;h4&gt;Distribution&lt;/h4&gt;&lt;p&gt;Most of the software included in this operating system, allows you to freely modify, copy and distribute it. Also included in the OS software is distributed in the different conditions. For more information please refer to the documentation accompanying a particular software component.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;h3&gt;License&lt;/h3&gt;&lt;h4&gt;License Agreement&lt;/h4&gt;&lt;p&gt;This operating system (the OS) is composed of many individual software components, the copyrights on each of which belong to their respective owners. Each component is distributed under their own license agreement.&lt;/p&gt;&lt;p&gt;Installing, modifying or distributing this operating system, given to you as free archive, you agree with all of the following.&lt;/p&gt;&lt;h4&gt;Warranties&lt;/h4&gt;&lt;p&gt;This software is distributed without warranty of any kind. You assume all responsibility for the use of the operating system.&lt;/p&gt;&lt;h4&gt;Installing&lt;/h4&gt;&lt;p&gt;OS can be installed on any number of computers.&lt;/p&gt;&lt;h4&gt;Distribution&lt;/h4&gt;&lt;p&gt;Most of the software included in this operating system, allows you to freely modify, copy and distribute it. Also included in the OS software is distributed in the different conditions. For more information please refer to the documentation accompanying a particular software component.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="unfinished">Приемам</translation>
    </message>
</context>
<context>
    <name>PageMountPoints</name>
    <message>
        <source>Mount points</source>
        <translation>Точки на монтиране</translation>
    </message>
    <message>
        <source>Partition</source>
        <translation>Дял</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Етикет</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translation type="obsolete">Точка на монтиране</translation>
    </message>
    <message>
        <source>File system</source>
        <translation type="obsolete">Файлова система</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Форматиране</translation>
    </message>
    <message>
        <source>Select mount points:</source>
        <translation type="obsolete">Изберете точки на монтиране:</translation>
    </message>
    <message>
        <source>Select the mount points use double click to partitions. For continue must be set mount point for /</source>
        <translation type="obsolete">Изберете точки на монтиране с двойно кликане на нужния дял. За да продължите е нужно да зададете точка на монтиране</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <source>You select auto partitioning. Press &quot;Next&quot; to continue.</source>
        <translation>Избрахте автоматично разделяне. Натиснете Напред за да продължите.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <source>Select mount point for /</source>
        <translation type="obsolete">Изберете точка на монтиране за главния дял /</translation>
    </message>
    <message>
        <source>no</source>
        <translation>не</translation>
    </message>
    <message>
        <source>YES</source>
        <translation>ДА</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="obsolete">Точка за монтиране на %1 не може да бъде избрана повече от един път</translation>
    </message>
    <message>
        <source>Add a new mount point</source>
        <translation type="unfinished">Добавете нова точка на монтиране</translation>
    </message>
    <message>
        <source>Remove a selected mount point</source>
        <translation type="unfinished">Премахнете избраната точка на монтиране</translation>
    </message>
    <message>
        <source>Mount
point</source>
        <translation type="unfinished">Точка на
монтиране</translation>
    </message>
    <message>
        <source>File
system</source>
        <translation type="unfinished">Файлова
система</translation>
    </message>
    <message>
        <source>Choose mount points for the installation. You need at least a root / partition.
To modify the mount point, double-click it.</source>
        <translation type="unfinished">Изберете точка на монтиране за инсталацията. Минимално необходимо е да изберете дял за / .
За редактиране на създадена точка за монтиране, кликнете два пъти на нея.</translation>
    </message>
    <message>
        <source>To continue You need to choose partition for mount point %1.</source>
        <translation>За да продължите изберете раздел за точка на монтиране %1</translation>
    </message>
    <message>
        <source>Adding is not possible. All partitions is busy.</source>
        <translation type="unfinished">Не може да се добави. Всички дялове вече се използват.</translation>
    </message>
    <message>
        <source>Root partition can&apos;t be deleted</source>
        <translation>Дял / не може да бъде изтрит.</translation>
    </message>
    <message>
        <source>Migrated mount point can&apos;t be deleted. Use &apos;none&apos; for disabling migration.</source>
        <translation type="unfinished">Точка на монтиране която ще се използва в новата система не може да бъде изтрита. Използвайте none в име на раздела за деактивиране на прехвърлянето.</translation>
    </message>
</context>
<context>
    <name>PagePartitioning</name>
    <message>
        <source>Partitioning</source>
        <translation type="unfinished">Разделяне</translation>
    </message>
    <message>
        <source>Disk for install: </source>
        <translation type="unfinished">Диск за инсталиране:</translation>
    </message>
    <message>
        <source>Use existing partitions</source>
        <translation type="unfinished">Използване на съществуващ дял</translation>
    </message>
    <message>
        <source>Use automatically partitioning</source>
        <translation type="unfinished">Използване на автоматично разделяне на диска</translation>
    </message>
    <message>
        <source>Manually partitioning</source>
        <translation type="unfinished">Ръчно разделяне</translation>
    </message>
    <message>
        <source>Selected disk will be used for manual or automatic partitioning.</source>
        <translation type="unfinished">Избрания диск ще бъде използван за ръчно или автоматично разделяне.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Грешка</translation>
    </message>
    <message>
        <source>Disks not found</source>
        <translation type="unfinished">Избрания диск не съществува</translation>
    </message>
</context>
<context>
    <name>PageUsers</name>
    <message>
        <source>Users</source>
        <translation type="unfinished">Потребители</translation>
    </message>
    <message>
        <source>Set root password:</source>
        <translation type="unfinished">Парола за root:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished">Парола</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translation type="unfinished">Потвърждаване на паролата</translation>
    </message>
    <message>
        <source>Add user</source>
        <translation type="unfinished">Добави потребител</translation>
    </message>
    <message>
        <source>Remove selected user</source>
        <translation type="unfinished">Премахни избрания потребител</translation>
    </message>
    <message>
        <source>Added users.
For modifying user - double click it.</source>
        <translation type="unfinished">Добавяне на потребители.
За да редактирате данните за потребителите - кликнете два пъти върху избрания.</translation>
    </message>
    <message>
        <source>Create users:</source>
        <translation type="unfinished">Създаване на потребители:</translation>
    </message>
    <message>
        <source>Root password will be moved from current system.</source>
        <translation type="obsolete">Паролата за потребител root ще бъде преместена в текущата система.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Грешка</translation>
    </message>
    <message>
        <source>User %1 already exists.</source>
        <translation type="unfinished">Потребител %1 вече съществува.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be deleted.</source>
        <translation type="obsolete">Потребител guest не може да бъде изтрит.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be modified.</source>
        <translation type="obsolete">Потребител guest не може да бъде променян.</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation type="unfinished">Паролите съвпадат</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation type="unfinished">Паролите не съвпадат</translation>
    </message>
    <message>
        <source>These user accounts will be migrated: </source>
        <translation type="unfinished">Профилите на тези потребители ще бъдат прехвърлени:</translation>
    </message>
    <message>
        <source>If the users will not be added, the &apos;guest&apos; user be created automatically.</source>
        <translation type="unfinished">Ако не бъдат добавени потребители, потребител &apos;guest&apos; ще бъде създаден автоматично.</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <source>Welcome</source>
        <translation type="unfinished">Добре дошли</translation>
    </message>
    <message>
        <source>&lt;p&gt;Welcome to Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.org&apos;&gt;http://www.calculate-linux.org&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;Добре дошли в Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.org&apos;&gt;http://www.calculate-linux.org&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Choose a language: </source>
        <translation type="unfinished">Изберете език:</translation>
    </message>
    <message>
        <source>Please choose the language which should be used for this application.</source>
        <translation type="unfinished">Изберете език който да бъде използван по време на инсталацията.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="unfinished">Грешка</translation>
    </message>
    <message>
        <source>You do not have administrative privileges.</source>
        <translatorcomment>Вы не имеете привелегий суперпользователя.</translatorcomment>
        <translation type="unfinished">Вие нямате привилегии на суперпотребител.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="obsolete">Беларус</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="obsolete">България</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="obsolete">Дания</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="obsolete">Англия [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="obsolete">Англия [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="obsolete">Франция [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="obsolete">Франция [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="obsolete">Франция [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="obsolete">Германия</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="obsolete">Исландия</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Италия</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Полша</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Португалия</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Русия</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Швеция</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Испания</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="obsolete">Норвегия</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="obsolete">Украйна</translation>
    </message>
</context>
<context>
    <name>SystemInstaller</name>
    <message>
        <source>Critical error</source>
        <translation type="unfinished">Критична грешка</translation>
    </message>
    <message>
        <source>Failed to launch &apos;cl-install&apos;.</source>
        <translation>Неуспешно стартиране на &apos;cl-install&apos;</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished">Назад</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="unfinished">Напред</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation type="unfinished">Финал</translation>
    </message>
    <message>
        <source>About</source>
        <translatorcomment>О программе</translatorcomment>
        <translation type="unfinished">За програмата</translation>
    </message>
    <message>
        <source>Copy</source>
        <translatorcomment>Копировать</translatorcomment>
        <translation type="unfinished">Копиране</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="unfinished">Внимание</translation>
    </message>
    <message>
        <source>Do you want to abort the installation now?</source>
        <translation type="unfinished">Искате ли да прекратите инсталацията?</translation>
    </message>
    <message>
        <source>About cl-install-gui</source>
        <translation type="unfinished">За програмата calculate-install-gui</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графичен интерфейс за програмата cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчици:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Преводачи:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>Calculate Linux installer</source>
        <translation type="unfinished">Програма за инсталиране на Calculate Linux</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished">Не</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графичен интерфейс за програмата cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчици:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Преводачи:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="unfinished">&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;Графичен интерфейс за програмата cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчици:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Преводачи:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
</context>
<context>
    <name>UserInfoDialog</name>
    <message>
        <source>Add user</source>
        <translation type="unfinished">Добави потребител</translation>
    </message>
    <message>
        <source>Modify user</source>
        <translation type="unfinished">Промени потребител</translation>
    </message>
    <message>
        <source>User name:</source>
        <translation type="unfinished">Потребителско име:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished">Парола</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translation type="unfinished">Потвърди парола</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">Добре</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Отказ</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation type="unfinished">Паролите съвпадат</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation type="unfinished">Паролите не съвпадат</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Грешка</translation>
    </message>
    <message>
        <source>User name is empty</source>
        <translation type="unfinished">Потребителско име не трябва да бъде празно</translation>
    </message>
    <message>
        <source>User root can&apos;t added</source>
        <translation type="unfinished">Потребител root не може да бъде добавен</translation>
    </message>
</context>
</TS>
