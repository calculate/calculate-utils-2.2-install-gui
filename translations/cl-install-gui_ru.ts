<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU" sourcelanguage="en_US">
<context>
    <name>CalculateConfig</name>
    <message>
        <source>Critical error</source>
        <translation type="unfinished">Критическая ошибка</translation>
    </message>
    <message>
        <source>cl-install error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MountPointDialog</name>
    <message>
        <source>Device: </source>
        <translation type="obsolete">Раздел:</translation>
    </message>
    <message>
        <source>Mount point: </source>
        <translation>Точка монтирования:</translation>
    </message>
    <message>
        <source>Format partition</source>
        <translation>Форматировать раздел</translation>
    </message>
    <message>
        <source>File system: </source>
        <translation>Файловая система:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Partition: </source>
        <translation type="unfinished">Раздел:</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">Предупреждение</translation>
    </message>
    <message>
        <source>Mount point must be not empty</source>
        <translation type="unfinished">Точка монтирования не должна быть пустой</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="unfinished">Точка монтирования %1 должна быть выбрана не более одного раза</translation>
    </message>
</context>
<context>
    <name>PageCfdisk</name>
    <message>
        <source>Partitioning</source>
        <translation>Разметка</translation>
    </message>
    <message>
        <source>Do manual partitioning. To finish, exit from %1</source>
        <translation>Разметьте диск вручную. Для окончания, выйдите из %1</translation>
    </message>
</context>
<context>
    <name>PageConfiguration</name>
    <message>
        <source>Configuring</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <source>Select parameters: </source>
        <translation>Выберите параметры:</translation>
    </message>
    <message>
        <source>Hostname: </source>
        <translation>Имя хоста:</translation>
    </message>
    <message>
        <source>Domain: </source>
        <translation>Домен:</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <source>Timezone:</source>
        <translation>Часовой пояс:</translation>
    </message>
    <message>
        <source>Device for install Grub:</source>
        <translation>Диск для установки загрузчика:</translation>
    </message>
    <message>
        <source>Video driver:</source>
        <translation>Видео драйвер:</translation>
    </message>
    <message>
        <source>Use desktop effects</source>
        <translation>Использовать эфекты рабочего стола</translation>
    </message>
    <message>
        <source>Expert settings</source>
        <translation>Расширенные настройки</translation>
    </message>
    <message>
        <source>Make options (MAKEOPTS):</source>
        <translation>Опции MAKEOPTS:</translation>
    </message>
    <message>
        <source>Proxy server:</source>
        <translation type="unfinished">Proxy сервер:</translation>
    </message>
    <message>
        <source>NTP server:</source>
        <translation>NTP сервер:</translation>
    </message>
    <message>
        <source>Clock type:</source>
        <translation>Часы:</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Локальное время</translation>
    </message>
    <message>
        <source>UTC</source>
        <translation type="unfinished">UTC</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Hostname is empty.</source>
        <translation>Имя хоста не должно быть пустым.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="unfinished">Беларусский</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="unfinished">Болгарский</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="unfinished">Датский</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="unfinished">Английский [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="unfinished">Английский [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="unfinished">Французский [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="unfinished">Французский [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="unfinished">Французский [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="unfinished">Немецкий</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="unfinished">Исландский</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished">Итальянский</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="unfinished">Польский</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="unfinished">Португальский</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="unfinished">Русский</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished">Шведский</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="unfinished">Испанский</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="unfinished">Норвежский</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="unfinished">Украинский</translation>
    </message>
    <message>
        <source>Installation for assembling</source>
        <translation type="unfinished">Установка для сборки</translation>
    </message>
</context>
<context>
    <name>PageFinish</name>
    <message>
        <source>Complete</source>
        <translation>Завершение</translation>
    </message>
    <message>
        <source>&lt;h4&gt;Congratulation!&lt;/h4&gt;&lt;p&gt;Installation complete.Press Finish for exit.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Поздравляем!&lt;/h4&gt;&lt;p&gt;Установка успешно завершена. Нажмите &quot;Закончить&quot; для выхода из программы установки.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PageInstall</name>
    <message>
        <source>Installing</source>
        <translation>Установка</translation>
    </message>
    <message>
        <source>Error. Additional information in /var/log/calculate/cl-install-gui-err.log</source>
        <translation>Ошибка. Дополнительная информация сохранена в файле /var/log/calculate/cl-install-gui-err.log</translation>
    </message>
    <message>
        <source>Users parsing error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageLicense</name>
    <message>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <source>&lt;h3&gt;License&lt;/h3&gt;&lt;h4&gt;License Agreement&lt;/h4&gt;&lt;p&gt;This operating system (the OS) is composed of many individual software components, the copyrights on each of which belong to their respective owners. Each component is distributed under their own license agreement.&lt;/p&gt;&lt;p&gt;Installing, modifying or distributing this operating system, given to you as free archive, you agree with all of the following.&lt;/p&gt;&lt;h4&gt;Warranties&lt;/h4&gt;&lt;p&gt;This software is distributed without warranty of any kind. You assume all responsibility for the use of the operating system.&lt;/p&gt;&lt;h4&gt;Installing&lt;/h4&gt;&lt;p&gt;OS can be installed on any number of computers.&lt;/p&gt;&lt;h4&gt;Distribution&lt;/h4&gt;&lt;p&gt;Most of the software included in this operating system, allows you to freely modify, copy and distribute it. Also included in the OS software is distributed in the different conditions. For more information please refer to the documentation accompanying a particular software component.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Принимаю</translation>
    </message>
</context>
<context>
    <name>PageMountPoints</name>
    <message>
        <source>Mount points</source>
        <translation>Точки монтирования</translation>
    </message>
    <message>
        <source>Partition</source>
        <translation>Раздел</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Метка</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translation type="obsolete">Точка монтирования</translation>
    </message>
    <message>
        <source>File system</source>
        <translation type="obsolete">Файловая система</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Форматировать</translation>
    </message>
    <message>
        <source>Select mount points:</source>
        <translation type="obsolete">Установите точки монтирования:</translation>
    </message>
    <message>
        <source>Select the mount points use double click to partitions. For continue must be set mount point for /</source>
        <translation type="obsolete">Установите точки монтирования используя двойной клик на нужном разделе. Для продолжения должна быть установлена точка монтирования для корневого раздела (/)</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <source>You select auto partitioning. Press &quot;Next&quot; to continue.</source>
        <translation>Вы выбрали автоматическу разметку диска программой установки. Нажмите &quot;Вперед&quot; для продолжения.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Select mount point for /</source>
        <translation type="obsolete">Выберите точку монтирования для корневого раздела /</translation>
    </message>
    <message>
        <source>no</source>
        <translation>нет</translation>
    </message>
    <message>
        <source>YES</source>
        <translation>ДА</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="obsolete">Точка монтирования %1 должна быть выбрана не более одного раза</translation>
    </message>
    <message>
        <source>Add a new mount point</source>
        <translation type="unfinished">Добавить новую точку монтирования</translation>
    </message>
    <message>
        <source>Remove a selected mount point</source>
        <translation type="unfinished">Удалить выбранную точку монтирования</translation>
    </message>
    <message>
        <source>Mount
point</source>
        <translation type="unfinished">Точка
монтирования</translation>
    </message>
    <message>
        <source>File
system</source>
        <translation type="unfinished">Файловая
система</translation>
    </message>
    <message>
        <source>Choose mount points for the installation. You need at least a root / partition.
To modify the mount point, double-click it.</source>
        <translation type="unfinished">Выберите точки монтирования для установки. Минимально необходимо выбрать раздел для / раздела.
Для редактирования созданых точек монтирования, используйте двойной щелчек на ней.</translation>
    </message>
    <message>
        <source>To continue You need to choose partition for mount point %1.</source>
        <translation>Для продолжения выберите раздел для точки монтирования %1</translation>
    </message>
    <message>
        <source>Adding is not possible. All partitions is busy.</source>
        <translation type="unfinished">Добавление не возможно. Все разделы уже используются.</translation>
    </message>
    <message>
        <source>Root partition can&apos;t be deleted</source>
        <translation>Раздел / не может быть удален.</translation>
    </message>
    <message>
        <source>Migrated mount point can&apos;t be deleted. Use &apos;none&apos; for disabling migration.</source>
        <translation type="unfinished">Переносимая точка монтирования не может быть удалена. Используйте &apos;none&apos; в имени раздела для деактивации переноса этой точки.</translation>
    </message>
</context>
<context>
    <name>PagePartitioning</name>
    <message>
        <source>Partitioning</source>
        <translation>Разметка</translation>
    </message>
    <message>
        <source>Disk for install: </source>
        <translation>Диск для установки:</translation>
    </message>
    <message>
        <source>Use existing partitions</source>
        <translation>Использовать существующие разделы</translation>
    </message>
    <message>
        <source>Use automatically partitioning</source>
        <translation>Использовать автоматическую разметку диска</translation>
    </message>
    <message>
        <source>Manually partitioning</source>
        <translation>Ручная разметка</translation>
    </message>
    <message>
        <source>Selected disk will be used for manual or automatic partitioning.</source>
        <translation>Выбранный диск будет использоваться для ручной или автоматической разметки.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Disks not found</source>
        <translation>Диски не найдены</translation>
    </message>
</context>
<context>
    <name>PageUsers</name>
    <message>
        <source>Users</source>
        <translation>Пользователи</translation>
    </message>
    <message>
        <source>Set root password:</source>
        <translation>Установите пароль для root:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translation>Подтверждение пароля</translation>
    </message>
    <message>
        <source>Add user</source>
        <translation>Добавить пользователя</translation>
    </message>
    <message>
        <source>Remove selected user</source>
        <translation>Удалить выбранного пользователя</translation>
    </message>
    <message>
        <source>Added users.
For modifying user - double click it.</source>
        <translation>Добавленные пользователи.
Для редактирования данных пользователя - дважды кликните его.</translation>
    </message>
    <message>
        <source>Create users:</source>
        <translation>Создать пользователя:</translation>
    </message>
    <message>
        <source>Root password will be moved from current system.</source>
        <translation type="obsolete">Пароль пользователя root будет перенесен из текущей системы.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>User %1 already exists.</source>
        <translation>Пользователь %1 уже существует.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be deleted.</source>
        <translation type="obsolete">Пользователь guset не может быть удален.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be modified.</source>
        <translation type="obsolete">Пользователь guset не может быть изменен.</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation>Пароли совпадают</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation>Пароли не совпадают</translation>
    </message>
    <message>
        <source>These user accounts will be migrated: </source>
        <translation type="unfinished">Аккаунты этих пользователей будут перенесены: </translation>
    </message>
    <message>
        <source>If the users will not be added, the &apos;guest&apos; user be created automatically.</source>
        <translation type="unfinished">Если пользователи не будут добавлены, пользователь &apos;guest&apos; будет создан автоматически.</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <source>Welcome</source>
        <translation>Добро пожаловать</translation>
    </message>
    <message>
        <source>&lt;p&gt;Welcome to Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.org&apos;&gt;http://www.calculate-linux.org&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Добро пожаловать в Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.ru&apos;&gt;http://www.calculate-linux.ru&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Choose a language: </source>
        <translation>Выберите язык:</translation>
    </message>
    <message>
        <source>Please choose the language which should be used for this application.</source>
        <translation>Пожалуйста, выберите язык, который будет использоваться во время установки.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>You do not have administrative privileges.</source>
        <translation>Вы не имеете привелегий суперпользователя.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="obsolete">Беларусский</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="obsolete">Болгарский</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="obsolete">Датский</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="obsolete">Английский [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="obsolete">Английский [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="obsolete">Французский [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="obsolete">Французский [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="obsolete">Французский [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="obsolete">Немецкий</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="obsolete">Исландский</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Итальянский</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Польский</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Португальский</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Русский</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Шведский</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Испанский</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="obsolete">Норвежский</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="obsolete">Украинский</translation>
    </message>
</context>
<context>
    <name>SystemInstaller</name>
    <message>
        <source>Critical error</source>
        <translation>Критическая ошибка</translation>
    </message>
    <message>
        <source>Failed to launch &apos;cl-install&apos;.</source>
        <translation>Не удалось запустить &apos;cl-install&apos;</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>Закончить</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <source>Do you want to abort the installation now?</source>
        <translation type="unfinished">Вы хотите прервать установку?</translation>
    </message>
    <message>
        <source>About cl-install-gui</source>
        <translation>О программе calculate-intsall-gui</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графический фронтэнд для программы cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчик:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Переводчики:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>Calculate Linux installer</source>
        <translation type="unfinished">Программа для установки Calculate Linux</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished">Нет</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графический фронтэнд для программы cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчик:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Переводчики:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="unfinished">&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;Графический фронтэнд для программы cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчик:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Переводчики:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
</context>
<context>
    <name>UserInfoDialog</name>
    <message>
        <source>Add user</source>
        <translation>Добавить пользователя</translation>
    </message>
    <message>
        <source>Modify user</source>
        <translation>Изменить данные пользователя</translation>
    </message>
    <message>
        <source>User name:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translation>Подтверждение пароля</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation>Пароли совпадают</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation>Пароли не совпадают</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>User name is empty</source>
        <translation>Имя пользоватеоя не должно быть пустым.</translation>
    </message>
    <message>
        <source>User root can&apos;t added</source>
        <translation>Пользователь root не может быть добавлен</translation>
    </message>
</context>
</TS>
