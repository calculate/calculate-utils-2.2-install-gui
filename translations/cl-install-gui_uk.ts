<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk_UA" sourcelanguage="en_US">
<context>
    <name>CalculateConfig</name>
    <message>
        <source>Critical error</source>
        <translation type="unfinished">Критична помилка</translation>
    </message>
    <message>
        <source>cl-install error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MountPointDialog</name>
    <message>
        <source>Device: </source>
        <translatorcomment>Раздел:</translatorcomment>
        <translation type="obsolete">Розділ:</translation>
    </message>
    <message>
        <source>Mount point: </source>
        <translatorcomment>Точка монтирования:</translatorcomment>
        <translation>Точка монтування:</translation>
    </message>
    <message>
        <source>Format partition</source>
        <translatorcomment>Форматировать раздел</translatorcomment>
        <translation>Форматувати розділ</translation>
    </message>
    <message>
        <source>File system: </source>
        <translatorcomment>Файловая система:</translatorcomment>
        <translation>Файлова система:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасування</translation>
    </message>
    <message>
        <source>Partition: </source>
        <translation type="unfinished">Розділ:</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">Попередження</translation>
    </message>
    <message>
        <source>Mount point must be not empty</source>
        <translation type="unfinished">Точка монтування не повинна бути порожньою</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="unfinished">Точка монтування %1 повинна бути вибрана не більше одного разу</translation>
    </message>
</context>
<context>
    <name>PageCfdisk</name>
    <message>
        <source>Partitioning</source>
        <translatorcomment>Разметка</translatorcomment>
        <translation>Розмітка</translation>
    </message>
    <message>
        <source>Do manual partitioning. To finish, exit from %1</source>
        <translatorcomment>Разметьте диск вручную. Для окончания, выйдите из %1</translatorcomment>
        <translation>Розмітьте диск вручну. Для закінчення, вийдіть з%1</translation>
    </message>
</context>
<context>
    <name>PageConfiguration</name>
    <message>
        <source>Configuring</source>
        <translatorcomment>Конфигурация</translatorcomment>
        <translation>Конфігурація</translation>
    </message>
    <message>
        <source>Select parameters: </source>
        <translatorcomment>Выберите параметры:</translatorcomment>
        <translation>Оберіть параметри:</translation>
    </message>
    <message>
        <source>Hostname: </source>
        <translatorcomment>Имя хоста:</translatorcomment>
        <translation>Ім&apos;я хоста:</translation>
    </message>
    <message>
        <source>Domain: </source>
        <translatorcomment>Домен:</translatorcomment>
        <translation>Домен:</translation>
    </message>
    <message>
        <source>Language:</source>
        <translatorcomment>Язык:</translatorcomment>
        <translation>Мова:</translation>
    </message>
    <message>
        <source>Timezone:</source>
        <translatorcomment>Часовой пояс:</translatorcomment>
        <translation>Часовий пояс:</translation>
    </message>
    <message>
        <source>Device for install Grub:</source>
        <translatorcomment>Диск для установки загрузчика:</translatorcomment>
        <translation>Диск для встановлення завантажувача:</translation>
    </message>
    <message>
        <source>Video driver:</source>
        <translatorcomment>Видео драйвер:</translatorcomment>
        <translation>Відео драйвер:</translation>
    </message>
    <message>
        <source>Use desktop effects</source>
        <translatorcomment>Использовать эфекты рабочего стола</translatorcomment>
        <translation>Використовувати ефекти робочого столу</translation>
    </message>
    <message>
        <source>Expert settings</source>
        <translatorcomment>Расширенные настройки</translatorcomment>
        <translation>Розширені налаштування</translation>
    </message>
    <message>
        <source>Make options (MAKEOPTS):</source>
        <translatorcomment>Опции MAKEOPTS:</translatorcomment>
        <translation>Опції MAKEOPTS:</translation>
    </message>
    <message>
        <source>Proxy server:</source>
        <translatorcomment>Proxy сервер:</translatorcomment>
        <translation>Proxy сервер:</translation>
    </message>
    <message>
        <source>NTP server:</source>
        <translatorcomment>NTP сервер:</translatorcomment>
        <translation>NTP сервер:</translation>
    </message>
    <message>
        <source>Clock type:</source>
        <translatorcomment>Часы:</translatorcomment>
        <translation>Годинник:</translation>
    </message>
    <message>
        <source>Local</source>
        <translatorcomment>Локальное время</translatorcomment>
        <translation>Локальний час</translation>
    </message>
    <message>
        <source>UTC</source>
        <translation>UTC</translation>
    </message>
    <message>
        <source>Warning</source>
        <translatorcomment>Предупреждение</translatorcomment>
        <translation>Попередження</translation>
    </message>
    <message>
        <source>Hostname is empty.</source>
        <translatorcomment>Имя хоста не должно быть пустым.</translatorcomment>
        <translation>Ім&apos;я хоста не повинно бути порожнім.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="unfinished">Білоруська</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="unfinished">Болгарська</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="unfinished">Датська</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="unfinished">Англійська [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="unfinished">Англійська [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="unfinished">Французька [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="unfinished">Французька [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="unfinished">Французька [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="unfinished">Німецька</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="unfinished">Iсландська</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished">Iталійська</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="unfinished">Польська</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="unfinished">Португальська</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="unfinished">Російська</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished">Шведська</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="unfinished">Iспанська</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="unfinished">Норвезька нюнорськ</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="unfinished">Українська</translation>
    </message>
    <message>
        <source>Installation for assembling</source>
        <translation type="unfinished">Встановлення для збирання</translation>
    </message>
</context>
<context>
    <name>PageFinish</name>
    <message>
        <source>Complete</source>
        <translatorcomment>Завершение</translatorcomment>
        <translation>Завершення</translation>
    </message>
    <message>
        <source>&lt;h4&gt;Congratulation!&lt;/h4&gt;&lt;p&gt;Installation complete.Press Finish for exit.&lt;/p&gt;</source>
        <translatorcomment>&lt;h4&gt;Поздравляем!&lt;/h4&gt;&lt;p&gt;Установка успешно завершена. Нажмите &quot;Закончить&quot; для выхода из программы установки.&lt;/p&gt;</translatorcomment>
        <translation>&lt;h4&gt; Вітаємо! &lt;/ h4&gt; &lt;p&gt; Встановлення успішно завершено. Натисніть &quot;Завершити&quot; для виходу з програми інсталяції. &lt;/ P&gt;</translation>
    </message>
</context>
<context>
    <name>PageInstall</name>
    <message>
        <source>Installing</source>
        <translatorcomment>Установка</translatorcomment>
        <translation>Встановлення</translation>
    </message>
    <message>
        <source>Error. Additional information in /var/log/calculate/cl-install-gui-err.log</source>
        <translatorcomment>Ошибка. Дополнительная информация сохранена в файле /var/log/calculate/cl-install-gui-err.log</translatorcomment>
        <translation>Помилка. Додаткова інформація збережена у файлі / var / log / calculate / cl-install-gui-err.log</translation>
    </message>
    <message>
        <source>Users parsing error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageLicense</name>
    <message>
        <source>License</source>
        <translatorcomment>Лицензия</translatorcomment>
        <translation>Ліцензія</translation>
    </message>
    <message>
        <source>&lt;h3&gt;License&lt;/h3&gt;&lt;h4&gt;License Agreement&lt;/h4&gt;&lt;p&gt;This operating system (the OS) is composed of many individual software components, the copyrights on each of which belong to their respective owners. Each component is distributed under their own license agreement.&lt;/p&gt;&lt;p&gt;Installing, modifying or distributing this operating system, given to you as free archive, you agree with all of the following.&lt;/p&gt;&lt;h4&gt;Warranties&lt;/h4&gt;&lt;p&gt;This software is distributed without warranty of any kind. You assume all responsibility for the use of the operating system.&lt;/p&gt;&lt;h4&gt;Installing&lt;/h4&gt;&lt;p&gt;OS can be installed on any number of computers.&lt;/p&gt;&lt;h4&gt;Distribution&lt;/h4&gt;&lt;p&gt;Most of the software included in this operating system, allows you to freely modify, copy and distribute it. Also included in the OS software is distributed in the different conditions. For more information please refer to the documentation accompanying a particular software component.&lt;/p&gt;</source>
        <translation>&lt;h3&gt; License &lt;/ h3&gt; &lt;h4&gt; Ліцензійна угода &lt;/ h4&gt; &lt;p&gt; Ця операційна система (ОС) складається з безлічі окремих компонентів програмного забезпечення, авторські права на кожен з яких належать їх відповідним власникам. Кожен компонент розповсюджується під їх власною ліцензійною угодою. &lt;/ P&gt; Встановлення, зміну чи поширення цієї операційної системи, дані вам як вільний архів, ви згодні з усіма з наступних дій. &lt;/ P&gt; &lt; h4&gt; Гарантії &lt;/ h4&gt; &lt;p&gt; Це програмне забезпечення поширюється без будь-яких гарантій. Ви приймаєте на себе всю відповідальність за використання операційної системи. &lt;/ P&gt; &lt;h4&gt; Встановлення &lt;/ h4&gt; &lt;p&gt; OS може бути встановлена на будь-якій кількості комп&apos;ютерів. &lt;/ P&gt; &lt;h4&gt; поширення &lt; ; / h4&gt; &lt;p&gt; Більша частина програм, включених в цю операційну систему, дозволяє вільно змінювати, копіювати і поширювати його. Крім того, програмне забезпечення ОС поширюється в різних умовах. За додатковою інформацією звертайтеся до супровідної документації на конкретний компонент програмного забезпечення. &lt;/ P&gt; </translation>
    </message>
    <message>
        <source>Accept</source>
        <translatorcomment>Принимаю</translatorcomment>
        <translation>Приймаю</translation>
    </message>
</context>
<context>
    <name>PageMountPoints</name>
    <message>
        <source>Mount points</source>
        <translatorcomment>Точки монтирования</translatorcomment>
        <translation>Точки монтування</translation>
    </message>
    <message>
        <source>Partition</source>
        <translatorcomment>Раздел</translatorcomment>
        <translation>Розділ</translation>
    </message>
    <message>
        <source>Label</source>
        <translatorcomment>Метка</translatorcomment>
        <translation>Мітка</translation>
    </message>
    <message>
        <source>Size</source>
        <translatorcomment>Размер</translatorcomment>
        <translation>Розмір</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translatorcomment>Точка монтирования</translatorcomment>
        <translation type="obsolete">Точка монтування</translation>
    </message>
    <message>
        <source>File system</source>
        <translatorcomment>Файловая система</translatorcomment>
        <translation type="obsolete">Файлова система</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Форматувати</translation>
    </message>
    <message>
        <source>Select mount points:</source>
        <translatorcomment>Установите точки монтирования:</translatorcomment>
        <translation type="obsolete">Встановіть точки монтування:</translation>
    </message>
    <message>
        <source>Select the mount points use double click to partitions. For continue must be set mount point for /</source>
        <translatorcomment>Установите точки монтирования используя двойной клик на нужном разделе. Для продолжения должна быть установлена точка монтирования для корневого раздела (/)</translatorcomment>
        <translation type="obsolete">Встановіть точки монтування використовуючи подвійний клік на потрібному розділі. Для продовження повинна бути встановлена точка монтування для кореневого розділу (/)</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>You select auto partitioning. Press &quot;Next&quot; to continue.</source>
        <translation>Ви обрали автоматичну розмітку диска програмою встановлення. Натисніть &quot;Вперед&quot; для продовження.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Попередження</translation>
    </message>
    <message>
        <source>Select mount point for /</source>
        <translation type="obsolete">Оберіть точку монтування для кореневого розділу /</translation>
    </message>
    <message>
        <source>no</source>
        <translation>ні</translation>
    </message>
    <message>
        <source>YES</source>
        <translation>ТАК</translation>
    </message>
    <message>
        <source>Duplicate mount point %1</source>
        <translation type="obsolete">Точка монтування %1 повинна бути вибрана не більше одного разу</translation>
    </message>
    <message>
        <source>Add a new mount point</source>
        <translation type="unfinished">Додати нову точку монтування</translation>
    </message>
    <message>
        <source>Remove a selected mount point</source>
        <translation type="unfinished">Видалити обрану точку монтування</translation>
    </message>
    <message>
        <source>Mount
point</source>
        <translation type="unfinished">Точка
монтування</translation>
    </message>
    <message>
        <source>File
system</source>
        <translation type="unfinished">Файлова
система</translation>
    </message>
    <message>
        <source>Choose mount points for the installation. You need at least a root / partition.
To modify the mount point, double-click it.</source>
        <translation type="unfinished">Виберіть точки монтування для встановлення. Мінімально необхідно вибрати розділ для / розділу.
Для редагування створених точок монтування, використовуйте подвійний клік на ній.</translation>
    </message>
    <message>
        <source>To continue You need to choose partition for mount point %1.</source>
        <translation>Для продовження виберіть розділ для точки монтування%1</translation>
    </message>
    <message>
        <source>Adding is not possible. All partitions is busy.</source>
        <translation type="unfinished">Додавання не можливо. Всі розділи вже використовуються.</translation>
    </message>
    <message>
        <source>Root partition can&apos;t be deleted</source>
        <translation>Розділ / не може бути видалений.</translation>
    </message>
    <message>
        <source>Migrated mount point can&apos;t be deleted. Use &apos;none&apos; for disabling migration.</source>
        <translation type="unfinished">Переносима точка монтування не може бути вилучена. Використовуйте &apos;none&apos; в імені розділу для деактивації перенесення цієї точки.</translation>
    </message>
</context>
<context>
    <name>PagePartitioning</name>
    <message>
        <source>Partitioning</source>
        <translation>Розмітка</translation>
    </message>
    <message>
        <source>Disk for install: </source>
        <translatorcomment>Диск для установки:</translatorcomment>
        <translation>Диск для встановлення:</translation>
    </message>
    <message>
        <source>Use existing partitions</source>
        <translatorcomment>Использовать существующие разделы</translatorcomment>
        <translation>Використовувати існуючі розділи</translation>
    </message>
    <message>
        <source>Use automatically partitioning</source>
        <translatorcomment>Использовать автоматическую разметку диска</translatorcomment>
        <translation>Використовувати автоматичну розмітку диска</translation>
    </message>
    <message>
        <source>Manually partitioning</source>
        <translatorcomment>Ручная разметка</translatorcomment>
        <translation>Ручна розмітка</translation>
    </message>
    <message>
        <source>Selected disk will be used for manual or automatic partitioning.</source>
        <translatorcomment>Выбранный диск будет использоваться для ручной или автоматической разметки.</translatorcomment>
        <translation>Обраний диск буде використовуватися для ручної або автоматичної розмітки.</translation>
    </message>
    <message>
        <source>Error</source>
        <translatorcomment>Ошибка</translatorcomment>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>Disks not found</source>
        <translatorcomment>Диски не найдены</translatorcomment>
        <translation>Диски не знайдені</translation>
    </message>
</context>
<context>
    <name>PageUsers</name>
    <message>
        <source>Users</source>
        <translatorcomment>Пользователи</translatorcomment>
        <translation>Користувачі</translation>
    </message>
    <message>
        <source>Set root password:</source>
        <translatorcomment>Установите пароль для root:</translatorcomment>
        <translation>Встановіть пароль для root:</translation>
    </message>
    <message>
        <source>Password</source>
        <translatorcomment>Пароль</translatorcomment>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translatorcomment>Подтверждение пароля</translatorcomment>
        <translation>Підтвердження пароля</translation>
    </message>
    <message>
        <source>Add user</source>
        <translatorcomment>Добавить пользователя</translatorcomment>
        <translation>Додати користувача</translation>
    </message>
    <message>
        <source>Remove selected user</source>
        <translatorcomment>Удалить выбранного пользователя</translatorcomment>
        <translation>Видалити обраного користувача</translation>
    </message>
    <message>
        <source>Added users.
For modifying user - double click it.</source>
        <translatorcomment>Добавленные пользователи.
Для редактирования данных пользователя - дважды кликните его.</translatorcomment>
        <translation>Додані користувачі.
Для редагування даних користувача - двічі клікніть його.</translation>
    </message>
    <message>
        <source>Create users:</source>
        <translatorcomment>Создать пользователя:</translatorcomment>
        <translation>Створити користувача:</translation>
    </message>
    <message>
        <source>Root password will be moved from current system.</source>
        <translatorcomment>Пароль пользователя root будет перенесен из текущей системы.</translatorcomment>
        <translation type="obsolete">Пароль користувача root буде перенесений з поточної системи.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>User %1 already exists.</source>
        <translatorcomment>Пользователь %1 уже существует.</translatorcomment>
        <translation>Користувач%1 вже існує.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be deleted.</source>
        <translatorcomment>Пользователь guset не может быть удален.</translatorcomment>
        <translation type="obsolete">Користувач guset не може бути знищено.</translation>
    </message>
    <message>
        <source>User guest can&apos;t be modified.</source>
        <translatorcomment>Пользователь guset не может быть изменен.</translatorcomment>
        <translation type="obsolete">Користувач guset не може бути змінений.</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translatorcomment>Пароли совпадают</translatorcomment>
        <translation>Паролі збігаються</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translatorcomment>Пароли не совпадают</translatorcomment>
        <translation>Паролі не збігаються</translation>
    </message>
    <message>
        <source>These user accounts will be migrated: </source>
        <translation type="unfinished">Акаунти цих користувачів будуть перенесені:</translation>
    </message>
    <message>
        <source>If the users will not be added, the &apos;guest&apos; user be created automatically.</source>
        <translation type="unfinished">Якщо користувачі не будуть додані, користувач &apos;guest&apos; буде створений автоматично.</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <source>Welcome</source>
        <translatorcomment>Добро пожаловать</translatorcomment>
        <translation>Ласкаво просимо</translation>
    </message>
    <message>
        <source>&lt;p&gt;Welcome to Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.org&apos;&gt;http://www.calculate-linux.org&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ласкаво просимо до Calculate Linux.&lt;/p&gt;&lt;p&gt;&lt;a href=&apos;http://www.calculate-linux.org&apos;&gt;http://www.calculate-linux.org&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Choose a language: </source>
        <translatorcomment>Выберите язык:</translatorcomment>
        <translation>Оберіть мову:</translation>
    </message>
    <message>
        <source>Please choose the language which should be used for this application.</source>
        <translatorcomment>Пожалуйста, выберите язык, который будет использоваться во время установки.</translatorcomment>
        <translation>Будь ласка, оберіть мову, яка буде використовуватися під час встановлення.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>You do not have administrative privileges.</source>
        <translatorcomment>Вы не имеете привелегий суперпользователя.</translatorcomment>
        <translation>Ви не маєте привілеїв суперкористувача.</translation>
    </message>
    <message>
        <source>Belarusian</source>
        <translation type="obsolete">Білоруська</translation>
    </message>
    <message>
        <source>Bulgarian</source>
        <translation type="obsolete">Болгарська</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="obsolete">Датська</translation>
    </message>
    <message>
        <source>English [en_GB]</source>
        <translation type="obsolete">Англійська [en_GB]</translation>
    </message>
    <message>
        <source>English [en_US]</source>
        <translation type="obsolete">Англійська [en_US]</translation>
    </message>
    <message>
        <source>French [fr_BE]</source>
        <translation type="obsolete">Французька [fr_BE]</translation>
    </message>
    <message>
        <source>French [fr_CA]</source>
        <translation type="obsolete">Французька [fr_CA]</translation>
    </message>
    <message>
        <source>French [fr_FR]</source>
        <translation type="obsolete">Французька [fr_FR]</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="obsolete">Німецька</translation>
    </message>
    <message>
        <source>Icelandic</source>
        <translation type="obsolete">Iсландська</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Iталійська</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Польська</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Португальська</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Російська</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Шведська</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Iспанська</translation>
    </message>
    <message>
        <source>Norwegian Nynorsk</source>
        <translation type="obsolete">Норвезька нюнорськ</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation type="obsolete">Українська</translation>
    </message>
</context>
<context>
    <name>SystemInstaller</name>
    <message>
        <source>Critical error</source>
        <translatorcomment>Критическая ошибка</translatorcomment>
        <translation>Критична помилка</translation>
    </message>
    <message>
        <source>Failed to launch &apos;cl-install&apos;.</source>
        <translation>Не вдалося запустити &apos;cl-install&apos;.</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished">Назад</translation>
    </message>
    <message>
        <source>Next</source>
        <translatorcomment>Вперед</translatorcomment>
        <translation type="unfinished">Вперед</translation>
    </message>
    <message>
        <source>Finish</source>
        <translatorcomment>Закончить</translatorcomment>
        <translation>Закінчити</translation>
    </message>
    <message>
        <source>About</source>
        <translatorcomment>О программе</translatorcomment>
        <translation>Про програму</translation>
    </message>
    <message>
        <source>Copy</source>
        <translatorcomment>Копировать</translatorcomment>
        <translation>Копіювати</translation>
    </message>
    <message>
        <source>Attention</source>
        <translatorcomment>Внимание</translatorcomment>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Do you want to abort the installation now?</source>
        <translation type="unfinished">Ви хочете перервати встановлення ?</translation>
    </message>
    <message>
        <source>About cl-install-gui</source>
        <translatorcomment>О программе calculate-intsall-gui</translatorcomment>
        <translation>Про програму calculate-intsall-gui</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</source>
        <translatorcomment>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графический фронтэнд для программы cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Разработчик:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Переводчики:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</translatorcomment>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графычний фронтенд для програми cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt; Розробник:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Перекладачі:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>Calculate Linux installer</source>
        <translation type="unfinished">Програма для встановлення Calculate Linux</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished">Так</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished">Нi</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;calculate-install-gui&lt;/b&gt;&lt;br&gt;Графычний фронтенд для програми cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt; Розробник:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Перекладачі:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;GUI-frontend for cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt;Developer:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Ivan Loskutov aka vanner&lt;br&gt;&lt;br&gt;Translators:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rosen Alexandrov aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Vadim Bosyuk aka Vados&lt;br&gt;</source>
        <translation type="unfinished">&lt;b&gt;calculate-install-gui %1&lt;/b&gt;&lt;br&gt;Графычний фронтенд для програми cl-install&lt;br&gt;&lt;br&gt;&lt;br&gt; Розробник:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Иван Лоскутов aka vanner&lt;br&gt;&lt;br&gt;Перекладачі:&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Росен Александров aka ROKO__&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Вадим Босюк aka Vados&lt;br&gt;</translation>
    </message>
</context>
<context>
    <name>UserInfoDialog</name>
    <message>
        <source>Add user</source>
        <translation>Додати користувачів</translation>
    </message>
    <message>
        <source>Modify user</source>
        <translatorcomment>Изменить данные пользователя</translatorcomment>
        <translation>Змінити дані користувача</translation>
    </message>
    <message>
        <source>User name:</source>
        <translatorcomment>Имя пользователя:</translatorcomment>
        <translation>Ім&apos;я користувача:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Confirm Password</source>
        <translation>Підтвердіть пароль</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation>Паролі збігаються</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation>Паролі не збігаються</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>User name is empty</source>
        <translatorcomment>Имя пользоватеоя не должно быть пустым.</translatorcomment>
        <translation>Ім&apos;я користувача не повинно бути порожнім</translation>
    </message>
    <message>
        <source>User root can&apos;t added</source>
        <translatorcomment>Пользователь root не может быть добавлен</translatorcomment>
        <translation>Користувач root не може бути доданий</translation>
    </message>
</context>
</TS>
